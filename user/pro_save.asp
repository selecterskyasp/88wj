﻿<!--#include file="top.asp"-->
<%
id=userid

f_cpname=checkstr(request.Form("f_cpname"))
if not chkNull(f_cpname,1) then f_cpname=left(f_cpname,100)
f_ecpname=checkstr(request.Form("f_ecpname"))
if not chkNull(f_ecpname,1) then f_ecpname=left(f_ecpname,120)
f_guige=checkstr(request.Form("f_guige"))
if not chkNull(f_guige,1) then f_guige=left(f_guige,15)
f_eguige=checkstr(request.Form("f_eguige"))
if not chkNull(f_eguige,1) then f_eguige=left(f_eguige,30)
f_bClassId=request.Form("f_bClassId")
f_sClassId=request.Form("f_sClassId")
f_cppic=request.Form("f_cppic")

f_jiage=request.Form("f_jiage")
f_num=request.Form("f_num")
f_content=replace_text(request.Form("f_content"))
f_econtent=replace_text(request.Form("f_econtent"))

if chkrequest(f_jiage) then
f_face=1
f_jiage=cdbl(f_jiage)
else 
f_face=0
f_jiage=0
end if
if chkrequest(f_num) then
f_face=1
f_num=cdbl(f_num)
else 
f_face=0
f_num=0
end if
errmsg=""
founderr=false

if not chkRange(f_cpname,2,50) then
founderr=true
errmsg=errmsg&"\n\n·产品名字输入不正确,长度必须为2-50个字符"
end if

if not chkrequest(f_bClassId) then
founderr=true
errmsg=errmsg&"\n\n·产品大分类丢失，请重新选择"
end if

if not chkrequest(f_sClassId) then
founderr=true
errmsg=errmsg&"\n\n·产品小分类丢失，请重新选择"
end if

if not chkRange(f_content,10,2000) then
founderr=true
if len(f_content)<10 then msg="，您的字数少于10个字"
if len(f_content)>2000 then msg="，您共输入了"&len(f_content)&"个字，已经超过2000字，请适当删除再提交"
errmsg=errmsg&"\n\n·您没有输入产品介绍(10-2000字)"&msg
end if

if founderr then
response.Write("<script>alert('对不起,产品信息操作失败,下面是可能引起失败的原因:"&errmsg&"');history.back();</script>")
response.End()
end if

set rs=server.CreateObject("adodb.recordset")
if request.QueryString("action")="add" then
'if session("proadd")<>"no" then
result=request.Form("f_code")
result=lcase(trim(result))

if result<>lcase(trim(session("GetCode"))) or len(result)<2 then
	call showErrmsg("\n·请输入正确的验证码!","")
end if
session("proadd")="no"
'end if
if not canSend then
call showErrmsg("\n\n-您的会员还没有通过审核，不能发布产品信息","")
end if
isadd=request.Form("f_isadd")
if chkrequest(isadd)=false or session("isadd")="" then
response.write("<script>alert('请不要在外面提交数据!');history.back();</script>")
response.end
end if
isadd=cint(isadd)
if cint(session("isadd"))<>isadd then
response.write("<script>alert('请不要在外面提交数据!');history.back();</script>")
response.end
end if
rs.open "select * from pro",conn,1,3
rs.addnew
rs("pronam")=f_cpname
rs("epronam")=f_ecpname
rs("prospe")=f_guige
rs("eprospe")=f_eguige
rs("proclb")=f_sClassId
rs("proimg")=f_cppic
rs("prosig")=f_cppic
rs("propre")=f_jiage
rs("pronum")=f_num
rs("procmt")=f_content
rs("eprocmt")=f_econtent
rs("profac")=f_face
rs("procdat")=now()
rs("proacti")="Y"
rs("proseq")=1
rs("prosel")=1
rs("prousr")=id
rs("proding")=false
rs("proshe")=0
rs.update
elseif request.QueryString("action")="edit" then
isadd=request.Form("f_isadd")
if chkrequest(isadd)=false or session("isadd")="" then
response.write("<script>alert('请不要在外面提交数据!');history.back();</script>")
response.end
end if
isadd=cint(isadd)
if cint(session("isadd"))<>isadd then
response.write("<script>alert('请不要在外面提交数据!');history.back();</script>")
response.end
end if

rs.open "select * from pro where proide="&clng(request.QueryString("id"))&" and prousr='"&userid&"'",conn,1,3
rs("pronam")=f_cpname
rs("epronam")=f_ecpname
rs("prospe")=f_guige
rs("eprospe")=f_eguige
rs("proclb")=f_sClassId
rs("proimg")=f_cppic
rs("prosig")=f_cppic
rs("propre")=f_jiage
rs("pronum")=f_num
rs("procmt")=f_content
rs("eprocmt")=f_econtent
rs("profac")=f_face
rs.update
else
response.Write("<script>alert('必要的参数丢失!');location.href='managepro.asp';</script>")
response.End()
end if

if not err then
session("GetCode")=""
session("isadd")=""
rs.close
set rs=nothing
closeconn
response.Write("<script>alert('恭喜,产品信息操作成功!');location.href='managepro.asp';</script>")
else
rs.close
set rs=nothing
closeconn
response.Write("<script>alert('对不起,产品信息操作失败,下面是可能引起失败的原因:\n\n数据库操作失败!');history.back();</script>")
response.End()
end if
%>
