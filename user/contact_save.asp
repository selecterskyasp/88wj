<!--#include file="top.asp"-->
<%
id=userid
f_lxr=checkstr(request.Form("f_lsr"))
f_xb=checkstr(request.Form("f_xb"))
f_tel=checkstr(request.Form("f_tel"))
f_city=checkstr(request.Form("addtwo_id"))
f_addr=checkstr(request.Form("f_addr"))
f_zhw=checkstr(request.Form("f_zhw"))
f_hy=checkstr(request.Form("f_hy"))
f_sell=checkstr(request.Form("f_sell"))
f_buy=checkstr(request.Form("f_buy"))

f_mobile=checkstr(request.Form("f_mobile"))
f_url=checkstr(request.Form("f_url"))
f_fax=checkstr(request.Form("f_fax"))
f_yb=checkstr(request.Form("f_yb"))

errmsg=""
founderr=false


if not chkRange(f_lxr,2,8) or not chkChinese(f_lxr) then
founderr=true
errmsg=errmsg&"\n\n·联系人输入不正确,长度必须2-4个汉字"
end if

if f_xb<>"1" and f_xb<>"0" then
founderr=true
errmsg=errmsg&"\n\n·请选择联系人性别"
else
f_xb=cint(f_xb)
if f_xb<>1 and f_xb<>0 then
founderr=true
errmsg=errmsg&"\n\n·请选择联系人性别"
end if
end if

if not chkRange(f_tel,6,13) then
founderr=true
errmsg=errmsg&"\n\n·联系电话不正确"
end if

if len(f_city)<>6 then
founderr=true
errmsg=errmsg&"\n\n·城市丢失,请选择您所有的城市"
end if

if not chkRange(f_addr,6,20) then
founderr=true
errmsg=errmsg&"\n\n·联系地址不正确,长度必须为6-20个字符"
end if

if not chkRange(f_zhw,2,10) or not chkChinese(f_zhw) then
founderr=true
errmsg=errmsg&"\n\n·职位输入不正确,长度为2-5个汉字"
end if

if not chkrequest(f_hy) then
founderr=true
errmsg=errmsg&"\n\n·行业丢失,请选择主营行业"
else
f_hy=cint(f_hy)
if f_hy<1 or f_hy>4 then
founderr=true
errmsg=errmsg&"\n\n·行业丢失,请选择主营行业"
end if
end if

if not chkRange(f_sell,2,10) then
founderr=true
errmsg=errmsg&"\n\n·主要供应的产品输入不正确,长度必须2-10个字符"
end if

if not chkRange(f_buy,2,10) then
founderr=true
errmsg=errmsg&"\n\n·主要求购的产品输入不正确,长度必须2-10个字符"
end if

if founderr then
response.Write("<script>alert('对不起,注册用户失败,下面是可能引起失败的原因:"&errmsg&"');history.back();</script>")
response.End()
end if
set rs=server.CreateObject("adodb.recordset")
rs.open "select * from usr where usride='"&id&"'",conn,1,3
rs("usrlsr")=f_lxr
rs("usrsex")=f_xb
rs("usrtel")=f_tel
rs("usrcty")=f_city
rs("usradr")=f_addr
rs("usrjob")=f_zhw
rs("usrtra")=f_hy
rs("usrsel")=f_sell
rs("usrbuy")=f_buy
rs("usrfax")=f_fax
rs("usrhtp")=f_url
rs("usrsjh")=f_mobile
rs("usrpos")=f_yb
rs.update
if not err then
rs.close
set rs=nothing
closeconn
response.Write("<script>alert('恭喜,修改信息成功!');location.href='contact.asp';</script>")
else
rs.close
set rs=nothing
closeconn
response.Write("<script>alert('对不起,修改信息失败,下面是可能引起失败的原因:\n\n数据库操作失败!');history.back();</script>")
response.End()
end if
%>
