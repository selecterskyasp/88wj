﻿<!--#include file="top.asp"-->
<!--#include virtual="/fckeditor/fckeditor.asp"-->
<%
randomize timer
isadd= int(rnd*8999)+1000 
session("isadd")=isadd
flag 	= request.QueryString("flag")
id 		= request.QueryString("id")

if not chkNull(flag,1) and chkrequest(id) then 	
	set rs=conn.execute ("select top 1 msgtyp,msgnam,msgcmt from msg where msgide="&clng(id)&" and msgusr='"&userid&"'")
	if not rs.eof then 
		msgtyp	= rs(0)
		msgnam	= rs(1)
		msgcmt	= rs(2)
	else
		call closers(rs)
		call showErrmsg("\n\n-找不到此信息，可能此信息已经删除","")
	end if
	rs.close
	set rs=nothing
	call closeconn()
else
if not canSend then
	call showErrmsg("\n\n-您的会员还没有通过审核，不能发布新闻信息","")
end if
end if
%>
<!-- 内容开始 -->
<!--友情提示-->
<!--#include file="head.asp"-->
<div class="nav"> <span class="gray">您现在的位置是：</span><a href="/user/" class="gray">会员管理中心</a> <span class="gray">&gt; 资讯管理</span> </div>
<!--会员信息-->
<div class="columnbt">
<h2>添加资讯</h2>
</div>
<div class="columncontent">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="msgtbl">
  <form name="news" method="post" action="action/news.asp" onsubmit="return Validator.Validate(this,3)">
  <%
	if chkrequest(id) then 
		response.Write("<input type=""hidden"" name=""id"" value="""&id&""" />")&vbcrlf
		response.Write("<input type=""hidden"" name=""action"" value=""edit"" />")&vbcrlf
	else
		response.Write("<input type=""hidden"" name=""action"" value=""add"" />")&vbcrlf
	end if 
  %>
  
  <tr>
    <td width="14%">标题:</td>
    <td width="86%"><input name="msgnam" type="text" value="<%=msgnam%>" size="60" maxlength="50" dataType="Require" msg="请先输入标题!"/></td>
  </tr>
  <tr>
    <td>类别:</td>
    <td>
		<select name="msgtyp" dataType="Require" msg="请选择资讯类别!">
			<option value="">请选择资讯类别</option>
			<option value="0" <%if msgtyp="0" then response.write "selected"%>>财经新闻</option>
			<option value="1" <%if msgtyp="1" then response.write "selected"%>>国际动态</option>
			<option value="2" <%if msgtyp="2" then response.write "selected"%>>行业动态</option>
			<option value="3" <%if msgtyp="3" then response.write "selected"%>>产业新闻</option>
			<option value="4" <%if msgtyp="4" then response.write "selected"%>>品牌战略</option>
			<option value="5" <%if msgtyp="5" then response.write "selected"%>>公司动态</option>
			<option value="6" <%if msgtyp="6" then response.write "selected"%>>价格行情</option>
			<option value="8" <%if msgtyp="8" then response.write "selected"%>>库存信息</option>
			<option value="9" <%if msgtyp="9" then response.write "selected"%>>五金市场</option>
		</select>
	</td>
  </tr>
  <tr>
    <td>内容:</td>
    <td>
	<%
	if not chkNull(msgcmt,1) then
	msgcmt=replace_t(msgcmt)
	end if
	Dim oFCKeditor
	Set oFCKeditor = New FCKeditor
	oFCKeditor.ToolbarSet 	="Basic"
	'oFCKeditor.BasePath	= sBasePath
	oFCKeditor.Width	= "100%"
	oFCKeditor.Height	= "300"
	oFCKeditor.Value	= msgcmt
	oFCKeditor.Create "msgcmt"
	%>
	</td>
  </tr>
   <tr>
    <td>验证码:</td>
    <td><input size="8" name="f_code2" dataType="Require"  msg="请输入正确的答案!"/><script language="javascript" src="/inc/code.js"></script>
    &nbsp;<a href="javascript:;" onclick="document.getElementById('codeiframe').src='/inc/code.asp';">刷新验证码</a>&nbsp;<span class="red">为了防止恶意发布,请输入图片中的字符(不区分大小写)</span>	
	</td>
  </tr>  
  <tr>
    <td><input type="hidden" name="usrid" value="<%=userid%>" /></td>
    <td><input type="hidden" name="f_isadd" id="f_isadd" value="<%=isadd%>" /><input type="submit" name="submit" value="提  交"/>&nbsp;&nbsp;&nbsp;<input name="reset" value="重  设" type="reset"/></td>
  </tr>
  </form>
</table>
<%closeconn%>
</div>
<!-- 内容结束 -->
</body>
</html>