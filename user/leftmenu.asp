<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="UTF-8" />
<meta name="robots" content="all" />
<meta http-equiv="imagetoolbar" content="false" />
<meta name="MSSmartTagsPreventParsing" content="true" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<meta name="Copyright" content="Copyright (c) 2008 www.58china.cn" />
<meta name="Keywords" content="五金，采购，机械，机床，出口，进口，百货，工具，建材，模具，小家电，金属，不锈钢，日用品" />
<meta name="Description" content="中国最大、最安全的网上交易社区，尽享中华二手设备网！" />
<link rel="stylesheet" href="style/css.css" type="text/css" media="all" />
<title>中华二手设备网会员管理后台</title>
</head>
<body>

<!-- 内容开始 -->
<%if request.cookies("islogin")("user")<>"" then 
url="showInfo.asp" 
target="right"
else 
url="index.html"
target="_parent"
end if 
%>
<div class="menuicon"><h2><a href="<%=url%>" target="<%=target%>" class="greenlink" >商务中心首页</a></h2>
</div>
<div class="menuicon"><h2><a href="javascript:void(0)" class="greenlink" onclick="change('display1'); return false">企业资料</a></h2>
</div>
<div id="display1">
<div class="menucon">
<div class="hd">
	<a class='menu_title' href="busicInfo.asp" target="right" name="right">·基本资料</a>
	<a class='menu_title' href="contact.asp" target="right" name="right">·联系方式</a>
	<a class='menu_title' href="editpass.asp" target="right" name="right">·修改密码</a>
</div>
</div>
</div>
<!--<div class="menuicon"><h2><a href="javascript:void(0)" class="greenlink" onclick="change('display2'); return false">自助建站</a></h2></div>
<div id="display2">
<div class="menucon">
<div class="hd">
	<a class='menu_title' href="#" target="right" name="right">·企业自助建站</a>
</div>
</div>
</div>-->
<div class="menuicon">
  <h2><a href="javascript:void(0)" class="greenlink" onclick="change('display3'); return false">产品管理</a></h2>
</div>
<div id="display3">
<div class="menucon">
<div class="hd">
	<a class='menu_title' href="proadd.asp" target="right" name="right">·产品添加</a>
	<a class='menu_title' href="managePro.asp" target="right" name="right">·产品列表</a>
</div>
</div>
</div>
<div class="menuicon">
  <h2><a href="javascript:void(0)" class="greenlink" onclick="change('display4'); return false">商贸机会</a></h2>
</div>
<div id="display4">
<div class="menucon">
<div class="hd">
	<!--<a class='menu_title' href="pieclass.asp" target="right" name="right">·发布商机</a> -->
	<a class='menu_title' href="addshangji.asp" target="right" name="right">·发布商机</a>
	<a class='menu_title' href="manageBusiness.asp" target="right" name="right">·商机列表</a>
</div>
</div>
</div>
<div class="menuicon"><h2><a href="javascript:void(0)" class="greenlink" onclick="change('display5'); return false">展会管理</a></h2></div>
<div id="display5">
<div class="menucon">
<div class="hd">
	<a class='menu_title' href="addExhi.asp" target="right" name="right">·发布展会</a>
	<a class='menu_title' href="manageExhi.asp" target="right" name="right">·展会列表</a>
</div>
</div>
</div>
<div class="menuicon"><h2><a href="javascript:void(0)" class="greenlink" onclick="change('display6'); return false">新闻管理</a></h2></div>
<div id="display6">
<div class="menucon">
<div class="hd">
	<a class='menu_title' href="addnews.asp" target="right" name="right">·发布新闻</a>
	<a class='menu_title' href="managenews.asp" target="right" name="right">·新闻列表</a>
</div>
</div>
</div>
<div class="menuicon"><h2><a href="logout.asp" class="greenlink" target="_parent">退出管理后台</a></h2></div>
<!-- 内容结束 -->

<script language="JavaScript">
function change(el) {
whichEl = document.getElementById(el)   //获得指定ID值的对象
if (whichEl.style.display == 'none') {   //block是默认值为显示,none为隐藏
   whichEl.style.display   = 'block'; 
} else {
     whichEl.style.display   = 'none';
   }
}
</script>
</body>
</html>