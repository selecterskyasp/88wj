﻿<!--#include file="top.asp"-->
<%
if not canSend then
call showErrmsg("\n\n-您的会员还没有通过审核，不能发布产品信息","")
end if
%>
<!--#include virtual="/fckeditor/fckeditor.asp"-->
<script language="javascript" type="text/javascript" src="/jquery/jquery.js"></script>
<script language="javascript" type="text/javascript" src="/jquery/jQuery.FillOptions.js"></script>
<script language="javascript" type="text/javascript" src="/jquery/jQuery.CascadingSelect.js"></script>
<script type="text/javascript" src="/ajaxupload/AnPlus.js"></script>
<script type="text/javascript" src="/ajaxupload/AjaxUploader.js"></script>
<%
randomize timer
isadd= int(rnd*8999)+1000 
session("isadd")=isadd
%>
<!-- 内容开始 -->
<!--友情提示-->
<!--#include file="head.asp"-->
<div class="nav"> <span class="gray">您现在的位置是：</span><a href="/user/" class="gray">会员管理中心</a> <span class="gray">&gt; 产品添加</span> </div>
<!--会员信息-->
<div class="columnbt">
<h2>产品添加</h2>
</div>
<div class="columncon">
<div class="columncontent">
<!--==============-->
<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td>
<!--####################################-->
<table width="100%" border="0" cellspacing="1" cellpadding="0" > 
  <tr>
    <td bgcolor=#ffffff height=25>
<!--======-->
<table width="80%" border="0" cellspacing="0" cellpadding="0" align=center>
<form name="addform" method="post" action="pro_save.asp?action=add"  onSubmit="return Validator.Validate(this,3);"> 
  <tr>
    <td width="21%" align=right height=30><font color=red>*</font>产品名称： &nbsp;</td>
	<td width="79%"><input name="f_cpname" type="text" size="20" maxlength="40"  dataType="Require" msg="产品名称不能为空"></td>
  </tr> 
  <tr>
    <td width="21%" align=right height=30>英文名称： &nbsp;</td>
	<td width="79%"><input name="f_ecpname" type="text" size="20" maxlength="40"></td>
  </tr> 
  <tr>
    <td width="21%" align=right height=30>产品规格： &nbsp;</td>
	<td width="79%"><input name="f_guige" type="text" size="20" maxlength="40"></td>
  </tr> 
  <tr>
    <td width="21%" align=right height=30>英文规格： &nbsp;</td>
	<td width="79%"><input name="f_eguige" type="text" size="20" maxlength="40"></td>
  </tr>
  <tr>
    <td align=right height=30><font color=red>*</font>产品分类： &nbsp;</td>
	<td><select name="f_bClassId" id="f_bClassId" style="width:100px" dataType="Require" msg="请选择产品大分类">
</select>&nbsp;<select name="f_sClassId" id="f_sClassId" style="width:100px" dataType="Require" msg="请选择产品小分类">
</select>
  </tr>  
  <script language="javascript" type="text/javascript">
    
    $(function(){        
            $("#f_bClassId").FillOptions("/inc/getclass.asp",{datatype:"json",textfield:"name",valuefiled:"id"});
    });
    
	$("#f_bClassId").CascadingSelect(
							$("#f_sClassId"),
							"/inc/getclass.asp",
							{datatype:"json",textfield:"name",valuefiled:"id",parameter:"p"},
							function(){}
			);
    </script>
  <tr>
    <td align=right height=30>产品图片： &nbsp;</td>
	<td><input name="f_cppic" type="text" size="40" maxlength="48">
	<INPUT alt="请单击“浏览”上传图" TYPE="button" value="浏览" onClick="showUploader('f_cppic',this);">
	<div style="color:#FF0000; display:none" id="sperr"></div> 
	<!--<input type="button" name="Submit11" value="上传图片" onclick="window.open('aldupload.asp?id=f_cppic','','status=no,scrollbars=no,top=20,left=110,width=400,height=120')" />-->
	</td>
  </tr> 
   <div id="uploadContenter" style="background-color:#eeeeee;position:absolute;border:1px #555555 solid;padding:3px;"></div>
<iframe style="display:none;" name="AnUploader"></iframe>
<script type="text/javascript">
	/*======================================
	下面语句使上传控件显示在上面ID为uploadContenter的Div标签
	提交URL为upload.asp保存目录为upload
	表单提交到上面name属性为AnUploader的iframe里面；
	========================================*/
	
	var AjaxUp=new AjaxProcesser("uploadContenter");
	
	//设置提交到的iframe名称
	AjaxUp.target="AnUploader";  
	
	//上传处理页面,尽量不要更改
	AjaxUp.url="/ajaxupload/memberupload.asp"; 
	
	//保存目录
	AjaxUp.savePath="/UploadFile/Member/"+"<%=userid%>";
	
	var contenter=document.getElementById("uploadContenter");
	contenter.style.display="none"; //隐藏容器
	
	function showUploader(objID,srcElement){
		AjaxUp.reset();  //重置上传控件		
		contenter.style.display="block"; //显示容器
		var ps=_.abs(srcElement);//作用--获取指定标签的绝对坐标,目的是为了把上传控件定位到按钮下面
		contenter.style.top=(ps.y + 30) + "px";  
		contenter.style.left=(ps.x-180) + "px";
		//上传成功时要执行的程序
		AjaxUp.succeed=function(files){
		    var fujian=document.getElementById(objID);
			fujian.value=AjaxUp.savePath + "/" + files[0].name;  //因为上传控件就只上传一个 文件，这里索引是0
			contenter.style.display="none";			
			document.getElementById("sperr").style.display="none";
		}
		//上传失败时要执行的程序
		AjaxUp.faild=function(msg){document.getElementById("sperr").innerText="文件上传失败，原因："+msg;contenter.style.display="none";document.getElementById("sperr").style.display="block"}
	}
</script>

  <tr>
    <td align=right height=30>产品价格： &nbsp;</td>
	<td><input name="f_jiage" type="text" size="10" maxlength="10"  dataType="Double" msg="参考价格输入不正确,应为数字" value="0">
<font color=red>0</font>为价格面议</td>
  </tr> 
  <tr>
    <td align=right height=30>产品数量： &nbsp;</td>
	<td><input name="f_num" type="text" size="10" maxlength="10"  dataType="Double" msg="产品数量输入不正确,应为数字" value="0">
<font color=red>0</font>为无限供应</td>
  </tr> 
<tr>
    <td>产品介绍(中文):</td>
    <td>
	<%
	Dim oFCKeditor
	Set oFCKeditor = New FCKeditor
	oFCKeditor.ToolbarSet 	="Basic"
	'oFCKeditor.BasePath	= sBasePath
	oFCKeditor.Width	= "100%"
	oFCKeditor.Height	= "300"
	oFCKeditor.Value	= ""
	oFCKeditor.Create "f_content"
	%>
	</td>
  </tr><tr>
    <td>产品介绍(英文):</td>
    <td>
	<%
	oFCKeditor.Value	= ""
	oFCKeditor.Create "f_econtent"
	%>
	</td>
  </tr>  
  <%'if session("proadd")<>"no" then%>
  <tr>
    <td>验证码:</td>
    <td><input size="8" name="f_code" dataType="Require"  msg="请输入正确的答案!"/><script language="javascript" src="/inc/code.js"></script>
    &nbsp;<a href="javascript:;" onclick="document.getElementById('codeiframe').src='/inc/code.asp';">刷新验证码</a>&nbsp;<span class="red">为了防止恶意发布,请输入图片中的字符(不区分大小写)</span>	
	</td>
  </tr>  
  <%'end if%>
  <tr>
    <td height=30 align="center" colspan="2"><input type="hidden" name="f_isadd" id="f_isadd" value="<%=isadd%>" /><input type="submit" name="submit" value="提  交"/></td>
  </tr>
</form>
</table>
<!--======-->
	</td>
  </tr>
</table>
<!--####################################-->
	</td>
  </tr>
<tr><td height=25></td></tr>
</table>
<!--==============-->
</div>
</div>
<!-- 内容结束 -->
</body>
</html>