<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="UTF-8" />
<meta name="robots" content="all" />
<meta http-equiv="imagetoolbar" content="false" />
<meta name="MSSmartTagsPreventParsing" content="true" />
<link rel="shortcut icon" href="../admin/favicon.ico" type="image/x-icon" />
<meta name="Copyright" content="Copyright (c) 2007 www.58china.cn" />
<meta name="Keywords" content="义乌生产资料网管理后台。" />
<meta name="Description" content="义乌生产资料网管理后台www.58china.cn" />
<link rel="stylesheet" href="style/css.css" type="text/css" media="all" />
<title>义乌生产资料网管理后台</title>
</head>
<body scroll="no" style="overflow-y:hidden">

<!-- 头部开始 -->
<div id="header">
<ul>
<li class="logo">
<div class="logo">
<h1><a title="返回义乌生产资料网首页" href="/"></a></h1>
</div>
</li>
<li class="name"></li>
<li class="topbg">
<div class="topbg">
&nbsp;
</div>
</li>
</ul>
</div>
<!-- 头部结束 -->

<!-- 内容开始 -->
<div id="content">
<ul>
<li class="left">
<div class="manage">
<div class="home">
<h1><a title="返回管理中心首页" href="../admin/index.html"></a></h1>
</div>
</div>
<div class="menutop"></div>
<div class="menucenter">

<IFRAME name="left" frameBorder="0" frameSpacing="0" height="316" width="150" marginHeight="0" marginWidth="0" scrolling="auto" 
src="left.asp" style="overflow-x:hidden"></IFRAME>

</div>
<div class="menubottom"></div>
<div class="exit"><a href="logout.asp"><img src="images/exit.gif" width="171" height="97" border="0" /></a></div>
</li>
<li class="right">
<div class="line"></div>
<IFRAME name="right" frameBorder="0" frameSpacing="0" height="480" width="100%" marginHeight="0" marginWidth="0" scrolling="auto" 
src="config.asp"></IFRAME>
</li>
</ul>
</div>
<!-- 内容结束 -->

<!-- footer开始 -->
<div id="footer">  版权所有 义乌生产资料网<br />
  Copyright©2000-2008 www.58china.cn. All Rights Reserved
</div>
<!-- footer结束 -->

</body>
</html>