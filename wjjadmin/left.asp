﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="UTF-8" />
<meta name="robots" content="all" />
<meta http-equiv="imagetoolbar" content="false" />
<meta name="MSSmartTagsPreventParsing" content="true" />
<link rel="shortcut icon" href="../admin/favicon.ico" type="image/x-icon" />
<meta name="Copyright" content="Copyright (c) 2007 www.88xsp.com" />
<meta name="Keywords" content="小商品资源，小商品，全球供应商。" />
<meta name="Description" content="www.88xsp.com,小商品全球供应商。" />
<link rel="stylesheet" href="style/css.css" type="text/css" media="all" />
<title>义乌生产资料网管理后台</title>
</head>
<body>

<!-- 内容开始 -->
<div class="menuicon"><h1><a href="javascript:void(0)" onclick="change('display'); return false">系统设置</a></h1></div>
<div id="display">
<div class="menucon">
  ·<a href="system/addoneadd.asp" target="right" name="right">地区设置</a> <br />
  ·<a href="system/links.asp" target="right" name="right">友情连接</a> <br />
  ·<a href="system/class.asp" target="right" name="right">类别管理</a> <br />
  ·<a href="system/moveclass.asp" target="right" name="right">移动分类</a> <br />
</div>
</div>
<div class="menuicon"><h1><a href="javascript:void(0)" onclick="change('display2'); return false">会员管理</a></h1></div>
<div id="display2" style="display:none;">
    <div class="menucon">
      ·<a href="menber/vip.asp" target="right" name="right">会员等级</a> <br />
	  ·<a href="menber/list.asp?ss=2" target="right" name="right">待审会员</a> <br />
	  ·<a href="menber/list.asp?ss=1" target="right" name="right">正常会员</a> <br />
      ·<a href="menber/list.asp" target="right" name="right">所有会员</a> <br />
    </div>
</div>
<div class="menuicon"><h1><a href="javascript:void(0)" onclick="change('display3'); return false">商品管理</a></h1></div>
<div id="display3" style="display:none;">
<div class="menucon">
  ·<a href="product/list.asp?ss=2" target="right" name="right">待审产品</a> <br />
  ·<a href="product/list.asp?ss=1" target="right" name="right">正常产品</a> <br />
  ·<a href="product/list.asp" target="right" name="right">所有产品</a> <br />
</div> 
</div>
<div class="menuicon"><h1><a href="javascript:void(0)" onclick="change('display4'); return false">新闻管理</a></h1></div>
<div id="display4" style="display:none;">
<div class="menucon">
  ·<a href="news/list.asp?ss=2" target="right" name="right">待审新闻</a> <br />
  ·<a href="news/list.asp?ss=1" target="right" name="right">正常新闻</a> <br />	
  ·<a href="news/list.asp" target="right" name="right">所有新闻</a> <br />
  </div>
</div>
<div class="menuicon"><h1><a href="javascript:void(0)" onclick="change('display5'); return false">供求信息</a></h1></div>
<div id="display5" style="display:none;">
<div class="menucon">
  ·<a href="records/list.asp?ss=2" target="right" name="right">待审供求</a> <br />
  ·<a href="records/list.asp?ss=1" target="right" name="right">正常供求</a> <br />	
  ·<a href="records/list.asp" target="right" name="right">所有供求</a> <br />
  </div>
</div>
<div class="menuicon"><h1><a href="javascript:void(0)" onclick="change('display12'); return false">管理员管理</a></h1></div>
<div id="display12" style="display:none;">
<div class="menucon">
  ·<a href="admin.asp" target="right" name="right">管理员管理</a>
  </div>
</div>
<!--<div class="menuicon"><h1><a href="javascript:void(0)" onclick="change('display14'); return false">广告管理</a></h1></div>
<div id="display14" style="display:none;">
<div class="menucon">
  ·<a href="ad/list.asp" target="right" name="right">广告管理</a></div>
</div>-->
<!-- 内容结束 -->


<script language="JavaScript">
function change(el) {
whichEl = document.getElementById(el)   //获得指定ID值的对象
if (whichEl.style.display == 'none') {   //block是默认值为显示,none为隐藏
   whichEl.style.display   = 'block'; 
} else {
     whichEl.style.display   = 'none';
   }
}
</script>
</body>
</html>