﻿<!--#include file="../top.asp"-->
<!--#include file="createdClassJs.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>管理员信息</title>
<link href="../style/css.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="/js/validator.js"></script>
</head>

<body>
<table border="0" cellspacing="0" cellpadding="0" class="MsgTbl">
  <tr>
    <td class="tbTitle">后台 > 系统信息</td>
  </tr>
</table>
<table class="MsgTbl" border="0" cellspacing="0" cellpadding="0">
 <tr>
    <td class="tbHeader">编号</td>
    <td class="tbHeader">小类</td>
    <td class="tbHeader">排序</td>
    <td class="tbHeader">状态</td>
    <td class="tbHeader">操作</td>
  </tr>
  <%
  	action=request.QueryString("action")
	pid = request.querystring("pid")
  	select case action
		case "del"
			id=request.QueryString("id")
			pid=request.QueryString("pid")
			del_record "clb","clbide="&int(id)&""
			response.Redirect("class1.asp?pid="&pid&"")
			response.End()
	end select
  	sql ="select * from clb where clbcla="&clng(pid)&" order by clbseq asc"
 	set rs=server.CreateObject("adodb.recordset")
	rs.open sql,conn,1,1
	if not rs.eof then 
		for i=1 to rs.recordcount
			 if rs("clbacti")="Y" then
			 	s = "开"
			else
				s = "关"
			end if
  %>
	  <tr>
		<td class="tbMsg"><%=rs("clbide")%></td>
		<td class="tbMsg"><%=rs("clbnam")%></td>
        <td class="tbMsg"><%=rs("clbseq")%></td>
        <td class="tbMsg"><%=s%></td>
		<td class="tbMsg"><a href="?action=del&amp;id=<%=rs("clbide")%>&amp;pid=<%=pid%>" onclick="return confirm('删除分类会删除此分类下的所有的相关信息不能恢复，确认吗？');">删除</a>
         | <a href="?action=ed&amp;id=<%=rs("clbide")%>&amp;pid=<%=pid%>#m">修改</a>
        <!-- | <a href="class1.asp?pid=<%=rs("clbide")%>&pid=<%=pid%>">添加子类</a>-->
         </td>
         
	  </tr>
  <%
	  rs.movenext
	  next
  end if 
  rs.close
  set rs=nothing
  %>
  <tr>
 	<td class="tbFooter" colspan="5"><a href="class1.asp?action=add&amp;pid=<%=pid%>#m">添加</a></td>
 </tr>
</table>
<%
action1=request.form("action1")
'response.write action1
if action1 = "edit" then
	update_record "clb","clbnam='"&request.form("clbnam")&"',clbseq="&request.form("clbseq")&",clbacti='"&request.form("clbacti")&"'","clbide="&cint(request.Form("id"))&""
	response.Redirect("class1.asp?pid="&request.form("clbcla")&"")
	response.End()
elseif action1 = "add" then 
	insert_record "clb","clbcla,clbnam,clbseq,clbacti",""&request.form("clbcla")&",'"&request.form("clbnam")&"',"&request.form("clbseq")&",'"&request.form("clbacti")&"'"
	response.Redirect("class1.asp?pid="&request.form("clbcla")&"")
	response.End()
end if 
if action="add" or action="ed" then
		if action = "ed" then 
		id = request.QueryString("id")
		set rsv = conn.execute("select top 1 * from clb where clbide="&cint(id))
		if not rsv.eof then 
			clbnam = rsv("clbnam")
			clbseq = rsv("clbseq")
			clbacti= rsv("clbacti")
		end if
		rsv.close
		set rsv = nothing
	elseif action="add" then 
		clbnam = ""
		clbseq = 0
		clbacti= "Y"
	end if
%>
<a name="m" id="m"></a>
<table border="0" cellspacing="0" cellpadding="0" class="MsgTbl">
<form action="class1.asp"  name="form1" method="post" onsubmit="return Validator.Validate(this,3);">
	<%if id<>"" then %>
		<input type="hidden" name="id" value="<%=id%>">
		<input type="hidden" name="action1" value="edit">
	<%else%>
		<input type="hidden" name="action1" value="add">
	<%end if%>
    <input type="hidden" name="clbcla" value="<%=pid%>">
 <tr>
    <td class="tbLabel">名称:</td>
    <td class="tbMsg"><input name="clbnam" type="text" value="<%=clbnam%>" dataType="Require" msg="请输入名称!"/></td>
  </tr>
  <tr>
    <td class="tbLabel">排序:</td>
    <td class="tbMsg"><input name="clbseq" type="text" value="<%=clbseq%>" dataType="Number" msg="请输入数量!"/></td>
  </tr>
  <tr>
    <td class="tbLabel">开放:</td>
    <td class="tbMsg">
    	<input name="clbacti" type="radio" value="Y" <%if trim(clbacti)="Y" then response.write "checked"%>/>是
        <input name="clbacti" type="radio" value="N" <%if trim(clbacti)="N" then response.write "checked"%>/>否
    </td>
  </tr>
  
  <tr>
    <td class="tbLabel"></td>
    <td class="tbMsg"><input name="submit" type="submit" value="提交" />
	<input name="submit" type="reset" value="重置" /></td>
  </tr>
 </form>
</table>
<%end if %>
<%call createdjs()
call closeconn()%>
<br>
</body>
</html>
