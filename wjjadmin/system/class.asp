﻿<!--#include file="../top.asp"-->
<!--#include file="createdClassJs.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>管理员信息</title>
<link href="../style/css.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="/js/validator.js"></script>
</head>

<body>
<table border="0" cellspacing="0" cellpadding="0" class="MsgTbl">
  <tr>
    <td class="tbTitle">后台 > 系统信息</td>
  </tr>
</table>
<table class="MsgTbl" border="0" cellspacing="0" cellpadding="0">
 <tr>
    <td class="tbHeader">编号</td>
    <td class="tbHeader">大类</td>
    <td class="tbHeader">排序</td>
    <td class="tbHeader">状态</td>
    <td class="tbHeader">操作</td>
  </tr>
  <%
  	action=request.QueryString("action")
  	select case action
		case "del"
			id=request.QueryString("id")
			del_record "cla","claide="&int(id)&""
			response.Redirect("class.asp")
			response.End()
	end select
  	sql ="select * from cla order by claseq asc"
 	set rs=server.CreateObject("adodb.recordset")
	rs.open sql,conn,1,1
	if not rs.eof then 
		for i=1 to rs.recordcount
			 if rs("claacti")="Y" then
			 	s = "开"
			else
				s = "关"
			end if
  %>
	  <tr>
		<td class="tbMsg"><%=rs("claide")%></td>
		<td class="tbMsg"><%=rs("clanam")%></td>
        <td class="tbMsg"><%=rs("claseq")%></td>
        <td class="tbMsg"><%=s%></td>
		<td class="tbMsg"><a href="?action=del&amp;id=<%=rs("claide")%>" onclick="return confirm('删除分类会删除此分类下的所有的相关信息不能恢复，确认吗？');">删除</a>
         | <a href="?action=ed&amp;id=<%=rs("claide")%>#m">修改</a>
         | <a href="class1.asp?pid=<%=rs("claide")%>">添加子类</a>
         </td>
         
	  </tr>
  <%
	  rs.movenext
	  next
  end if 
  rs.close
  set rs=nothing
  %>
  <tr>
 	<td class="tbFooter" colspan="5"><a href="class.asp?action=add#m">添加</a></td>
 </tr>
</table>
<%
action1=request.form("action1")
'response.write action1
if action1 = "edit" then
	update_record "cla","clanam='"&checkstr(request.form("clanam"))&"',claseq="&request.form("claseq")&",claacti='"&request.form("claacti")&"'","claide="&cint(request.Form("id"))&""
	response.Redirect("class.asp")
	response.End()
elseif action1 = "add" then 
	insert_record "cla","clanam,claseq,claacti","'"&checkstr(request.form("clanam"))&"',"&request.form("claseq")&",'"&request.form("claacti")&"'"
	response.Redirect("class.asp")
	response.End()
end if 
if action="add" or action="ed" then
		if action = "ed" then 
		id = request.QueryString("id")
		set rsv = conn.execute("select top 1 * from cla where claide="&cint(id))
		if not rsv.eof then 
			clanam = rsv("clanam")
			claseq = rsv("claseq")
			claacti= rsv("claacti")
		end if
		rsv.close
		set rsv = nothing
	elseif action="add" then 
		clanam = ""
		claseq = 0
		claacti= "Y"
	end if
%>
<a name="m" id="m"></a>
<table border="0" cellspacing="0" cellpadding="0" class="MsgTbl">
<form action="class.asp"  name="form1" method="post" onsubmit="return Validator.Validate(this,3);">
	<%if id<>"" then %>
		<input type="hidden" name="id" value="<%=id%>">
		<input type="hidden" name="action1" value="edit">
	<%else%>
		<input type="hidden" name="action1" value="add">
	<%end if%>
 <tr>
    <td class="tbLabel">名称:</td>
    <td class="tbMsg"><input name="clanam" type="text" value="<%=clanam%>" dataType="Require" msg="请输入名称!"/></td>
  </tr>
  <tr>
    <td class="tbLabel">排序:</td>
    <td class="tbMsg"><input name="claseq" type="text" value="<%=claseq%>" dataType="Number" msg="请输入数量!"/></td>
  </tr>
  <tr>
    <td class="tbLabel">开放:</td>
    <td class="tbMsg">
    	<input name="claacti" type="radio" value="Y" <%if trim(claacti)="Y" then response.write "checked"%>/>是
        <input name="claacti" type="radio" value="N" <%if trim(claacti)="N" then response.write "checked"%>/>否
    </td>
  </tr>
  
  <tr>
    <td class="tbLabel"></td>
    <td class="tbMsg"><input name="submit" type="submit" value="提交" />
	<input name="submit" type="reset" value="重置" /></td>
  </tr>
 </form>
</table>
<%end if %>
<%call createdjs()
call closeconn()%>
<br>
</body>
</html>
