﻿<!--#include file="../top.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>管理员信息</title>
<link href="../style/css.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="/js/validator.js"></script>
<script language="javascript" src="/js/jquery.js"></script>
</head>

<body>
<%
act=request.QueryString("act")
if act="save" then
	bid=request.Form("anclassid1")
	sid=request.Form("Nclassid")
	bid2=request.Form("anclassid2")
	sid2=request.Form("Nclassid2")
	if not chkrequest(bid) or not chkrequest(sid) then alert "请选择产品源分类","",1
	if not chkrequest(bid2) or not chkrequest(sid2) then alert "请选择产品目标分类","",1
	sql="update que set queclb="&sid2&" where queclb="&sid
	conn.execute(sql)
	sql="update pro set proclb="&sid2&" where proclb="&sid
	conn.execute(sql)
	sql="update pie set pieclb="&sid2&" where pieclb="&sid
	conn.execute(sql)
	alert "移动分类成功！","",1
end if
dim count
set rs=server.createobject("adodb.recordset")
rs.open "select clbcla,clbide,clbnam from clb order by clbseq asc",conn,1,1%>
<script language = "JavaScript">
var onecount;
onecount=0;
subcat = new Array();
<%
   count = 0
   do while not rs.eof 
%>
subcat[<%=count%>] = new Array("<%= trim(rs("clbnam"))%>","<%= rs("clbcla")%>","<%= rs("clbide")%>");
<%
        count = count + 1
        rs.movenext
        loop
        rs.close
%>
onecount=<%=count%>;
function changelocation(locationid)
    {
    document.myform.Nclassid.length = 0;    
    var i;
    for (i=0;i < onecount; i++)
        {
            if (subcat[i][1] == locationid)
            { 
             document.myform.Nclassid.options[document.myform.Nclassid.length] = new Option(subcat[i][0], subcat[i][2]);
            }        
        }
    }   
function changelocation2(locationid)
    {
    document.myform.Nclassid2.length = 0;    
    var i;
    for (i=0;i < onecount; i++)
        {
            if (subcat[i][1] == locationid)
            { 
             document.myform.Nclassid2.options[document.myform.Nclassid2.length] = new Option(subcat[i][0], subcat[i][2]);
            }        
        }
    }  
function chkClassInfo(id)
{	
	$.ajax({
	type:"get",	
	dataType:"html",
	cache:"false",		
	url:"getClassinfo.asp?id="+id,
	beforeSend:function(XMLHttpRequest){
		$("#spinfo").html("<img src='http://www.xsp2.cn/jquery/images/loading.gif' border='0' />正在检查分类下的信息。。。");
		},
	success:function(data, textStatus){	
		$("#spinfo").html(data);			
		},
	complete: function(XMLHttpRequest, textStatus){
			//HideLoading();
		},
	error: function(){			
		}
	
	});
}
</script>
<table border="0" cellspacing="0" cellpadding="0" class="MsgTbl">
  <tr>
    <td class="tbTitle">后台 > 移动分类</td>
  </tr>
</table>
<table class="MsgTbl" border="0" align="center"  >
          <form name="myform" method="post" action="?act=save">
		  <tr > 
            <td width="15%" align="right">选择源分类：</td>
            <td width="85%" align="left"> 
              <%
	rs.open "select * from cla order by claseq",conn,1,1
	if rs.eof and rs.bof then
	response.write "请先添加栏目。"
	response.end
	else
%>
              大类： 
              <select name="anclassid1" size="1" id="anclassid" onChange="changelocation(document.myform.anclassid1.options[document.myform.anclassid1.selectedIndex].value)">
                <option selected value="<%=rs("claide")%>"><%=trim(rs("clanam"))%></option>
                <%      dim selclass
         selclass=rs("claide")
        rs.movenext
        do while not rs.eof
%>
                <option value="<%=rs("claide")%>"><%=trim(rs("clanam"))%></option>
                <%
        rs.movenext
        loop
		end if
        rs.close
%>
              </select>
              　 小类： 
              <select name="Nclassid">
                <%rs.open "select * from clb where clbcla="&selclass ,conn,1,1
if not(rs.eof and rs.bof) then
%>
                <option selected value="<%=rs("clbide")%>"><%=rs("clbnam")%></option>
                <% rs.movenext
do while not rs.eof%>
                <option value="<%=rs("clbide")%>"><%=rs("clbnam")%></option>
                <% rs.movenext
loop
end if
        rs.close
        
%>
              </select>&nbsp;<input type="button" value="检测此分类下的信息" onclick="chkClassInfo(document.getElementById('Nclassid').value);" /><br />(<font color="#FF0000">注：移动分类移动供求信息、产品信息、报价信息的分类。须谨慎操作！</font>)</td>
</tr>
 <tr > 
            <td  align="right">选择目标分类：</td>
            <td  align="left"> 
              <%
	rs.open "select * from cla order by claseq",conn,1,1
	if rs.eof and rs.bof then
	response.write "请先添加栏目。"
	response.end
	else
%>
              大类： 
              <select name="anclassid2" size="1" id="anclassid" onChange="changelocation2(document.myform.anclassid2.options[document.myform.anclassid2.selectedIndex].value)">
                <option selected value="<%=rs("claide")%>"><%=trim(rs("clanam"))%></option>
                <%     
         selclass=rs("claide")
        rs.movenext
        do while not rs.eof
%>
                <option value="<%=rs("claide")%>"><%=trim(rs("clanam"))%></option>
                <%
        rs.movenext
        loop
		end if
        rs.close
%>
              </select>
              　 小类： 
              <select name="Nclassid2">
                <%rs.open "select * from clb where clbcla="&selclass ,conn,1,1
if not(rs.eof and rs.bof) then
%>
                <option selected value="<%=rs("clbide")%>"><%=rs("clbnam")%></option>
                <% rs.movenext
do while not rs.eof%>
                <option value="<%=rs("clbide")%>"><%=rs("clbnam")%></option>
                <% rs.movenext
loop
end if
        rs.close
        set rs = nothing
		conn.close
		set conn=nothing
%>
              </select>&nbsp;<input type="button" value="检测此分类下的信息" onclick="chkClassInfo(document.getElementById('Nclassid2').value);" /><br /><input type="submit" onclick="return confirm('注意：批量移动产品有一定危险，须谨慎，确定进行此操作吗？');" value="提交" />&nbsp;<span id="spinfo" style="color:#ff0000"></span></td>
</tr>
</form>
</table>
</body>
</html>
