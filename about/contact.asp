﻿<!--#include file="../inc/top.asp"-->
<meta name="Keywords" content="二手机械设备转让 二手机械设备回收 二手机械转让 二手机械回收 二手机械处理 二手机床转让 二手机床回收 二手机床处理 二手设备转让 二手设备回收 二手针织机械转让回收 二手工程设备转让回收 二手化工设备转让回收 二手服装设备转让回收 二手印刷设备转让回收" />
<meta name="Description" content="中国专业的二手设备转让 二手机械转让 二手机器转让 库存机械处理 库存设备处理 二手设备回收 二手机械回收 二手机床转让 二手机床回收 二手针织机械转让 二手化工设备转让 二手工程设备回收转让交易平台" />
<link rel="stylesheet" href="../style/about.css" type="text/css" media="all" />
<title>联系我们 义乌生产资料网</title>
</head>
<body>
<!--#include file="head.asp"-->


<!-- 内容开始 -->
<FIELDSET style="margin:0 auto; padding:30px 0 60px 0; width:930px; text-align:left;">
<div id="content">
<div class="content_left">
<!--#include file="left.asp"-->

</div>
<div class="content_right">
<h1 class="bt">联系我们</h1>
您有任何疑问和问题，欢迎您同我们联系。义乌生产资料网全体服务团队竭诚为您服务。<br />
工作时间：周一至周六8：30～17：30<br />
联系电话：0579-85367402    400—8877—978 (免长途话费)<br />
客服传真：0579-85177399<br />
联系地址：浙江省义乌市江东四区119幢一单元二楼<br />
邮政编码：322000</div>
</div>
</FIELDSET>
<!-- 内容结束 -->

<!-- 尾部开始 -->
<script language="javascript" src="/js/footer.js"></script>

</body>
</html>