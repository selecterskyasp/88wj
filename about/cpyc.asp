﻿<!--#include file="../inc/top.asp"-->
<meta name="Keywords" content="二手机械设备转让 二手机械设备回收 二手机械转让 二手机械回收 二手机械处理 二手机床转让 二手机床回收 二手机床处理 二手设备转让 二手设备回收 二手针织机械转让回收 二手工程设备转让回收 二手化工设备转让回收 二手服装设备转让回收 二手印刷设备转让回收" />
<meta name="Description" content="中国专业的二手设备转让 二手机械转让 二手机器转让 库存机械处理 库存设备处理 二手设备回收 二手机械回收 二手机床转让 二手机床回收 二手针织机械转让 二手化工设备转让 二手工程设备回收转让交易平台" />
<link rel="stylesheet" href="../style/about.css" type="text/css" media="all" />
<title>诚聘英才 义乌生产资料网</title>
</head>
<body>
<!--#include file="head.asp"-->


<!-- 内容开始 -->
<FIELDSET style="margin:0 auto; padding:30px 0 60px 0; width:930px; text-align:left;">
<div id="content">
<div class="content_left">
<!--#include file="left.asp"-->

</div>
<div class="content_right">
<h1 class="bt">诚聘英才</h1>
销售经理 <br />
1、大专学历以上，市场营销或电子商务相关专业。<br />
2、4年以上销售和管理经验。<br />
3、熟悉大型电子商务平台销售流程，具备很强的团队管理能力。<br />
4、具有带领10-20人的销售团队管理经验，有较强的商务谈判能力和激励团队战斗力。<br />
5、对二手机械、设备、库存行业等行业有较深的认识和兴趣。<br />
欢迎天下英才加盟义乌生产资料网！</div>
</div>
</FIELDSET>
<!-- 内容结束 -->

<!-- 尾部开始 -->
<script language="javascript" src="/js/footer.js"></script>

</body>
</html>