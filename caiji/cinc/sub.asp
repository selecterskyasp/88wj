<%
'子程序功能：得到公司的信息，并将产品类别列表的代码存入缓存
'返回数组 0用户名 1公司名称 2联系人 3联系电话 4传真号码 5邮箱 6联系地址 7主营行业  8企业介绍
'9 企业类型 10移动电话 11邮政编码 12Logo远程图片地址 13对方企业ID号 
function getCompanyInfo(byVal surl)
dim tArr(13),tstr,tbody,tpos
dim contacturl,findstr
tpos=1
tbody=getHttpPage(surl)

'ID号
tstr=getBody(tbody,1,"window.external.addFavorite('index-",".html",false,false)
tarr(13)=tstr(1)
erase tstr

'用户名
tstr=getBody(tbody,1,"企业地址:<a href='http://",".fengj.com'",false,false)
tarr(0)=tstr(1)
erase tstr

'公司名称
tstr=getBody(tbody,1,"<title>","</title>",false,false)
tarr(1)=tstr(1)
erase tstr

'联系人
tstr=getBody(tbody,1,"联系人：</strong></div>","&nbsp;</div>",false,false)
tarr(2)=getinnertext(tstr(1))
erase tstr

'移动电话
tstr=getBody(tbody,1,"移动电话：</strong></div>","&nbsp;</div>",false,false)
tarr(10)=getinnertext(tstr(1))
erase tstr

'联系电话
tstr=getBody(tbody,1,"联系电话：</strong></div>","&nbsp;</div>",false,false)
tarr(3)=getinnertext(tstr(1))
erase tstr

'传真
tstr=getBody(tbody,1,"传真：</strong></div>","&nbsp;</div>",false,false)
tarr(4)=getinnertext(tstr(1))
erase tstr

'电子邮箱
tstr=getBody(tbody,1,"电子邮箱：</strong></div>","&nbsp;</div>",false,false)
tarr(5)=getinnertext(tstr(1))
erase tstr

'联系地址
tstr=getBody(tbody,1,"联系地址：</strong></div>","&nbsp;</div>",false,false)
tarr(6)=getinnertext(tstr(1))
erase tstr

'邮编
tstr=getBody(tbody,1,"邮编：</strong></div>","&nbsp;</div>",false,false)
tarr(11)=getinnertext(tstr(1))
erase tstr

contacturl="http://shop.fengj.com/introduce-"&tarr(13)&".html"
tbody=getHttpPage(contacturl)

'logo
tstr=getBody(tbody,1,"<div style=""width:300px; float:right; z-index:2;""><img src=""","""",false,false)
tarr(12)=getinnertext(tstr(1))
if instr(tarr(12),"uploadfile")>0 then
	'替换成大图
	tarr(12)=replace(tarr(12),"/uploadfile/","/uploadfile/big/")
else
	'没有logo图
	tarr(12)=""
end if
erase tstr

if tarr(12)="" then
findstr="<table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">"&vbcrlf
findstr=findstr&"			  <tr>"&vbcrlf
findstr=findstr&"				<td>"&vbcrlf
else
findstr="width=""300"" /></div>"
end if
endstr="</td>"&vbcrlf
endstr=endstr&"			  </tr>"&vbcrlf
endstr=endstr&"			</table>"&vbcrlf

'公司介绍
tstr=getBody(tbody,1,findstr,endstr,false,false)
tarr(8)=getinnertext(tstr(1))
erase tstr


'主营业务或服务
tstr=getBody(tbody,1,"主营业务或服务：</strong></div>","&nbsp;</div>",false,false)
tarr(7)=left(getinnertext(tstr(1)),10)
erase tstr

getCompanyInfo=tarr
erase tarr
end function

'得到产品列表 返回列表的html代码
function getProList(surl)
dim tbody,tstr,tpos,tpos2,tcontent
dim tstr1
dim tarr,turl
'产品列表数组 0图片地址 1产品名称 2产品地址
redim tproList(2,0)
dim ti,page
tbody=getHttppage(surl)
tarr=getBody(tbody,1,"<div class=""cen_l_1_6_","</div>",false,false)
tstr=tarr(1)
tpos=tarr(2)
erase tarr
ti=0
tcontent=""
tcontent=tcontent&""&vbcrlf

do while tstr<>"" and tpos<>-1 and ti<=100
'图片地址
tarr=getBody(tstr,1,"<img src=""","""",false,false)
if tarr(1)<>"" and instr(tarr(1),"wutu.jpg")=0 then
redim preserve tprolist(2,ti)

tprolist(0,ti)=tarr(1)
erase tarr
'产品链接地址
tarr=getBody(tstr,1,"href=""","""",false,false)
tprolist(1,ti)=tarr(1)
tpos2=tarr(2)
erase tarr
'产品名称
tarr=getBody(tstr,1,"<li class=""cen_l_1_6_2_li2"" style=""overflow:hidden;"">","</a></li>",false,false)
tprolist(2,ti)=getinnertext(tarr(1))
erase tarr
tcontent=tcontent&"<tr>"&vbcrlf
tcontent=tcontent&"<td><img src="""&tprolist(0,ti)&""" width=""110"" height=""90""></td>"&vbcrlf
tcontent=tcontent&"<td><a href="""&tprolist(1,ti)&""" target=""_blank"">"&tprolist(2,ti)&"</td>"&vbcrlf
tcontent=tcontent&"<td><input type=""checkbox"" name=""ID"" value="""&tprolist(1,ti)&""" style=""border:0"">"&vbcrlf
tcontent=tcontent&"</tr>"&vbcrlf
end if
tarr=getBody(tbody,tpos,"<div class=""cen_l_1_6_","</div>",false,false)
tstr=tarr(1)
tpos=tarr(2)
erase tarr
ti=ti+1
loop
if instr(surl,"page")=0 then
	tarr=getBody(tbody,1,"条&nbsp;共","页&nbsp;当前第",false,false)
	tstr=tarr(1)	
	erase tarr	
	if not chkrequest(tstr) then tstr=1
	application.lock()
	application("page")=tstr
	application.unlock()
end if

tarr=getBody(surl,1,"page_no=","&company_id",false,false)
tstr=tarr(1)
tpos=tarr(2)
erase tarr
dim curpage
if tstr="" then curpage=1 else curpage=cint(tstr)

tstr=""
for page=1 to cint(application("page"))	
	if page=curpage then
	tstr=tstr&"<font color=red>第"&page&"页</font>&nbsp;"
	else	
	tstr=tstr&"<a href="""&home&"?act=3&proUrl="&server.URLEncode("http://shop.fengj.com/sell.asp?page_no="&page&"&company_id="&application("id")&"&class_2_id=")&""">第"&page&"页</a>&nbsp;"
	end if
next
tcontent=tcontent&"<tr>"&vbcrlf
tcontent=tcontent&"<td colspan=""3"">当前分类下共有 <strong>"&ti-1&"</strong> 个产品&nbsp;"&tstr&"</td>"&vbcrlf
tcontent=tcontent&"</tr>"&vbcrlf
getProList=tcontent
end function

'得到产品详细信息 返回产品详细信息数组
'0产品图片 1产品名称 2产品价格 3产品规格 4产品数量 5产品描述
function getProinfo(sproUrl)
dim tbody,tstr,tpos
'产品详细信息数组 
dim tproInfoarr(5),tarr
tbody=getHttppage(sproUrl)
'得到产品图片
tarr=getBody(tbody,1,"var pics=""",""";",false,false)
tstr=tarr(1)
erase tarr

if tstr<>"" then
tarr=split(tstr,"|")
tstr=tarr(0)
'替换成大图
tstr=replace(tstr,"/uploadfile/","/uploadfile/big/")
erase tarr
end if

tproinfoarr(0)=tstr
'产品名称
tarr=getBody(tbody,1,"产品名称：</strong>","</td>",false,false)
tstr=tarr(1)
erase tarr
tproinfoarr(1)=tstr

'产品价格
tarr=getBody(tbody,1,"价格范围：</strong>","</td>",false,false)
tstr=tarr(1)

if not chkrequest(tstr) then tstr=0
erase tarr
tproinfoarr(2)=tstr

'产品规格
tarr=getBody(tbody,1,"成　　色：</div>","</div>",false,false)
tstr=getinnertext(tarr(1))
tarr=empty
tproinfoarr(3)=tstr

'产品材质
tarr=getBody(tbody,1,"产品数量：</div>","</div>",false,false)

tstr=getinnertext(tarr(1))
tstr=getNum(tstr)

if not chkrequest(tstr) then tstr=0
erase tarr
tproinfoarr(4)=tstr

'描述
tarr=getBody(tbody,1,"<td valign=""top""style=""line-height:25px;"">","</td>",false,false)
tstr=tarr(1)
erase tarr
if len(tstr)<5 then tproinfoarr(5)=tproinfoarr(1) else tproinfoarr(5)=tstr

getProinfo=tproinfoarr
end function
%>