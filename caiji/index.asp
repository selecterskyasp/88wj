<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
Response.Buffer=TRUE %>
<!--#include file="../inc/conn.asp"-->
<!--#include file="cinc/function.asp"-->
<!--#include file="cinc/sub.asp"-->
<!--#include file="../inc/md5.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>fengj.com商铺产品采集插件 by cjx</title>
<style type="text/css">
body{margin:0;padding:0;border:0px none;background:#fff;color:#000000;font-size:12px;font-family:'宋体',arial,'Lucida Grande','Lucida Sans Unicode','新宋体',verdana,sans-serif;line-height:180%;}	
	select,input{font-size:12px;color:#000;}
	h1{margin:0;font-size:14px;color:#1b49ac; margin-top:15px}	
	A:link,A:visited,A:active{ text-decoration: none;color: #111;}
	input,textarea{
	border:1px solid #e5e5e5;	
	margin: 1px 0px 4px 0px;	
	color: #333;	
	padding:2px 0px 0px 3px;	
}
	td{ height:22px}
textarea:focus,input:focus{	
	border: 1px solid #FFFF00;
}
.tbHeader { height: 22px; background-color:#CEDFEF; text-align:center; }
</style>
<script language="javascript" type="text/javascript" src="js/jquery-1.1.3.pack.js"></script>
<script language="javascript" type="text/javascript" src="js/jQuery.FillOptions.js"></script>
<script language="javascript" type="text/javascript" src="js/jQuery.CascadingSelect.js"></script>
<script language="javascript" src="/js/validator.js"></script>
</head>
<body>
<%

'coArr 公司属性数组 0用户名 1公司名称 2联系人 3联系电话 4传真号码 5邮箱 6联系地址 7主营行业  8企业介绍
'9 企业类型 10移动电话 11邮政编码 12Logo远程图片地址 13对方企业ID号
'coConstArr 0行业大分类 1行业小分类 2城市大分类 3城市小分类 4经营方式 5注册成功后的企业ID号 6上传后的企业Logo图片地址
'cpArr 产品属性数组 0产品名称 1产品图片 2产品型号 3产品规格 4产品用途 5产品材质 6产品产地 7产品单价
'cpListArr 产品列表数组 0 产品名称 1产品图片
'coNum当天添加的企业个数 coNumall 12个月内添加的企业个数 proNum当前添加的产品个数 proNumall 12个月内添加的产品个数 全部保存在cookies里面
'proExp 产品application过期时间
dim rs,content,act,coNum,proNum,proExp,coNumall,proNumall,tstr
dim coArr,coConstArr(5),cpArr(7),cpListArr(1,0)
dim url
dim i
const home="index.asp"
coNum=0:proNum=0:coNumall=0:proNumall=0
act=request.QueryString("act")
set rs=server.CreateObject("adodb.recordset")
if isNUll(act) or act="" then act=1 else act=cint(act)

if request.Cookies("jianyingLogin")<>"yes" and act<>0 then
call Login()
set rs=nothing
conn.close
set conn=nothing
response.End()
end if

'读取历史添加的企业总数
if not isfileExists("Count.txt") then
	coNumall=0
	proNumall=0
else
	tstr=ReadFromUTF("Count.txt","utf-8")
	if instr(tstr,",")<1 or len(tstr)<3 then
		coNumall=0
		proNumall=0
	else
		tstr=split(tstr,",")
		coNumall=tstr(0)
		proNumall=tstr(1)
		if not chkrequest(coNumall) then coNumall=0 else coNumall=clng(coNumall)
		if not chkrequest(proNumall) then proNumall=0 else proNumall=clng(proNumall)
	end if
end if

'读取当天产品添加的产品数
proNum=application("proNum")
proExp=application("proExp")
if not chkrequest(proNum) or not isdate(proExp) then
	proNum=0
	application.Lock()
	application("proNum")=proNum
	application("proExp")=dateadd("h",8,now())
	application.UnLock()
else
	if	datediff("h",proExp,now())>8 then
		proNum=0
		application.Lock()
		application("proNum")=proNum
		application("proExp")=dateadd("h",8,now())
		application.UnLock()
	else
		proNum=clng(proNum)	
	end if
end if

'当天添加的公司
coNum=request.Cookies("coNum")
if not chkrequest(coNum) then 
	coNum=0 
	response.Cookies("coNum")=coNum
	Response.Cookies("coNum").Expires=DateAdd("h", 8, Now())
else 
	coNum=clng(coNum)
end if

select case act
case 0
call chkLogin()
case 1
call act1()
case 2
call act2()
case 3
call act3()
case 4
call act4()
case 10
call logout()
end select
set rs=nothing
conn.close
set conn=nothing
call foot()
%>

<%sub act1()%>
<div>
<h1>第一步 输入网址</h1>
<form action="<%=home%>?act=2" method="post" onsubmit="return Validator.Validate(this,2);">
  <table width="600" border="0" cellspacing="0" cellpadding="0">
  <tr>
      <td>公司网址</td>
      <td><input name="furl" type="text" id="furl" size="60" maxlength="100" dataType="Url" msg="输入正确的网址"/>
        <font color="#FF0000">如:http://slhg.fengj.com/</font></td>
    </tr>
    <tr>
      <td>经营模式</td>
      <td><select name="cojy" dataType="Require" msg="请选择所属行业!">
		<option value="">请选择</option>
		<option value="1">生产型</option>
		<option value="2">贸易型</option>
		<option value="3">服务型</option>
		<option value="4">其它</option>
</select></td>
    </tr>
    
	    <tr>
      <td>所在行业</td>
      <td>
      <select name="bClass" id="bClass" style="width:100px" dataType="Require" msg="请选择产品大分类">
</select>&nbsp;<select name="sClass" id="sClass" style="width:100px" dataType="Require" msg="请选择产品小分类"></select>
</td>
    </tr>
    <tr>
      <td>所在地区</td>
      <td><select name="addone_id" id="addone_id" style="width:100px" dataType="Require" msg="请选择您所在的省份!"></select>
		<select name="addtwo_id" id="addtwo_id" style="width:100px" dataType="Require" msg="请选择您所在的城市!" ></select> </td>
    </tr>
	 
	<tr>
      <td colspan="2"><label>
        <input type="submit" name="Submit" value="提交" />
        <input type="reset" name="Submit2" value="重置" />
      </label></td>    
    </tr>
  </table>
  </form>
 <script language="javascript" type="text/javascript">
    
    $(function(){        
            $("#bClass").FillOptions("/inc/getclass.asp",{datatype:"json",textfield:"name",valuefiled:"id"});
			$("#addone_id").FillOptions("/inc/getcity.asp",{datatype:"json",textfield:"name",valuefiled:"id"});
    });
    
	$("#bClass").CascadingSelect(
							$("#sClass"),
							"/inc/getclass.asp",
							{datatype:"json",textfield:"name",valuefiled:"id",parameter:"p"},
							function(){}
			);
			
	$("#addone_id").CascadingSelect(
							$("#addtwo_id"),
							"/inc/getcity.asp",
							{datatype:"json",textfield:"name",valuefiled:"id",parameter:"p"},
							function(){}
			);		
    </script>
</div>
<%end sub%>

<%sub act2()
dim bclassid,sclassid,addone_id,addtwo_id,cojy
dim coID,tstr
dim tmpArr
dim cache
url=request.Form("furl")
bclassid=request.Form("bclass")
sclassid=request.Form("sclass")
addone_id=request.Form("addone_id")
addtwo_id=request.Form("addtwo_id")
cojy=request.Form("cojy")
if not chkUrl(url) then
call showErrmsg("目标网址输入不正确","")
end if
if not chkrequest(bclassid) or not chkrequest(sclassid) then
call showErrmsg("没有选择企业行业分类","")
end if

if len(addtwo_id)<>6 then
call showErrmsg("没有选择省份城市","")
end if

if not chkrequest(cojy) then
call showErrmsg("请选择所属行业","")
end if

coConstArr(0)=bclassid
coConstArr(1)=sclassid
coConstArr(2)=addone_id
coConstArr(3)=addtwo_id
coConstArr(4)=cojy

coArr=getCompanyInfo(url)

'加入缓存
application.Lock()
application("sclassid")=sclassid
application("addtwo_id")=addtwo_id
application("cojy")=cojy
application("url")=url
application("id")=coArr(13)
application.UnLock()
%>
<div>
<h1>第二步 确认公司信息</h1>
<form action="<%=home%>?act=3" method="post" onsubmit="return Validator.Validate(this,2);">
  <table width="600" border="0" cellspacing="0" cellpadding="0">
  <tr>
      <td>目标网址</td>
      <td><a href="<%=url%>" target="_blank"><%=url%></a></td>
    </tr>
    <tr>
      <td>企业ID：</td>
      <td><%=coArr(13)%>
        </td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>    
    </tr>
	<tr>
      <td colspan="2"><strong>联系信息</strong></td>    
    </tr>
    
	<tr>
      <td>用户名</td>
      <td><input maxlength="20" size="30" name="f_user" dataType="Username" msg="请输入4到20个字符的登录名!" value="<%=coArr(0)%>"/>
        <font color="#FF0000">*</font></td>
    </tr>  <tr>
      <td>企业LOGO</td>
      <td><%=coArr(12)%>
	  <input type="hidden" name="f_logo" value="<%=coArr(12)%>" />
        <a href="<%=coArr(12)%>" target="_blank">查看</a></td>
    </tr>   
	<tr>
      <td>联系人</td>
      <td><input maxlength="8" size="30" name="f_lxr" dataType="Require" msg="联系人是必须!" value="<%=coArr(2)%>"/>
        <font color="#FF0000">*</font></td>
    </tr>
	<tr>
      <td>联系电话</td>
      <td>
   <input maxlength="15" name="f_tel" dataType="LimitB" max="15" min="6" msg="请输入正确联系电话号码" value="<%=coArr(3)%>"/>
   <font color="#FF0000">*</font></td>
    </tr>
	<tr>
      <td>传真号码</td>
      <td>
   <input maxlength="10" name="f_fax" value="<%=coArr(4)%>"/></td>
    </tr>
		<tr>
      <td>手机号码</td>
      <td><input maxlength="13" name="f_mobile" value="<%=coArr(10)%>"/>  </td>
    </tr>
		<tr>
      <td>邮箱</td>
      <td><input maxlength="13" name="f_email" value="<%=coArr(5)%>" dataType="Email" msg="必须为有效的邮箱"/> <font color="#FF0000">*</font> </td>
    </tr>
		<tr>
      <td>联系地址</td>
      <td><input maxlength="60" name="f_address" style="width:300px" value="<%=coArr(6)%>"/>
        <font color="#FF0000">*</font> </td>
    </tr>
	<tr>
      <td colspan="2"><strong>企业信息</strong></td>    
    </tr>
	 <tr>
      <td>公司名称</td>
      <td><input maxlength="40" name="f_cpname" dataType="LimitB" max="40" min="4" msg="请输入正确的公司名字" style="width:300px" value="<%=coArr(1)%>"/>
        <font color="#FF0000">*</font></td>
    </tr>
	 <tr>
      <td>经营行业</td>
      <td><input name="f_jy" dataType="LimitB" max="15" min="1" msg="请输入经营行业" value="<%=coArr(7)%>"/>
        <font color="#FF0000">*</font></td>
    </tr>	
	<tr>
      <td>企业介绍</td>
      <td><textarea name="f_js" cols="55" rows="5"><%=coArr(8)%></textarea><font color="#FF0000">*</font></td>
    </tr>
	<tr>
      <td colspan="2"><label>	 	
		<input type="hidden" name="actfl" value="saveyp" />
        <input type="submit" name="Submit" value="添加企业进入产品列表" />
		<input type="submit" name="Submit" value="不添加企业进入产品列表" />
        <input type="button" name="Submit2" value="返回" onclick="history.back()"/>
      </label></td>    
    </tr>
  </table>
  </form>  
</div>
<%end sub%>

<%sub act3()
dim bclassid,sclassid,addone_id,addtwo_id,cojy,f_market,f_marketid
dim f_user,f_lxr,f_telq,f_tel,f_faxq,f_fax,f_mobile,f_qq,f_address,f_cpname,f_jy,f_fr,f_zb,f_js,f_logo
dim actfl
dim cache
dim finderr,errmsg
dim ypid,sql
dim filename
actfl=request("actfl")
sclassid=application("sclassid")
addtwo_id=application("addtwo_id")
cojy=application("cojy")
if not chkrequest(sclassid) then
call showErrmsg("没有选择企业行业分类","index.asp")
end if
if len(addtwo_id)<>6 then
call showErrmsg("没有选择省份城市","")
end if

if not chkrequest(cojy) then
call showErrmsg("请选择所属行业","")
end if

if actfl="saveyp" and request.Form("Submit")="添加企业进入产品列表" then
	f_user=request.Form("f_user")	
	f_lxr=request.Form("f_lxr")	
	f_tel=request.Form("f_tel")	
	f_fax=request.Form("f_fax")
	f_mobile=request.Form("f_mobile")
	f_email=request.Form("f_email")
	f_address=request.Form("f_address")	
	f_cpname=request.Form("f_cpname")
	f_jy=request.Form("f_jy")		
	f_js=request.Form("f_js")
	f_logo=request.Form("f_logo")
	
	finderr=false
	errmsg=""
	if not isValidName(f_user) then
	finderr=true
	errmsg=errmsg&"用户名不正确(4-20以字母开头，数字和下划线组合)\n"
	end if
	if not chkRange(f_lxr,2,8) or not chkChinese(f_lxr) then
	finderr=true
	errmsg=errmsg&"联系人不正确(2-4个汉字)\n"
	end if
	if  not chkRange(f_tel,6,20)  then
	finderr=true
	errmsg=errmsg&"电话号码不正确(号码6-20个数字)\n"
	end if	
	if not chkNull(f_fax,1) then
	if not chkRange(f_fax,6,20) then
	finderr=true
	errmsg=errmsg&"传真号码不正确(6-20个数字，如无请留空)\n"
	end if
	end if
	if not chkNull(f_mobile,1) then
	if not chkRange(f_mobile,10,12) or not isnumeric(f_mobile) then
	finderr=true
	errmsg=errmsg&"手机号码不正确(10-13个数字，如无请留空)\n"
	end if
	end if
	if not IsValidEmail(f_email) then	
	finderr=true
	errmsg=errmsg&"请输入一个有效的邮箱\n"	
	end if
	if not chkRange(f_address,4,30) then
	finderr=true
	errmsg=errmsg&"联系地址不正确(4-30字符)\n"
	end if
	if not chkRange(f_cpname,4,30) then
	finderr=true
	errmsg=errmsg&"公司名字不正确(4-30字符)\n"
	end if
	if not chkRange(f_jy,2,15) then
	finderr=true
	errmsg=errmsg&"主营产品不正确(2-15字符)\n"
	end if		
	if not chkRange(f_js,10,300) then
	finderr=true
	errmsg=errmsg&"企业介绍不正确(10-300个汉字)\n"
	end if	
	if finderr then
	call showErrmsg(errmsg,"")
	end if
	if isExist(f_user,f_cpname,f_email) then
	call showErrmsg("用户名或者企业已经存在","")
	end if
	usride =  now()
	usride =  replace(usride," ","")
	usride =  replace(usride,"-","")
	usride =  replace(usride,":","")
	
	sql="insert into usr (usride,usrcod,usrpwd,usrsdt,usredt,usrmtw,usrmpd,usrlsr,usrsex,usreml,usrtel,usrcty,usradr,usrdat,usrgsn,usrjob,usrtra,usrsel,usrbuy,usracti,usrlgn,usrlgi,usrlgc,usrtyp,usrcmt,usrsjh,usrfax,usrpos)values('"&usride&"','"&f_user&"','1187320E33234FDAEB67E2EE61ACBDCD','"&now()&"','"&now()&"','请输入一个问题','"&md5("123456")&"','"&f_lxr&"',1,'"&f_email&"','"&f_tel&"','"&addtwo_id&"','"&f_address&"','"&now()&"','"&f_cpname&"','经理',"&cojy&",'"&f_hy&"','"&f_hy&"','Y','"&now()&"','"&Request.ServerVariables("REMOTE_ADDR")&"',1,1,'"&f_js&"','"&f_mobile&"','"&f_fax&"','"&usrpos&"')"
	'response.Write(sql)
	'response.End()	
	conn.execute(sql)
	
	if f_logo<>"" then
		filename="/UploadFile/Member/"&usride&"/"&makefilename()&"."&GetExt(f_logo)	
		call Createfol("/UploadFile/Member/"&usride)
		if  SaveRemoteFile(filename,f_logo)	then	
			call movecopy(filename)		
			conn.execute("update usr set usrlgo='"&filename&"' where usride='"&usride&"'")	
		end if
	end if
	application.Lock()
	application("ypid")=usride		
	application.UnLock()
	'添加企业的个数保存到cookies里面	
	coNum=coNum+1
	coNumall=coNumall+coNum	
	response.Cookies("coNum")=coNum
end if

if actfl="saveyp" and request.Form("Submit")="不添加企业进入产品列表" then
	f_user=request.Form("f_user")	
	f_cpname=request.Form("f_cpname")
	f_email=request.Form("f_email")
	if not isValidName(f_user) then
		call showErrmsg("用户名不正确(4-20以字母开头，数字和下划线组合)","")
	end if
	if not chkRange(f_cpname,4,30) then
		call showErrmsg("公司名字不正确(4-30字符)","")
	end if
	if not IsValidEmail(f_email) then		
		call showErrmsg("请输入一个有效的邮箱\n")	
	end if
	if not isExist(f_user,f_cpname,f_email) then
		call showErrmsg("当前企业不存在，必须要先添加企业才能进行下一步","")
	end if
end if
%>
<div>
<h1>第三步 添加产品</h1>    
  <script>
function CheckAll(form)  {
  for (var i=0;i<form.elements.length;i++)    {
    var e = form.elements[i];
    if (e.name != 'chkall')       e.checked = form.chkall.checked; 
   }
  }
</script>
  <form action="<%=home%>?act=4" method="post" onsubmit="return Validator.Validate(this,2);">
  <table width="500" border="0" cellspacing="0" cellpadding="0">
  
  <tr class="tbHeader">
      <td width="30%">图片地址</td>
      <td width="*">产品名称</td>
	  <td width="5%">选择</td>
  </tr>	
  <%
  proUrl=request.QueryString("proUrl")
  if chknull(proUrl,1) then proUrl="http://shop.fengj.com/sell-"&application("id")&".html"
  response.Write(getProList(proUrl))
  %> 
	<tr>
      <td colspan="3" class="tbHeader"><label>	  
		<input name="chkall" type="checkbox" id="chkall" value="select" onclick="CheckAll(this.form)" style="border:0">全选
        <input type="submit" name="Submit" value="添加" />
        <input type="button" name="Submit2" value="返回" onclick="history.back()"/>
      </label></td>    
    </tr>
  </table>
  </form>
  
</div>
<%end sub%>

<%sub act4()%>
<div>
<h1>第四步 添加产品。。。</h1>
<%
dim proid,profl,proflid,projl
dim ti
dim tarr
dim ypid,bclassid,sclassid,addone_id,addtwo_id,cojy
dim filename,sql
proid=request.Form("id")
ypid=application("ypid")
sclassid=application("sclassid")
addtwo_id=application("addtwo_id")
cojy=application("cojy")
if  chknull(ypid,5) or not chkrequest(sclassid) or len(addtwo_id)<>6 then
call showErrmsg("致命错误，必须的系统配置信息丢失，程序将自动返回第一页","index.asp")
end if



if chkNull(proid,1) then
call showErrmsg("你没有选择任何产品","")
end if
proid=replace(proid," ","")
proid=split(proid,",")
ti=0
for i=0 to ubound(proid)
if not chkNull(proid(i),10) then
	response.Write("正在获取第 <strong>"&i+1&"</strong> 个产品信息。。")
	response.Flush()
	tarr=getProinfo(proid(i))
	response.Write("<font color=""green"">完成</font><br />")
	if chkNull(tarr(0),10) or chkNull(tarr(1),2) or chkNull(tarr(5),5) then
		response.Write("<font color=""#ff000"">产品信息不完整，放弃添加此产品！</font> 图片："&tarr(0)&" 名称："&tarr(1)&" 描叙："&left(tarr(5),5)&" 产品地址："&proid(i)&"<br>")
		response.Flush()
	else
		response.Write("正在保存产品图片。。")
		response.Flush()
		call createfol("/UploadFile/Member/"&ypid&"/")
		filename="/UploadFile/Member/"&ypid&"/"&makefilename()&"."&getext(tarr(0))
		if SaveRemoteFile(filename,tarr(0)) then
		'处理图片，去掉onccc图片上的水印 需要安装组件aspjpeg
		call movecopy(filename)		
		response.Write("<font color=""green"">成功</font><br />")
		response.Flush()
		if chkrequest(tarr(3)) then f_face=1 else f_face=0
		sql="insert into pro (pronam,epronam,prospe,eprospe,proclb,proimg,prosig,propre,pronum,procmt,eprocmt,profac,procdat,proacti,proseq,prosel,prousr,proding,proshe)values('"&tarr(1)&"','','"&tarr(3)&"','',"&sclassid&",'"&filename&"','"&filename&"',"&tarr(2)&","&tarr(4)&",'"&replace_text(tarr(5))&"','',"&f_face&",'"&now()&"','Y',1,1,'"&ypid&"',0,0)"		
		conn.execute(sql)
		response.Write("第 <strong>"&i+1&"</strong> 个产品信息已经加入数据库<br>")
		response.Flush()
		ti=ti+1
		else
		response.Write("<font color=""red"">产品图片保存失败，放弃添加!</font> 地址："&server.MapPath(filename)&" 图片远程地址："&tarr(0)&" 产品地址："&proid(i)&"<br />")
		response.Flush()
		end if
	end if
	tarr=empty
end if
next
response.Write("所有操作已经完成，共添加了 <strong>"&ti&"</strong> 个产品信息！<br /><br /><br><a href=""?act=3"" style=""color:#FF0000"">继续添加这个公司的产品</a>&nbsp;<a href=""index.asp"" style=""color:#FF0000"">添加其它公司的产品</a>")
proNum=proNum+ti
proNumall=proNumall+proNum
application.Lock()
application("proNum")=proNum
application.UnLock()
%>
<%end sub%>

<%sub Login()
%>
<div>
<h1>请先登录。。。</h1>
<form action="<%=home%>?act=0" method="post" onsubmit="return Validator.Validate(this,2);">
  <table width="500" border="0" cellspacing="0" cellpadding="0">
  	<tr><td>请输入你的密码：<input type="password" name="pass" />&nbsp;<input type="submit" value="进入" />
	</td>
	</tr>
  </table>
 </form>
</div>
<%end sub%>
<%
sub chkLogin()
dim pass
pass=request.Form("pass")
if not chkRange(pass,3,20) then
call showErrmsg("密码输入不正确(3-20个字符)","")
end if
if md5(pass)<>"0004A47CFB1AF6BB87FA56D088D3DA34" then
call showErrmsg("密码错误！","")
end if
response.Cookies("jianyingLogin")="yes"
response.Redirect("index.asp")
end sub
sub logout()
application.Lock()
application("sclassid")=empty
application("addtwo_id")=empty
application("cojy")=empty
application("url")=empty
application("id")=empty
application("ypid")=empty
application.UnLock()
response.Cookies("jianyingLogin")=empty
response.Write("<div><h1>退出成功。。<a href=""javascript:window.close()"">关闭窗口</a>&nbsp;<a href=""index.asp"">返回首页</a></h1></div>")
end sub
sub foot()
%>
<div>
<hr />
<h1>添加信息统计</h1>
今天你一天添加 <font color="#FF0000"><strong><%=coNum%></strong></font> 个公司，添加了 <font color="#FF0000"><strong><%=proNum%></strong></font> 个产品信息；这些时间来(2009-02-16日起)，你共添加了 <font color="#FF0000"><strong><%=coNumall%></strong></font> 个公司，共添加了 <font color="#FF0000"><strong><%=proNumall%></strong></font> 个产品信息&nbsp;<%if request.Cookies("jianyingLogin")="yes" then%><a href="<%=home%>?act=10" style="color:#ff0000">退出登录</a><%end if%>
</div>
<%
call savetofile(coNumall&","&proNumall,"Count.txt")
end sub%>
</body>
</html>
