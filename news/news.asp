﻿<!--#include virtual="/inc/top.asp"-->
<!--#include virtual="/inc/page.asp"-->

<%
dim rs,i,n,content,id,title,className

id=request.QueryString("id")
if id="" or isnull(id) or isempty(id) or not isnumeric(id) then
response.Write("<script>alert('错误的参数传递!');history.back();</script>")
response.End()
end if
id=cint(id)
if id<0 or id>9 then
response.Write("<script>alert('错误的参数传递!');history.back();</script>")
response.End()
end if
select case id
case 0
className="财经新闻"
case 1
className="国际动态"
case 2
className="行业动态"
case 3
className="产业新闻"
case 4
className="品牌战略"
case 6
className="价格行情"
case 7
className="五金展会"
case 8
className="库存信息"
case 9
className="五金市场"
end select
%>
<meta name="Keywords" content="<%=classname%>,五金，采购，机械，机床，出口，进口，百货，工具，建材，模具，小家电，金属，不锈钢，日用品" />
<meta name="Description" content="<%=classname%>,中国最大、最安全的网上交易社区，尽享义乌生产资料网！" />
<link rel="stylesheet" href="/style/style.css" type="text/css" media="all" />
<title><%=classname%>_设备资讯中心_义乌生产资料网</title>
</head>
<body>

<!-- 头部开始 -->
<!--#include virtual="/inc/head.asp"-->
<!--#include file="head.asp"-->
<!-- 头部结束 -->

<!-- 内容开始 -->
<!-- 导航 -->
<div class="navigation">
您当前位置：<a href="/" class="link">首页</a> >> <a href="/news/" class="link">设备资讯</a></span> >> <%=classname%></div>

<div id="probox">
<div class="probox_left">
<!--sector1-->
<div class="sector">
<div class="loginbt">
<h2><%=classname%></h2>
</div>
<!--价格行情-->
<div class="newslist">
<%
sql="msgtyp="&id&" and msgacti='Y' and key1=1"
sql_id="msgide"  
'显示字段   
sql_Field="msgide,msgnam,msgcdat"  
'查询表名   
sql_table="msg"  
'排序字段   
sql_order="msgcdat"  
'每页记录   
PageSize=30   
'获得总数  
pp="select count("&sql_id&") from "&sql_table&" where "&sql&" "
'response.Write(pp)
set prors=conn.execute(pp)   
if not prors.eof then   
recordcount=prors(0)   
end if   
prors.close   
set prors=nothing 
 	'总页数   
	pagecount=Abs(Int(recordcount/PageSize*(-1)))   
	'获得当前页码   
	if request.querystring("page") = "" then   
		page = 1   
	else   
		page = cint(request("page"))   
		if recordcount< page*PageSize then   
			page=pagecount   
		end if   
	end if   
'sql语句   
if page=1 then   
	sql="SELECT TOP "&PageSize&" "&sql_Field&" from "&sql_table&" where "&sql&" order by "&sql_order&" desc"  
else   
	sql="SELECT TOP "&PageSize&" "&sql_Field&" from "&sql_table&" where ("&sql_order&" <(SELECT MIN("&sql_order&") FROM (SELECT TOP "&((Page-1)*PageSize)&" "&sql_order&" FROM "&sql_table&" where "&sql&" order by "&sql_order&" desc) AS tblTMP)) and "&sql&" order by "&sql_order&" desc"  
end if 
set rs=server.CreateObject("adodb.recordset")
rs.open sql,conn,1,1
if rs.bof and rs.eof then
content="<div align=center>No Content</div>"
else
content=""
i=1
do while not rs.eof and i<=pagesize
id=rs("msgide")
title=leftT(rs("msgnam"),40)
getDate=formatdatetime(rs("msgcdat"),2)
if i mod 2 = 0 then 
n=3 
else 
n=1 '控制样式
end if 
content=content&"<ul>"&vbcrlf
content=content&"<li class=""news"&n&""">·<a href=""detail.asp?id="&id&""" target=""_blank"" title="""&getInnerText(rs("msgnam"))&""">"&title&"</a></li>"&vbcrlf
content=content&"<li class=""news"&n+1&""">"&getDate&"</li>"&vbcrlf
content=content&"</ul>"&vbcrlf
rs.movenext
i=i+1
loop
end if
response.Write(content)
%>
</div>
<!--分页-->
<div class="page2">
	记录总数：<strong class="red"><%=recordcount%></strong>  总页数：<strong class="red"><%=pagecount%></strong>
	<div id="pagelist">
	<ul>
	  <%call showpage(pagecount,pagesize,page,RecordCount,5)%>
	 </ul>
	</div>
</div>
<%rs.close%>
</div>
</div>
<!--#include virtual="/inc/newsright.asp"-->
</div>
<!-- 内容结束 -->

<!-- 尾部开始 -->
<script language='javascript' src="/js/footer.js"></script>
<!-- 尾部结束 -->
<%set rs=nothing
call closeconn()%>
</body>
</html>