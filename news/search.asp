<!--#include virtual="/inc/top.asp"-->
<!--#include virtual="/inc/page.asp"-->
<%
keyword=checkStr(request.QueryString("keyword"))
typ=request.QueryString("typ")

if len(keyword)<2 or len(keyword)>50 then
response.Write("<script>alert('请输入至少两个字的关键字!');history.back();</script>")
response.End()
end if

if not chkrequest(typ) then
sql="select msgide,msgnam,msgcdat from msg where msgtyp>=0 and msgtyp<5 and msgacti='Y' and key1=1 and msgnam like '%"&keyword&"%' order by msgcdat desc"
elseif cint(typ)<0 or cint(typ)>4 then
sql="select msgide,msgnam,msgcdat from msg where msgtyp>=0 and msgtyp<5 and msgacti='Y' and key1=1 and msgnam like '%"&keyword&"%' order by msgcdat desc"
else
sql="select msgide,msgnam,msgcdat from msg where msgtyp="&cint(typ)&" and msgacti='Y' and key1=1 and msgnam like '%"&keyword&"%' order by msgcdat desc"
end if

dim rs,i,n,content
set rs=server.CreateObject("adodb.recordset")

rs.open sql,conn,1,1
%>
<meta name="Keywords" content="五金，采购，机械，机床，出口，进口，百货，工具，建材，模具，小家电，金属，不锈钢，日用品" />
<meta name="Description" content="中国最大、最安全的网上交易社区，尽享中华二手设备网！" />
<link rel="stylesheet" href="../style/style.css" type="text/css" media="all" />
<title>搜索与 <%=keyword%> 相关的设备资讯信息,共找到 <%=rs.recordcount%> 条信息_中华二手设备网</title>
</head>
<body>

<!-- 头部开始 -->
<!--#include virtual="/inc/head.asp"-->
<!--#include file="head.asp"-->
<!-- 头部结束 -->

<!-- 内容开始 -->
<!-- 导航 -->
<div class="navigation">
您当前位置：<a href="/" class="link">首页</a> >> 搜索中心  >> 搜索 <font color="#ff0000"><%=keyword%></font>,共找到 <font color="#FF0000"><%=rs.recordcount%></font> 条信息</span></div>

<div id="probox">
<div class="probox_left">
<!--sector1-->
<div class="sector">
<div class="loginbt">
<h2>相关信息</h2>
</div>
<!--展会信息-->
<div class="newslist">
<%

if rs.bof and rs.eof then
content="<div align=center>对不起，没有找到相关数据</div>"
else
content=""
i=1
page=clng(request.querystring("page"))
rs.pagesize=30
if (page-Rs.pagecount) > 0 then
	page=Rs.pagecount
elseif page = "" or page < 1 then
	page = 1
end if
Rs.absolutepage=page
do while not rs.eof and i<=30
id=rs("msgide")
title=leftT(rs("msgnam"),40)
title=replace(title,keyword,"<font color=""#ff0000"">"+keyword+"</font>") '加亮显示
getDate=formatdatetime(rs("msgcdat"),2)
if i mod 2 = 0 then 
n=3 
else 
n=1 '控制样式
end if 
content=content&"<ul>"&vbcrlf
content=content&"<li class=""news"&n&""">·<a href=""detail.asp?id="&id&""" target=""_blank"" title="""&getInnerText(rs("msgnam"))&""">"&title&"</a></li>"&vbcrlf
content=content&"<li class=""news"&n+1&""">"&getDate&"</li>"&vbcrlf
content=content&"</ul>"&vbcrlf
rs.movenext
i=i+1
loop
end if
response.Write(content)
%>
</div>
<!--分页-->
<div class="page2">
	记录总数：<strong class="red"><%=rs.recordcount%></strong>  总页数：<strong class="red"><%=rs.pagecount%></strong>
	<div id="pagelist">
	<ul>
	  <%call showpage(rs.pagecount,rs.pagesize,page,rs.RecordCount,10)%>
	 </ul>
	</div>
</div>
<%rs.close%>
</div>
</div>
<div class="probox_right">
<!--价格行情-->
<div class="prosector">
<div class="prosectorbt">
<h2>价格行情</h2>
</div>
<ul class="sectorcon">
<%
rs.open "select top 10 queide,quenam from que where queacti='Y' order by quecdat desc",conn,1,1
if rs.bof and rs.eof then
content="<div align=center>No content</div>"
else
i=1
content=""
do while not rs.eof and i<=10
id=rs("queide")
title=leftT(rs("quenam"),12)
content=content&"<li>·<a href=""/news/detail.asp?id="&id&""" title="""&getInnerText(rs("quenam"))&""">"&title&"</a></li>"
i=i+1
rs.movenext
loop
end if
response.write(content)
rs.close
%>
</ul>
<div class="morenews">
  <a href="/news/que.asp">更多>></a></div>
</div>
<!--五金资讯-->
<div class="prosector">
<div class="prosectorbt">
<h2>五金资讯</h2>
</div>
<ul class="sectorcon">
<%
rs.open "select top 10 msgide,msgnam from msg where msgtyp=9 and msgacti='Y' and key1=1 order by msgcdat desc",conn,1,1
if rs.bof and rs.eof then
content="<div align=center>No content</div>"
else
i=1
content=""
do while not rs.eof and i<=10
id=rs("msgide")
title=leftT(rs("msgnam"),12)
content=content&"<li>·<a href=""/news/detail.asp?id="&id&""" title="""&getInnerText(rs("msgnam"))&""">"&title&"</a></li>"
i=i+1
rs.movenext
loop
end if
response.write(content)
rs.close
%>
</ul>
<div class="morenews">
  <a href="/news/news.asp?id=9">更多>></a></div>
</div>
<!--品牌战略-->
<div class="prosector">
<div class="prosectorbt">
<h2>品牌战略</h2>
</div>
<ul class="sectorcon">
<%
rs.open "select top 10 msgide,msgnam from msg where msgtyp=4 and msgacti='Y' and key1=1 order by msgcdat desc",conn,1,1
if rs.bof and rs.eof then
content="<div align=center>No content</div>"
else
i=1
content=""
do while not rs.eof and i<=10
id=rs("msgide")
title=leftT(rs("msgnam"),12)
content=content&"<li>·<a href=""/news/detail.asp?id="&id&""" title="""&getInnerText(rs("msgnam"))&""">"&title&"</a></li>"
i=i+1
rs.movenext
loop
end if
response.write(content)
rs.close
set rs=nothing
closeconn
%>
</ul>
<div class="morenews">
  <a href="/news/news.asp?id=4">更多>></a></div>
</div>

</div>
</div>
<!-- 内容结束 -->

<!-- 尾部开始 -->
<script language='javascript' src="/js/footer.js"></script>
<!-- 尾部结束 -->

</body>
</html>