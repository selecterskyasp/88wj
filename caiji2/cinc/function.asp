﻿<%
'==================================================
'函数名：GetHttpPage
'作  用：获取网页源码
'参  数：HttpUrl ------网页地址
'==================================================
Function GetHttpPage(byVal HttpUrl)
   If IsNull(HttpUrl)=True Or Len(HttpUrl)<18 Or HttpUrl="$False$" Then
      GetHttpPage="$False$"
      Exit Function
   End If
   Dim Http
   Set Http=server.createobject("MSXML2.XMLHTTP")
   Http.open "GET",HttpUrl,False
   Http.Send()
   If Http.Readystate<>4 then
      Set Http=Nothing 
      GetHttpPage="$False$"
      Exit function
   End if
   GetHTTPPage=bytesToBSTR(Http.responseBody,"gbk")
   Set Http=Nothing
   If Err.number<>0 then
      Err.Clear
   End If
End Function

'==================================================
'函数名：BytesToBstr
'作  用：将获取的源码转换为中文
'参  数：Body ------要转换的变量
'参  数：Cset ------要转换的类型
'==================================================
Function BytesToBstr(byVal Body,byVal Cset)
   Dim Objstream
   Set Objstream = Server.CreateObject("adodb.stream")
   objstream.Type = 1
   objstream.Mode =3
   objstream.Open
   objstream.Write body
   objstream.Position = 0
   objstream.Type = 2
   objstream.Charset = Cset
   BytesToBstr = objstream.ReadText 
   objstream.Close
   set objstream = nothing
End Function


'==================================================
'函数名：UrlEncoding
'作  用：转换编码
'==================================================
Function UrlEncoding(byVal DataStr)
    Dim StrReturn,Si,ThisChr,InnerCode,Hight8,Low8
    StrReturn = ""
    For Si = 1 To Len(DataStr)
        ThisChr = Mid(DataStr,Si,1)
        If Abs(Asc(ThisChr)) < &HFF Then
            StrReturn = StrReturn & ThisChr
        Else
            InnerCode = Asc(ThisChr)
            If InnerCode < 0 Then
               InnerCode = InnerCode + &H10000
            End If
            Hight8 = (InnerCode  And &HFF00)\ &HFF
            Low8 = InnerCode And &HFF
            StrReturn = StrReturn & "%" & Hex(Hight8) &  "%" & Hex(Low8)
        End If
    Next
    UrlEncoding = StrReturn
End Function

'==================================================
'函数名：GetBody
'作  用：截取字符串
'参  数：ConStr ------将要截取的字符串
'参  数：StartPos ------开始查找位置
'参  数：StartStr ------开始字符串
'参  数：OverStr ------结束字符串
'参  数：IncluL ------是否包含StartStr
'参  数：IncluR ------是否包含OverStr

'==================================================
Function GetBody(byVal ConStr,byVal StartPos_,byVal StartStr,byVal OverStr,byVal IncluL,byVal IncluR)   
   If ConStr="$False$" or ConStr="" or IsNull(ConStr) Or StartStr="" or IsNull(StartStr) Or OverStr="" or IsNull(OverStr) or IsNull(StartPos_) or not isnumeric(StartPos_) or StartPos_<1 Then   	  
      GetBody=array(1,"",-1)
      Exit Function
   End If
   
   Dim ConStrTemp
   Dim Start,Over
   Dim tempStr
   ConStrTemp=Lcase(ConStr)
   StartStr=Lcase(StartStr)
   OverStr=Lcase(OverStr)
   Start = InStrB(StartPos_, ConStrTemp, StartStr, vbBinaryCompare)
   If Start<=0 then
      GetBody=array(2,"",-1)
      Exit Function
   Else
      If IncluL=False Then
         Start=Start+LenB(StartStr)
      End If
   End If
   Over=InStrB(Start,ConStrTemp,OverStr,vbBinaryCompare)
   If Over<=0 Or Over<=Start then
      GetBody=array(3,"",-1)
      Exit Function
   Else
      If IncluR=True Then
         Over=Over+LenB(OverStr)
      End If
   End If
   tempStr=MidB(ConStr,Start,Over-Start)   
   GetBody=array(0,tempStr,Over+1)  
End Function

'保存文件
Sub SaveToFile(ByVal strBody,ByVal File)
    Dim objStream
    Set objStream = Server.CreateObject("ADODB"+"."+"Stream")
    If Err.Number=-2147221005 Then 
        Response.Write "<div align='center'>非常遗憾,您的主机不支持ADODB.Stream,不能使用本程序</div>"
        Err.Clear
        Response.End
    End If	
    With objStream
        .Type = 2
        .mode = 3
        .Charset = "utf-8"
		.Open
        .Position = objStream.Size
        .WriteText = trim(strBody)
        .SaveToFile Server.MapPath(File),2
		.flush
        .Close
    End With	
    Set objStream = Nothing
End Sub

'读取文件
function ReadFromUTF (byVal TempString,byVal charset)    'TempString要读取的模板文件路径; Charset是编码
	dim str,stm
    set stm=server.CreateObject("adodb.stream")
    stm.Type=2 
    stm.mode=3 
    stm.charset=charset
    stm.open
    stm.loadfromfile server.MapPath(TempString)	
    str=stm.readtext
    stm.Close
    set stm=nothing
    ReadFromUTF=str	
end function 


'判断文件是否存在
Function isfileExists(byVal FileName) 
isfileExists = false
dim delobj
set delobj=server.CreateObject("Scripting.FileSystemObject")
filename=server.MapPath(filename)	
if delobj.FileExists(filename) then
	isfileExists=true
else
	isfileExists=false
end if
set delobj=nothing
End Function 

'==================================================
'过程名：SaveRemoteFile
'作  用：保存远程的图片到本地
'参  数：LocalFileName ------ 本地文件名
'参  数：RemoteFileUrl ------ 远程文件URL
'==================================================
Function SaveRemoteFile(byVal LocalFileName,byVal RemoteFileUrl)
    SaveRemoteFile=false
	dim Ads,Retrieval,GetRemoteData
	Set Retrieval = Server.CreateObject("Microsoft.XMLHTTP")
	With Retrieval
		.Open "Get", RemoteFileUrl, False, "", ""
		.Send
        If .Readystate<>4 or .status<>200 then
            SaveRemoteFile=False
            Exit Function
        End If
		GetRemoteData = .ResponseBody
	End With
	Set Retrieval = Nothing
	Set Ads = Server.CreateObject("Adodb.Stream")
	With Ads
		.Type = 1
		.Open
		.Write GetRemoteData
		.SaveToFile server.MapPath(LocalFileName),2
		.Cancel()
		.Close()
	End With
	Set Ads=nothing
	SaveRemoteFile=true
end Function


'在指定的位置创建文件夹
function Createfol(byVal folpath)
	dim fso
	set fso=server.CreateObject("Scripting.FileSystemObject")	
	if right(folpath,1)<>"/" then folpath=folpath&"/"
	folpath=server.MapPath(folpath)	
	if not fso.FolderExists(folpath) then		
		fso.CreateFolder(folpath)
	end if	
	if err.number=0 then
		Createfol=fso.GetBaseName(folpath)
	else
		Createfol=false
	end if
	set fso=nothing
end function

'生成一个文件名 注：无扩展名
function makeFilename()
dim tmpfilename
dim rndnum
randomize timer
rndnum=int(rnd*10000)
tmpfilename=replace(replace(replace(replace(now(),":",""),"/","")," ","_"),"-","")
makeFilename=tmpfilename&rndnum
end function

Function GetFileSize(byVal sFileName) 
Dim f,objFSO,tmpfilename
tmpfilename=server.MapPath(sFileName)
Set objFSO = Server.CreateObject("Scripting.FileSystemObject") 
if objFSO.FileExists(tmpfilename) then
Set f = objFSO.Getfile(tmpfilename) 
GetFileSize = f.Size 
set f=nothing
else
GetFileSize=0
End if 
set objFSO=nothing
End Function

'会员发布的各种信息过滤
Function Replace_Text(byVal fString)
If Not IsNull(fString) Then
'fString = trim(fString)
fString = Replace(fString, CHR(10), "<BR> ")
fString = replace(fString, ">", "&gt;")
fString = replace(fString, chr(62), "&gt;") '>
fString = replace(fString, "<", "&lt;")
fString = replace(fString, chr(60), "&lt;") '<

fString = Replace(fString, CHR(34), "&quot;")
fString = Replace(fString, CHR(39), "&quot;")	'单引号过滤
fString = replace(fString, "'", "&quot;")
fString = replace(fString, """", "&quot;")
fString = Replace(fString, CHR(32), " ")		'&nbsp;
fString = Replace(fString, CHR(9), " ")			'&nbsp;
fString = Replace(fString, CHR(13), "")
Replace_Text = fString
End If
End Function


'完全过滤html
Function getInnerText(byVal strHTML)
strHTML=trim(strHTML)
  if strHTML="" or isnull(strHTML) then
    getInnerText="":exit function
  end if
    Dim objRegExp, Match, Matches 
    Set objRegExp = New Regexp
    
    objRegExp.IgnoreCase = True
    objRegExp.Global = True
    '取闭合的<>
    objRegExp.Pattern = "<.+?>"
    '进行匹配
    Set Matches = objRegExp.Execute(strHTML)
    
    ' 遍历匹配集合，并替换掉匹配的项目
    For Each Match in Matches 
    strHtml=Replace(strHTML,Match.Value,"")
    Next
	strHTML=replace(strHTML,"&nbsp;","")
	strHTML=replace(strHTML," ","")	
	strHTML=replace(strHTML,"	","")
    getInnerText=strHTML
    Set objRegExp = Nothing
End Function

'检测传递的参数是否为数字型
Function Chkrequest(byVal Para)
dim tempNum
Chkrequest=False
Para=trim(Para)
If not IsNull(Para) and not Para="" and IsNumeric(Para) Then	
	tempNum=clng(Para)
	if tempNum>0 then
  	 Chkrequest=True
   	end if
End If
End Function

'显示错误提示页面
'msg 错误信息 ref 返回页面 如果ref为空,即默认为history.back()
function showErrmsg(byVal msg,byVal ref)	
	if chkNull(ref,1) and ref<>"/" then 
	ref="javascript:history.back();"
	else
	ref="location.href='"&ref&"';"	
	end if
	response.Write("<script>alert('您的操作失败,可以引起的原因:\n\n·"&msg&"');"&ref&"</script>")	
	call closers(rs)
	call closeconngoto()
	response.End()
end function

'关闭对象
sub closers(Para)	
	if typename(Para)<>"Nothing" and isobject(Para) then
		if Para.state<>0 then Para.close	
		set Para=nothing
	end if
end sub

'释放资源
sub closeconn()'
	if typename(conn)<>"Nothing" and isobject(Para) then
		if conn.state<>0 then conn.close	
		set conn=nothing	
	end if
end sub

'检查网址是否有效
function chkUrl(byVal surl)
surl=lcase(surl)
if instr(surl,".fengj.com")=0 then
chkUrl=false
else
chkUrl=true
end if
end function
'检查字符串是满足长度
Function ChkNull(byVal Para,byVal strlen)
dim tempPara:tempPara=trim(Para)
if isnull(tempPara)=true or tempPara="" or len(tempPara)<strlen then
ChkNull=True
else
ChkNull=False
end if
end Function
'检查字符串满足长度的范围 如果满足，返回True 否则，返回False
function chkRange(byVal Para,byVal smin,byVal sMax)
dim tempPara:tempPara=trim(Para)
if isnull(tempPara) or tempPara="" or len(tempPara)<smin or len(tempPara)>sMax then
chkRange=False
else
chkRange=True
end if
end function

'检查是否汉字 true是，false否
function chkChinese(byVal para)
	dim RegExpObj,ReGCheck
	if chkNull(para,1) then 
	chkChinese=false
	exit function
	end if
	Set RegExpObj=new RegExp   
	RegExpObj.Pattern="^[\u4e00-\u9fa5]+$"   
	ReGCheck=RegExpObj.test(para) 
	Set RegExpObj=nothing
	chkChinese=ReGCheck
end function

'检查用户名是否合法 字母开头，4-20位的数字，字母，下划线组合
Function isValidName(byVal str)
    Dim re
    Set re = New RegExp
    re.IgnoreCase = True
    re.Global = True
    re.Pattern = "^[A-Za-z][A-Za-z0-9_]{3,19}$"
    isValidName = re.Test(str)
    Set re = Nothing
End Function

function IsValidEmail(temail)	
	dim regExpobj
	set regExpobj=new regexp
	regExpobj.IgnoreCase=True
	regExpobj.Global=true
	regExpobj.Pattern="^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$"
	IsValidEmail=regExpobj.test(temail)
	set regExpobj=nothing	
end function

'判断用户名和企业是否存在
function isExist(susername,scpname,semail)
isExist=false
dim grs
set grs=server.CreateObject("adodb.recordset")
grs.open "select top 1 usride,usrcty,usrtra from usr where usrcod='"&susername&"' or usrgsn='"&scpname&"' or usreml='"&semail&"'",conn,1,1
if grs.bof and grs.eof then
isExist=false
else
isExist=true
application.Lock()
application("ypid")=grs("usride")
application("addtwo_id")=grs("usrcty")
application("cojy")=grs("usrtra")
application.UnLock()
end if
grs.close
set grs=nothing
end function

'处理图片，将图片去掉水印 需要安装组件 aspjpeg
' 返回 true false,fpath 等处理的图片路径 tpath处理后保存为图片路径
function changePic(byVal fpath,byVal tpath)
dim jpeg
changePic=false
Set Jpeg = Server.CreateObject("Persits.Jpeg") '调用组件 
Jpeg.Open server.MapPath(fPath) '打开图片 
'去图片上面60像素，下面60像素
Jpeg.Width = Jpeg.OriginalWidth
Jpeg.Height = Jpeg.OriginalHeight
jpeg.crop 0,60,Jpeg.Width,Jpeg.Height-60
'保存图片 
Jpeg.Save Server.MapPath(tpath)
jpeg.close
set jpeg=nothing
changePic=true
end function

'删除地址中的参数
'surl 地址
'spara 要查找的参数
function delUrlpara(byVal surl,byVal spara)
dim pos1,pos2
dim tmpurl
spara=lcase(spara)
surl=lcase(surl)
spara=spara&"="
pos1=instr(surl,spara)
if pos1>0 then
	if pos1=1 then
		'在第一个位置,前面没有参数		
		pos2=instr(pos1,surl,"&")
		if pos2<>0 then
		tmpurl=right(surl,len(surl)-pos2+1)		
		else
		tmpurl=""
		end if
	else
		'不在第一个位置,前面以&开头的			
		if mid(surl,pos1-1,1)="&" then 
			tmpurl=left(surl,pos1-2)
			pos2=instr(pos1,surl,"&")
			if pos2>pos1 then tmpurl=tmpurl&right(surl,len(surl)-pos2+1) 
		elseif mid(surl,pos1-1,1)="?" then
			'是带有网址的参数
			tmpurl=left(surl,pos1-1)
			pos2=instr(pos1,surl,"&")
			if pos2>pos1 then tmpurl=tmpurl&right(surl,len(surl)-pos2+1) 
		else
			'没有找到
			tmpurl=surl
		end if
	end if
else
	'没有找到
	tmpurl=surl
end if
tmpurl=replace(replace(tmpurl,"?&","?"),"&&","&")
if right(tmpurl,1)="&" or right(tmpurl,1)="?" then tmpurl=left(tmpurl,len(tmpurl)-1)
if left(tmpurl,1)="&" then tmpurl=right(tmpurl,len(tmpurl)-1)
delUrlpara=tmpurl
surl="":tmpurl="":spara="":pos1="":pos2=""
end function

'获取扩展名
function GetExt(byVal FileName)
	GetExt = right(FileName,len(FileName)-instrrev(FileName,"."))	
	FileName=""	
end function

function moveCopy(byval path)
Dim Jpeg
dim tleft,ttop
dim savepath,fontsize,fontlen
path=Server.MapPath(path)
if err then
moveCopy=array(1,"路径不正确",path)
exit function
end if
Set Jpeg = Server.CreateObject("Persits.Jpeg")
Jpeg.Open path
if err then
moveCopy=array(2,"创建图片对象失败","")
exit function
end if

Jpeg.Canvas.Font.BkColor =&H541e04
Jpeg.Canvas.Font.BkMode="Opaque"
Jpeg.Canvas.Font.Color=&Hffffff
select case true
case jpeg.OriginalHeight>=1000
fontsize=35
case jpeg.OriginalHeight>=500
fontsize=30
case jpeg.OriginalHeight>=200
fontsize=20
case else
fontsize=18
end select
'Jpeg.Canvas.Font.size=fontsize

'ttop=Jpeg.OriginalHeight-158
'response.Write(jpeg.OriginalHeight&","&jpeg.height&","&ttop)
set jpeg2=server.createobject("persits.jpeg")
jpeg2.open server.mappath("big.jpg")
'tleft=Jpeg.OriginalWidth / 2-jpeg2.OriginalWidth
'ttop=Jpeg.OriginalHeight *0.8-jpeg2.OriginalHeight
tleft=Jpeg.OriginalWidth / 2-jpeg2.OriginalWidth/2
if Jpeg.OriginalHeight>=1000 then
ttop=Jpeg.OriginalHeight *0.8
else
ttop=Jpeg.OriginalHeight *0.8-jpeg2.OriginalHeight/2
end if
jpeg.canvas.drawimage tleft,ttop,jpeg2,1,&HFFFFFF
'x,y,水印图,透明度,抽取颜色

set jpeg2=nothing
'Jpeg.Canvas.Print tleft, ttop, "http://www.58china.cn"
' 保存文件
'savepath=server.MapPath("/uploadfile/member/2010113140627/output.jpg")
Jpeg.Save path
if err then
	moveCopy=array(3,"保存图片失败",savepath)
	set Jpeg=nothing
	exit function
end if
' 注销对象
Set Jpeg = Nothing
moveCopy=array(0,"","")
end function

'获取数字
Function getNum(str) 
Dim length,num 
dim i,char
length=Len(str) 
num=""
For i=1 To length 
char=Mid(str,i,1) 
If isNumeric(char) or char="." Then num=num&char 
Next 
getNum=num 
End function 
%>