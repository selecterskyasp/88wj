﻿<!--#include virtual="/inc/top.asp"-->
<!--#include virtual="/inc/page.asp"-->
<!--#include virtual="/inc/keyword.asp"-->
<meta name="Keywords" content="五金，采购，机械，机床，出口，进口，百货，工具，建材，模具，小家电，金属，不锈钢，日用品" />
<meta name="Description" content="中国最大、最安全的网上交易社区，尽享中华二手设备网！" />
<link rel="stylesheet" href="../style/style.css" type="text/css" media="all" />
<title>机械展会中心_义乌生产资料网</title>
</head>
<body>

<!-- 头部开始 -->
<!--#include virtual="/inc/head.asp"-->
<!--#include file="head.asp"-->
<!-- 头部结束 -->

<!-- 内容开始 -->
<!-- 导航 -->
<%
keyword=checkstr(request.QueryString("keyword"))
sql=""
if chkrange(keyword,1,25) then
	if len(keyword)<4 then
		sql=" and charindex('"&keyword&"',msgnam)>0"
	else
		keyword=getkeyword(keyword)(1)
		sql="and "&createsql("msgnam",keyword,0)(1)
	end if
end if
%>
<div class="navigation">
您当前位置：<a href="/" class="link">首页</a> >> 机械展会</span><%if len(keyword)>1 then response.Write("搜索关于 <font color=red>"&replace(keyword,"|"," ")&"</font> 的信息")%></div>

<div id="probox">
<div class="probox_left">
<!--sector1-->
<div class="sector">
<div class="loginbt">
<h2>展会信息</h2>
</div>
<!--展会信息-->
<div class="newslist">
<%

dim rs,i,n,content
set rs=server.CreateObject("adodb.recordset")
rs.open "select msgide,msgnam,msgcdat from msg where msgtyp=7 and msgacti='Y' and key1=1"&sql&" order by msgcdat desc",conn,1,1
if rs.bof and rs.eof then
content="<div align=center>暂无相关信息</div>"
else
content=""
i=1
page=cint(request.querystring("page"))
rs.pagesize=30
if (page-Rs.pagecount) > 0 then
	page=Rs.pagecount
elseif page = "" or page < 1 then
	page = 1
end if
Rs.absolutepage=page
do while not rs.eof and i<=30
id=rs("msgide")
title=leftT(rs("msgnam"),40)
getDate=formatdatetime(rs("msgcdat"),2)
title=boldkeyword(title,keyword,0)
if i mod 2 = 0 then n="2" else n="" '控制样式
content=content&"<ul>"&vbcrlf
content=content&"<li class=""news1"&n&""">·<a href=""detail.asp?id="&id&""" target=""_blank"" title="""&getInnerText(rs("msgnam"))&""">"&title&"</a></li>"&vbcrlf
content=content&"<li class=""news2"&n&""">"&getDate&"</li>"&vbcrlf
content=content&"</ul>"&vbcrlf
rs.movenext
i=i+1
loop
end if
response.Write(content)
%>
</div>
<!--分页-->
<div class="page2">
	记录总数：<strong class="red"><%=rs.recordcount%></strong>  总页数：<strong class="red"><%=rs.pagecount%></strong>
	<div id="pagelist">
	<ul>
	  <%call showpage(rs.pagecount,rs.pagesize,page,rs.RecordCount,10)%>
	 </ul>
	</div>
</div>
<%rs.close%>
</div>
</div>
<div class="probox_right">
<!--价格行情-->
<div class="prosector">
<div class="prosectorbt">
<h2>价格行情</h2>
</div>
<ul class="sectorcon">
<%
rs.open "select top 10 msgide,msgnam from msg where msgtyp=6 and msgacti='Y' and key1=1 order by msgcdat desc",conn,1,1
if rs.bof and rs.eof then
content="<div align=center>No content</div>"
else
i=1
content=""
do while not rs.eof and i<=10
id=rs("msgide")
title=leftT(rs("msgnam"),12)
content=content&"<li>·<a href=""/news/detail.asp?id="&id&""" title="""&getInnerText(rs("msgnam"))&""">"&title&"</a></li>"
i=i+1
rs.movenext
loop
end if
response.write(content)
rs.close
%>
</ul>
<div class="morenews">
  <a href="/news/que.asp">更多>></a></div>
</div>
<!--五金资讯-->
<div class="prosector">
<div class="prosectorbt">
<h2>五金资讯</h2>
</div>
<ul class="sectorcon">
<%
rs.open "select top 10 msgide,msgnam from msg where msgtyp=9 and msgacti='Y' and key1=1 order by msgcdat desc",conn,1,1
if rs.bof and rs.eof then
content="<div align=center>No content</div>"
else
i=1
content=""
do while not rs.eof and i<=10
id=rs("msgide")
title=leftT(rs("msgnam"),12)
content=content&"<li>·<a href=""/news/detail.asp?id="&id&""" title="""&getInnerText(rs("msgnam"))&""">"&title&"</a></li>"
i=i+1
rs.movenext
loop
end if
response.write(content)
rs.close
%>
</ul>
<div class="morenews">
  <a href="/news/news.asp?id=9">更多>></a></div>
</div>
<!--品牌战略-->
<div class="prosector">
<div class="prosectorbt">
<h2>品牌战略</h2>
</div>
<ul class="sectorcon">
<%
rs.open "select top 10 msgide,msgnam from msg where msgtyp=4 and msgacti='Y' and key1=1 order by msgcdat desc",conn,1,1
if rs.bof and rs.eof then
content="<div align=center>No content</div>"
else
i=1
content=""
do while not rs.eof and i<=10
id=rs("msgide")
title=leftT(rs("msgnam"),12)
content=content&"<li>·<a href=""/news/detail.asp?id="&id&""" title="""&getInnerText(rs("msgnam"))&""">"&title&"</a></li>"
i=i+1
rs.movenext
loop
end if
response.write(content)
rs.close
set rs=nothing
closeconn
%>
</ul>
<div class="morenews">
  <a href="/news/news.asp?id=4">更多>></a></div>
</div>

</div>
</div>
<!-- 内容结束 -->

<!-- 尾部开始 -->
<script language='javascript' src="/js/footer.js"></script>
<!-- 尾部结束 -->

</body>
</html>