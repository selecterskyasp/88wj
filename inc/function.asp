﻿<%
'On Error Resume Next
'Server.ScriptTimeOut=9999999
function makefilename(byVal fname)
	  fname = now()
	  fname = replace(fname,"-","")
	  fname = replace(fname," ","") 
	  fname = replace(fname,":","")
	  fname = replace(fname,"PM","")
	  fname = replace(fname,"AM","")
	  fname = replace(fname,"上午","")
	  fname = replace(fname,"下午","")
	  makefilename=fname
end function 
function GetExtendName(byVal FileName)
	dim ExtName
	if len(FileName)<1 or isNull(FileName) or FileName="" then
	ExtName=""
	else
	ExtName = right(FileName,len(FileName)-instrrev(FileName,"."))	
	end if
	GetExtendName = ExtName	
end function
Function getHTTPPage(byVal Path)
        t = GetBody(Path)
        getHTTPPage=BytesToBstr(t,"utf-8")
End function

Function GetBody(byVal url) 
        on error resume next
        Set Retrieval = CreateObject("Microsoft.XMLHTTP") 
        With Retrieval 
        .Open "Get", url, False, "", "" 
        .Send 
        GetBody = .ResponseBody
        End With 
        Set Retrieval = Nothing 
End Function

Function BytesToBstr(byVal body,byVal Cset)
        dim objstream
        set objstream = Server.CreateObject("adodb.stream")
        objstream.Type = 1
        objstream.Mode =3
        objstream.Open
        objstream.Write body
        objstream.Position = 0
        objstream.Type = 2
        objstream.Charset = Cset
        BytesToBstr = objstream.ReadText 
        objstream.Close
        set objstream = nothing
End Function

Function Newstring(byVal wstr,byVal strng)
        Newstring=Instr(lcase(wstr),lcase(strng))
        if Newstring<=0 then Newstring=Len(wstr)
End Function

'采用二进制数据流操作,为了解决编码问题生成静态页面
Sub SaveToFile(ByVal strBody,ByVal File)
    Dim objStream
    On Error Resume Next
    Set objStream = Server.CreateObject("ADODB"+"."+"Stream")
    If Err.Number=-2147221005 Then 
        Response.Write "<div align='center'>非常遗憾,您的主机不支持ADODB.Stream,不能使用本程序</div>"
        Err.Clear
        Response.End
    End If
    With objStream
        .Type = 2
        .mode = 3
        .Charset = "utf-8"
		.Open
        .Position = objStream.Size
        .WriteText = trim(strBody)
        .SaveToFile Server.MapPath(File),2
		.flush
        .Close
    End With
    Set objStream = Nothing
End Sub

function ReadFromUTF (byVal TempString,byVal charset)    'TempString要读取的模板文件路径; Charset是编码
    dim str
    set stm=server.CreateObject("adodb.stream")
    stm.Type=2 
    stm.mode=3 
    stm.charset=charset
    stm.open
    stm.loadfromfile server.MapPath(TempString)
    str=stm.readtext
    stm.Close
    set stm=nothing
    ReadFromUTF=str
	
end function 

'在指定的位置创建文件夹
function Creatfol(byVal folpath)
	dim fso
	set fso=server.CreateObject("Scripting.FileSystemObject")
	folpath=server.MapPath(folpath)
	if not fso.FolderExists(folpath) then		
		fso.CreateFolder(folpath)
	end if	
	if err.number=0 then
		Creatfol=fso.GetBaseName(folpath)
	else
		Creatfol=false
	end if
	set fso=nothing
end function

'删除指定的文件夹
function Delfol(byVal folpath)
	dim fsobj
	set fsobj=server.CreateObject("Scripting.FileSystemObject")
	folpath=server.MapPath(folpath)

	if not fsobj.FolderExists(folpath) then
		Delfol=true
		exit function		
	end if
	fsobj.DeleteFolder(folpath)
	if err.number=0 then
		Delfol=true
	else
		Delfol=false
	end if
	set fsobj=nothing
end function

'删除指定的文件
function DelFile(byVal filename)
on error resume next
	dim delobj
	set delobj=server.CreateObject("Scripting.FileSystemObject")
	filename=server.MapPath(filename)	

	if delobj.FileExists(filename) then
		delobj.DeleteFile(filename)
	else
	delFile=false
	set delobj=nothing
	exit function
	end if
	set delobj=nothing
	if err.number=0 then
		DelFile=true
	else
		DelFile=false
	end if
end function


'移动指定的文件
function MoveFile(byVal filepath,byVal newfilepath,byVal filename)
	dim moveobj
	set moveobj=server.CreateObject("Scripting.FileSystemObject")
	filepath=server.MapPath(filepath)&"\"&filename
	if moveobj.FileExists(filepath) then
		moveobj.MoveFile filepath,server.MapPath(newfilepath)&"\"
	end if
	set moveobj=nothing
	if err.number=0 then
		MoveFile=true
	else
		MoveFile=false
	end if
end function

'判断指定文件夹是否为空
function isNullFolder(byVal path)
 	dim fs,f,f1,fc,s,folderPath
	folderPath=server.mapPath(path)
	set fs=createobject("scripting.filesystemobject") 	
	if fs.FolderExists(folderPath) then
	set f=fs.GetFolder(folderPath)
	if f.size=0 then
	isNullFolder=true
	else
	isNullFolder=false
	end if
	set f=nothing
	else
	isNullFolder=false
	end if 
	set fs=nothing
end function

'增加google广告
function addAD(byVal content)
st="<p style=""float:left;"">"
st=st&"<script type=""text/javascript"">"&vbcrlf
st=st&"<!--"&vbcrlf
st=st&"google_ad_client = ""pub-1556189734924127"";"&vbcrlf
st=st&"/* 图片广告 250x250, 创建于 08-3-24 */"&vbcrlf
st=st&"google_ad_slot = ""0788173083"";"&vbcrlf
st=st&"google_ad_width = 250;"&vbcrlf
st=st&"google_ad_height = 250;"&vbcrlf
st=st&"//-->"&vbcrlf
st=st&"</script>"&vbcrlf
st=st&"<script type=""text/javascript"" src=""http://pagead2.googlesyndication.com/pagead/show_ads.js"">"&vbcrlf
st=st&"</script>"&vbcrlf
st=st&"</p>"&vbcrlf
if instr(content,"pub-1556189734924127")=0 then
addAD=st+content
else
addAD=content
end if
end function

'生成网站首页函数
function createIndex()
Dim wstr,str,url,start,over,dtime
fname="/index.html"
url="http://www.xsp2.com/default.asp"
        wstr=getHTTPPage(url)
        start=Newstring(wstr,"<!DOCTYPE")
        over=Newstring(wstr,"</html>")
		'response.Write(wstr&start&over)
'		response.end
 body=mid(wstr,start,over-start)
 SaveToFile body,fname
end function
'-----根据ID取得name的Sub-----------------
Function getValueByID(sortID,inArray)
    dim i
    if NOT IsArray(inArray) then
        getValueByID=""
        Exit Function
    end if
    for i=0 to UBound(inArray,2)
        if Cstr(sortID)=Cstr(inArray(0,i)) then
            getValueByID=inArray(1,i) '返回name
            Exit Function
        end if
    next
    getValueByID=""
End Function
sub insert_record(table,Parameters,values)'表名,条件,返回路径
'	response.Write("insert into "&table&"("&Parameters&")values("&values&")")
'response.End()
	sql="insert into "&table&"("&Parameters&")values("&values&")"
	conn.execute(sql)
end sub

sub del_record(table,Conditions)'表名,条件,返回路径
'	response.Write("delete from "&table&" where "&Conditions&"")
'response.End()
	conn.execute("delete from "&table&" where "&Conditions&"")
end sub

sub update_record(table,Parameters,Conditions)'表名,条件,返回路径
	'response.Write("update "&table&" set "&Parameters&" where "&Conditions&"")
'response.End()
	conn.execute("update "&table&" set "&Parameters&" where "&Conditions&"")
end sub
Function select_recordset(fieldname,tablename,v_fieldname,v_fieldname_value)

	  sql_view="select "&fieldname&" from "&tablename&" where  "&v_fieldname&"="&v_fieldname_value&""
	  set rs_view=server.CreateObject("adodb.recordset")
	  rs_view.open sql_view,conn
	     if not rs_view.eof then
	        select_recordset=rs_view(fieldname)
	     end if 
	  rs_view.close
	  set rs_view=nothing
 end Function

function showmsg(msg)
	response.Write("<script>alert('您当前操作结果:\n\n"&msg&"');history.back();</script>")
	response.End()
end function
%>