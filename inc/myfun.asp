<%
'=========================================================
Function code_bl(strer)
  strer=strer
  if strer="" or isnull(strer) then
    code_bl="":exit function
  end if

  strer=replace(strer,"$SMT_name$",cityname)
  strer=replace(strer,"$SMT_mail$",citymail)
  strer=replace(strer,"$SMT_eurl$",cityeurl)
  strer=replace(strer,"$SMT_url$",cityurl)

  code_bl=strer
End Function

'检测传递的参数是否为日期型
Function Chkrequestdate(byVal Para)
Chkrequestdate=False
If Not (IsNull(Para) Or Trim(Para)="" Or Not IsDate(Para)) Then
   Chkrequestdate=True
End If
End Function

'显示错误提示页面
'msg 错误信息 ref 返回页面 如果ref为空,即默认为history.back()
function showErrmsg(byVal msg,byVal ref)	
	if chkNull(ref,1) and ref<>"/" then 
	ref="javascript:history.back();"
	else
	ref="location.href='"&ref&"';"	
	end if
	response.Write("<script>alert('您的操作失败,可以引起的原因:"&msg&"');"&ref&"</script>")	
	call closers(rs)
	call closeconngoto()
	response.End()
end function

'得到当前页面的地址 
Function GetUrl() 
Dim strTemp 
If LCase(Request.ServerVariables("HTTPS")) = "off" Then 
strTemp = "http://" 
Else 
strTemp = "https://" 
End If 
strTemp = strTemp & CheckStr(Request.ServerVariables("SERVER_NAME")) 
If Request.ServerVariables("SERVER_PORT") <> 80 Then strTemp = strTemp & ":" & CheckStr(Request.ServerVariables("SERVER_PORT")) 
strTemp = strTemp & CheckStr(Request.ServerVariables("URL")) 
If Trim(Request.QueryString) <> "" Then strTemp = strTemp & "?" & CheckStr(Trim(Request.QueryString)) 
GetUrl = strTemp  
End Function 

Function CheckReferer()  '检查用户是否在浏览器里输入了本页的地址
    Dim sReferer, Icheck
    CheckReferer = True
    sReferer = Request.ServerVariables("HTTP_REFERER")
    ServerIP = Request.ServerVariables("LOCAL_ADDR")
    Icheck = InStr(sReferer, "ServerIP")
    If Icheck = 0 Then
    CheckReferer = False
    End If
End Function

'日期格式化
Function FormatDate(byVal DT,byVal tp)
	dim Y,M,D
	Y=Year(DT)
	M=month(DT)
	D=Day(DT)
	if M<10 then M="0"&M
	if D<10 then D="0"&D
	select case tp
	case 1 FormatDate=Y&"年"&M&"月"&D&"日"
	case 2 FormatDate=Y&"-"&M&"-"&D
	end select
End Function
Function makefilename(fname)
	  fname = now()
	  fname = replace(fname,"-","")
	  fname = replace(fname," ","") 
	  fname = replace(fname,":","")
	  fname = replace(fname,"PM","")
	  fname = replace(fname,"AM","")
	  fname = replace(fname,"上午","")
	  fname = replace(fname,"下午","")
	  makefilename=fname
End Function 

'不允许外部提交数据的选择
Function ChkPost()
    dim HTTP_REFERER,SERVER_NAME
	dim server_v1,server_v2
	chkpost=false
    SERVER_NAME=CheckStr(Request.ServerVariables("SERVER_NAME"))
	HTTP_REFERER=CheckStr(Request.ServerVariables("HTTP_REFERER"))
	server_v1=Cstr(HTTP_REFERER)
	server_v2=Cstr(SERVER_NAME)
	if mid(server_v1,8,len(server_v2))<>server_v2 then
		chkpost=false
	else
		chkpost=true
	end if
End Function

'构造上传图片文件名随机数
function MakedownName()
dim fname
fname = now()
fname = replace(fname,"-","")
fname = replace(fname," ","") 
fname = replace(fname,":","")
fname = replace(fname,"PM","")
fname = replace(fname,"AM","")
fname = replace(fname,"上午","")
fname = replace(fname,"下午","")
fname = int(fname) + int((10-1+1)*Rnd + 1)
MakedownName=fname
end function

'错误记录入库
function ProcERR(id,table,errMsg)
if not isnumeric(id) then
ProcERR=1
exit function
end if
if table="" or len(table)<2 then
ProcERR=2
exit function
end if
id=clng(id)
table=trim(table)
if id<0 or id="" then
ProcERR=3
exit function
end if
errsql="select * from SMT_err where SMT_err_id="&id&" and SMT_err_table='"&table&"'"
set errRS=server.CreateObject("adodb.recordset")
errRS.open errsql,conn,1,1
if not (errRS.bof and errRS.eof) then
ProcERR=4
errRS.close
set errRS=nothing
exit function
end if
set errRS=server.CreateObject("adodb.recordset")
errRS.open "select * from SMT_err",conn,1,3
errRS.addnew
errRS("SMT_err_id")=id
errRS("SMT_err_table")=table
errRs("SMT_err_msg")=errMsg
errRS.update
errRS.close
set errRS=nothing
ProcERR=0
end function
%>