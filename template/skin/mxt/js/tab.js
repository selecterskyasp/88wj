
function Show(id,i_id){
        var on_img="images/collapse.gif";//打开时图片
        var off_img="images/expand.gif";//隐藏时图片
        var obj=document.getElementById('c_'+id);
        if(obj.style.display=="none"){
                obj.style.display="";
        i_id.src=on_img;
        //将子菜单Id放入Cookies
        var curShow = readCookie('curShow');
        if(curShow!='')
        {
          var arr_curShow = curShow.split(',');
          var found = false;
          for(i=0;i<arr_curShow.length-1;i++)
          {
            if(arr_curShow[i].toString()==id) {found=true;}
          }
          if(!found){writeCookie('curShow',curShow+','+id,12)}
        }
        else {
          writeCookie('curShow',id,12)
        }
        //将子菜单Id放入Cookies End
        }else{
                obj.style.display="none";
        i_id.src=off_img;
        //将子菜单Id从Cookies中移除 End
        }

}
function readCookie(name)
{
  var cookieValue = "";
  var search = name + "=";
  if(document.cookie.length > 0)
  { 
    offset = document.cookie.indexOf(search);
    if (offset != -1)
    { 
      offset += search.length;
      end = document.cookie.indexOf(";", offset);
      if (end == -1) end = document.cookie.length;
      cookieValue = unescape(document.cookie.substring(offset, end))
    }
  }
  return cookieValue;
}
function writeCookie(name, value, hours)
{
  var expire = "";
  if(hours != null)
  {
    expire = new Date((new Date()).getTime() + hours * 3600000);
    expire = "; expires=" + expire.toGMTString();
  }
  document.cookie = name + "=" + escape(value) + expire;
}
Array.prototype.del=function(n) {  //n表示第几项，从0开始算起。
//prototype为对象原型，注意这里为对象增加自定义方法的方法。
  if(n<0)  //如果n<0，则不进行任何操作。
    return this;
  else
    return this.slice(0,n).concat(this.slice(n+1,this.length));
}


window.onload = function a()
{
  var curShow = readCookie('curShow');
  var totalShow = 4;  //自己修改一共有多少个二级分类
  if(curShow!=''&&curShow!=null)
  {
    for(i=1;i<=totalShow;i++)
    {
      document.getElementById('c_'+i).display="none";
    }
    var arr_curShow;
    arr_curShow = curShow.split(',');
    for(i=0;i<=arr_curShow.length-1;i++)
    {
      if(arr_curShow[i]!=''){document.getElementById('c_'+arr_curShow[i]).style.display="";}
    }
  }
}



<!--
function setActiveStyleSheet(title) {
  var i, a, main;
  if (title) {
  for(i=0; (a = document.getElementsByTagName('link')[i]); i++) {
  if(a.getAttribute('rel').indexOf('style') != -1 && a.getAttribute('title')) {
  a.disabled = true;
  if(a.getAttribute('title') == title) a.disabled = false;
  }
  }
  }
  }
  function getActiveStyleSheet() {
  var i, a;
  for(i=0; (a = document.getElementsByTagName('link')[i]); i++) {
  if(a.getAttribute('rel').indexOf('style') != -1 && a.getAttribute('title') && !a.disabled) return a.getAttribute('title');
  }
  return null;
} 
//-->


window.onload=resetTab;
    function getTabTitle(tab) {
        var childNodesList=tab.childNodes;
        var titleNodes=new Array();
        var j=0;
        var i;
        for (i=0;i<childNodesList.length;i++) {
            if(childNodesList[i].nodeName=="H1") {
                titleNodes[j]=childNodesList[i];
                j++;
            }
        }
        return titleNodes;
    }
    function getTabContent(tab) {
        var childNodesList=tab.childNodes;
        var tabContent=new Array();
        var j=0;
        var i;
        for (i=0;i<childNodesList.length;i++) {
            if(childNodesList[i].nodeName=="DIV") {
                tabContent[j]=childNodesList[i];
                j++;
            }
        }
        return tabContent;
    }
    function resetTab() {
        var allDiv=document.getElementsByTagName("div");
        var tab=new Array();
        var j=0;
        var i;
        for (i=0;i<allDiv.length;i++) {
            if(allDiv[i].className=="tabs") {
                tab[j]=allDiv[i];
                j++;
            }
        }
        var tabTitle,tabContent;
        for(i=0;i<tab.length;i++) {
            tabTitle=getTabTitle(tab[i]);
            tabTitle[0].className="selectTab";
            tabContent=getTabContent(tab[i]);
            tabContent[0].className="selectContent";
            for (j=1;j<tabTitle.length;j++) {
                tabTitle[j].className="unselectTab";
                tabContent[j].className="unselectContent";
            }
        }
    }
    function changTab(tab) {
        var tabTitle,tabContent,i;
        if(tab.className!="selectTab") {
            tabTitle=getTabTitle(tab.parentNode);
            tabContent=getTabContent(tab.parentNode);
            for(i=0;i<tabTitle.length;i++) {
                if(tabTitle[i].className=="selectTab") {
                    tabTitle[i].className="unselectTab";
                }
                if(tabContent[i].className=="selectContent") {
                    tabContent[i].className="unselectContent";
                }
            }
        tab.className="selectTab";
        for(i=0;i<tabTitle.length;i++) {
            if(tabTitle[i].className=="selectTab") {
                tabContent[i].className="selectContent";
                break;
            }
        }
        }
    }