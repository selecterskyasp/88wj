<%
'获取缓存sfilepath模版文件名
function getCache(sfilepath)
dim tpath
tpath=skinpath&sfilepath
getCache=ReadFromUTF(tpath,"utf-8")
end function

'设置公共变量值
function setCommon(sbody_)
sbody_=replace(sbody_,"$skinpath$",skinpath)
sbody_=replace(sbody_,"$id$",id)
setCommon=sbody_
end function

'获取头部
function getHead(c,k,d,t)
dim head_
dim turl,tmparr(6,1),i
tmparr(0,0)="index.asp"
tmparr(1,0)="about.asp"
tmparr(2,0)="news.asp"
tmparr(3,0)="sell.asp"
tmparr(4,0)="buy.asp"
tmparr(5,0)="products.asp"
tmparr(6,0)="price.asp"
for i=0 to ubound(tmparr)
	tmparr(i,1)=""
next
turl=lcase(request.ServerVariables("URL"))

for i=0 to ubound(tmparr)
	if instr(turl,tmparr(i,0))>0 then 
		tmparr(i,1)="on"
		exit for
	end if
next
head_=getcache("head.html")
head_=replace(head_,"$copyright",c)
head_=replace(head_,"$keywords",k)
head_=replace(head_,"$description",d)
head_=replace(head_,"$title",t)
head_=replace(head_,"$coname",usrgsn)
head_=replace(head_,"$coename","")
head_=replace(head_,"$usrlgo$",usrlgo)
for i=0 to ubound(tmparr)
	head_=replace(head_,"$stylearr"&i&"$",tmparr(i,1))	
next
if vip=1 then
	head_=replace(head_,"$year",datediff("yyyy",usrsdt,now()))	
end if
erase tmparr
getHead=head_:head_=""
end function

'获取左边
function getLeft()
dim tcontent
tcontent=getcache("left.html")
tcontent=replace(tcontent,"$供应产品",getCoPro(10,"","",0))
tcontent=replace(tcontent,"$联系方式",getCoContact())
getLeft=tcontent:tcontent=""
end function

'获取脚部
function getFoot()
dim tcontent,body_
body_=getcache("foot.html")
tcontent=tcontent&"<a href=""/about/fzjg.asp"">分支机构</a> | <a href=""/about/ggfw.asp"">广告服务 </a> | <a href=""/about/wzdt.asp"">网站地图</a> | <a href=""/about/cpyc.asp"">诚聘英才 </a> | <a href=""/about/flsm.asp"">法律声明</a> | <a href=""/about/yjts.asp"">意见投诉</a> | <a href=""/about/help.asp"">帮助中心</a> | <a href=""/about/contact.asp"">联系我们</a><br />" & vbcrlf 
tcontent=tcontent&"  版权所有 中华二手设备网 <a href=""http://www.miibeian.gov.cn"">浙ICP备06054945号</a><br />" & vbcrlf 
tcontent=tcontent&"  Copyright©2000-"&year(now())&" www.58china.cn. All Rights Reserved" & vbcrlf 
body_=replace(body_,"$foot$",tcontent):tcontent=""
getFoot=body_:body_=""
end function

'获取产品信息 
function getCoPro(snum,swhere,sorder,smode)
dim rsv,tcontent,ti,tsql
dim turl,tt,tpic,price,tpage
ti=1
tcontent=""
if sorder="" then sorder="proshe desc"
tsql="select top "&snum&" proide,pronam,proclb,prosig,propre,procdat from pro where proacti='Y' and prousr='"&id&"'"&swhere&" order by "&sorder
if smode=1 or smode=2 then
	tpage=request.QueryString("page")
	if not chkrequest(tpage) then tpage=1 else tpage=cint(tpage)
	if tpage<>1 then
		tsql="SELECT TOP "&snum&" proide,pronam,proclb,prosig,propre,procdat from pro  where (proide <(SELECT MIN(proide) FROM (SELECT TOP "&((page-1)*snum)&" proide FROM pro where proacti='Y' and prousr='"&id&"'"&swhere&" order by proide desc) AS tblTMP)) and proacti='Y' and prousr='"&id&"'"&swhere&" order by proide desc"
	end if
end if
set rsv=server.CreateObject("adodb.recordset")
rsv.open tsql,conn,1,1
if rsv.bof and rsv.eof then
	getCoPro=""
	rsv.close
	set rsv=nothing
	exit function
end if
do while not rsv.eof and ti<=snum
	turl="/products/detail.asp?id="&rsv("proide")&"&clb="&rsv("proclb")
	tt=getinnertext(rsv("pronam"))
	if not chkrequest(rsv("propre")) then  price="面议" else price=getprice(rsv("propre"),2)
	if chknull(rsv("prosig"),10) then  tpic="/images/public/null.jpg" else tpic=rsv("prosig")
	
	select case smode
	case 0
	tcontent=tcontent&"<li><a href="""&turl&""" class=""blacklink"" target=""_blank"">"&left(tt,15)&"</a></li>"&vbcrlf
	case 1
		tcontent=tcontent&"<li>"&vbcrlf
		tcontent=tcontent&"<a href="""&turl&""" target=""_blank""><img src="""&tpic&""" border=""0""  width=""80"" height=""80"" title="""&tt&""" alt="""&tt&"""/></a><br />"&vbcrlf
		tcontent=tcontent&"<a href="""&turl&""" class=""blacklink"" target=""_blank"">"&left(tt,15)&"</a><br />"&vbcrlf
		tcontent=tcontent&"发布时间："&rsv("procdat")&"<br />"&vbcrlf
		tcontent=tcontent&"单价：<span class=""red"">"&price&"</span>"&vbcrlf
		tcontent=tcontent&"</li>"&vbcrlf
		if ti mod 3 = 0 and ti<>tnum then tcontent=tcontent&"</ul><ul>"&vbcrlf		
	case 2
		tcontent=tcontent&"<li><a href="""&turl&""" target=""_blank""><img src="""&tpic&""" border=""0"" width=""150"" height=""150"" align=""absmiddle"" /></a> <span><a href="""&turl&""" target=""_blank"">"&left(tt,14)&"</a></span> <span class=""flesh"">￥"&price&"</span> </li>" & vbcrlf 	

	end select
	rsv.movenext
	ti=ti+1
loop
rsv.close
set rsv=nothing
getCoPro=tcontent
tcontent=""
end function

function getCoPie(snum,swhere,sorder,smode)
dim rsv,tcontent,ti
dim tsql
dim turl,tt,tpic
dim page
ti=1:tcontent=""
if sorder="" then sorder="pieide desc"
tsql="select top "&snum&"  pieide,pietyp,pieclb,pienam,piesig,piecmt,pieusr,piecdat from pie where pieacti='Y' and pieusr='"&id&"'"&swhere&" order by "&sorder 
if smode=0 or smode=1 then
	page=request.QueryString("page")
	if not chkrequest(page) then page=1 else page=cint(page)
	if page<>1 then	
		tsql="SELECT TOP "&snum&" pieide,pietyp,pieclb,pienam,piesig,piecmt,pieusr,piecdat from pie  where (pieide <(SELECT MIN(pieide) FROM (SELECT TOP "&((page-1)*snum)&" pieide FROM pie where pieacti='Y' and pieusr='"&id&"'"&swhere&" order by pieide desc) AS tblTMP)) and pieacti='Y' and pieusr='"&id&"'"&swhere&" order by pieide desc"  
	end if
end if
set rsv=server.CreateObject("adodb.recordset")
rsv.open tsql,conn,1,1
if rsv.bof and rsv.eof then
	rsv.close
	set rsv=nothing
	getCoPie="暂无相关信息"
	exit function
end if
do while not rsv.eof and ti<=snum
tt=getinnertext(rsv("pienam"))
'if rsv("pietyp")=0 then
turl="/supply/detail.asp?id="&rsv("pieide")&"&clb="&rsv("pieclb")
'else
'turl="/sell/detail.asp?id="&rsv("pieide")&"&clb="&rsv("pieclb")
'end if
if chknull(rsv("piesig"),10) then   tpic="/images/public/null.jpg" else  tpic=rsv("piesig")
select case smode
case 0
tcontent=tcontent&"<li>"&vbcrlf
tcontent=tcontent&"<a href="""&turl&""" target=""_blank""><img src="""&tpic&""" border=""0""  width=""80"" height=""80"" title="""&tt&""" alt="""&tt&"""/></a><br />"&vbcrlf
tcontent=tcontent&"<a href="""&turl&""" class=""blacklink"" target=""_blank"">"&left(tt,15)&"</a><br />"&vbcrlf
tcontent=tcontent&"发布时间："&rsv("piecdat")&"<br />"&vbcrlf
tcontent=tcontent&"</li>"&vbcrlf
if ti mod 3 = 0 and ti<>tnum then tcontent=tcontent&"</ul><ul>"&vbcrlf	
case 1
tcontent=tcontent&"<tr>" & vbcrlf 
tcontent=tcontent&"    <td align=""center""><a href="""&turl&"""><img src="""&tpic&""" border=""0"" align=""absmiddle"" class=""img"" width=""150"" height=""150"" /></a></td>" & vbcrlf 
tcontent=tcontent&"    <td align=""center""><a href="""&turl&""">"&left(tt,14)&"</a></td>" & vbcrlf 
tcontent=tcontent&"    <td align=""center"">"&rsv("piecdat")&"</td> " & vbcrlf 
tcontent=tcontent&"  </tr>" & vbcrlf 
end select
rsv.movenext
ti=ti+1
loop
getCoPie=tcontent:tcontent=""
rsv.close
set rsv=nothing
end function

'获取新闻
function getNews(snum,swhere,sorder,smode)
dim rsv,tcontent,ti
dim tsql
dim turl,tt,tpic
dim page
ti=1:tcontent=""
if sorder="" then sorder="msgide desc"
tsql="select top "&snum&" msgide,msgnam,msgcdat from msg where  msgacti='Y' and msgusr='"&id&"'"&swhere&" order by " &sorder
if smode=0 or smode=1 then
	page=request.QueryString("page")
	if not chkrequest(page) then page=1 else page=cint(page)
	if page<>1 then	
		tsql="SELECT TOP "&snum&" msgide,msgnam,msgcdat from msg  where (msgide <(SELECT MIN(msgide) FROM (SELECT TOP "&((page-1)*snum)&" msgide FROM msg where msgacti='Y'and msgusr='"&id&"'"&swhere&" order by msgide desc) AS tblTMP)) and msgacti='Y'and msgusr='"&id&"'"&swhere&" order by msgide desc"  
	end if
end if

set rsv=server.CreateObject("adodb.recordset")
rsv.open tsql,conn,1,1
if rsv.bof and rsv.eof then
	rsv.close
	set rsv=nothing
	getNews="暂无相关信息"
	exit function
end if
do while not rsv.eof and ti<=snum
tt=getinnertext(rsv("msgnam"))
turl="/news/detail.asp?id="&rsv("msgide")
select case smode
case 0
tcontent=tcontent&"<li><a href="""&turl&""" class=""blacklink"" target=""_blank"">"&tt&"</a> <span class=""gray"">"&rsv("msgcdat")&"</span></span></li>"&vbcrlf
case 1
tcontent=tcontent&"<li>· <a href="""&turl&""" target=""_blank"" class=""flesh"">"&tt&"</a>  "&rsv("msgcdat")&" </li>" & vbcrlf 
end select
rsv.movenext
ti=ti+1
loop
getNews=tcontent:tcontent=""
rsv.close
set rsv=nothing
end function

'获取联系方式
function getCoContact()
dim tcontent
if instr(usrhtp,"http://") then 
 	usrhtp	= usrhtp
 else
	usrhtp	= "http://"&usrhtp 
 end if 
tcontent=tcontent&"<li>电    话："&usrtel&" </li>"&vbcrlf
tcontent=tcontent&"<li>移动电话："&usrsjh&" </li>"&vbcrlf
tcontent=tcontent&"<li>传    真："&usrfax&" </li>"&vbcrlf
tcontent=tcontent&"<li>地    址："&usradr&"  </li>"&vbcrlf
tcontent=tcontent&"<li>邮    编："&usrpos&" </li>"&vbcrlf
tcontent=tcontent&"<li>公司主页： </li>"&vbcrlf
tcontent=tcontent&"<li>  <a href="""&usrhtp&""" target=""_blank"" class=""blacklink"">"&usrhtp&"</a> </li>"&vbcrlf
tcontent=tcontent&"<li>  <a href=""index.asp?id="&id&""" target=""_blank"" class=""blacklink"">http://"&id&".58china.cn</a> </li>"&vbcrlf
getCoContact=tcontent:tcontent=""
end function

'得到分页
function getshowpage(pagecount,pagesize,page,resultcount,pageListCount)'pagecount分页数,pagesize每页条数,page跳转到第几页,resultcount总共记录
'pageListCount=8'每行显示几个数字
Dim query, a, x, temp
action = "http://" & Request.ServerVariables("HTTP_HOST") & Request.ServerVariables("SCRIPT_NAME")
query = Split(Request.ServerVariables("QUERY_STRING"), "&")
For Each x In query
a = Split(x, "=")
If StrComp(a(0), "page", vbTextCompare)<> 0 Then
temp = temp & a(0) & "=" & a(1) & "&"
End If
Next 
if page<=1 then
hcontent = hcontent&"<li><a href=""javascript:;"" class=""blue"">首页</a></li>"
hcontent = hcontent&"<li><a href=""javascript:;"" class=""blue"">上一页</a></li>"
else
hcontent = hcontent&"<li><a href=" & action & "?" & temp & "Page=1 class=""blue"">首页</a></li>"
hcontent = hcontent&"<li><a href=" & action & "?" & temp & "Page="&page-1&" class=""blue"">上一页</a></li>"
end if
startNum=int((page-1)/pageListCount)*pageListCount+1
'公式：int((n-1)/col)*col+1	n给定的参数	col每行显示几个数字		从1开始，顺序排
if startNum<0 then startNum=1

for i=startNum to startNum+pageListCount-1
	'response.write i
	if cint(i)=cint(page) then
		hcontent = hcontent&"<li class=""current"">"&i&"</li>"&vbcrlf
	else
		hcontent = hcontent&"<li><a href=" & action & "?" & temp & "page="&i&">"&i&"</a></li>"&vbcrlf
	end if	
	if cint(i)>=pagecount then
		exit for
	end if
Next
if page<pagecount then
hcontent=hcontent&"<li><a href=" & action & "?" & temp & "Page=" & page+1 & " class=""blue"">下一页</a></li>"
hcontent=hcontent&"<li><a href=" & action & "?" & temp & "Page=" & pageCount & " class=""blue"">尾页</a></li>"
else
hcontent=hcontent&"<li><a href=""javascript:;"" class=""blue"">下一页</a></li>"
hcontent=hcontent&"<li><a href=""javascript:;"" class=""blue"">尾页</a></li>"
end if
getshowpage = hcontent
End function
%>

