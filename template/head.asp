﻿<%

function getHead(c,k,d,t)
dim head_
head_=getcache("head.html")
head_=replace(head_,"$copyright",c)
head_=replace(head_,"$keywords",k)
head_=replace(head_,"$description",d)
head_=replace(head_,"$title",t)
head_=setcommon(head_)
end function
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="UTF-8" />
<meta name="robots" content="all" />
<meta http-equiv="imagetoolbar" content="false" />
<meta name="MSSmartTagsPreventParsing" content="true" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<meta name="Copyright" content="Copyright (c) 2008 www.58china.cn" />
<meta name="Keywords" content="五金，采购，机械，机床，出口，进口，百货，工具，建材，模具，小家电，金属，不锈钢，日用品" />
<meta name="Description" content="中国最大、最安全的网上交易社区，尽享义乌生产资料网！" />
<link rel="stylesheet" href="/style/moban_green.css" type="text/css" media="all" />
<title><%=usrgsn%>_义乌生产资料网</title>
</head>
<body>

<!-- 头部开始 -->
<div id="toplink">
<div class="link_left">
<a href="/">网站首页</a> | <a href="/supply/">供求信息</a> | <a href="/news/">五金资讯</a> | <a href="/supplier/">中国供应商</a> | <a href="/company/">企业名录</a> | <a href="/products/">产品大全</a> | <a href="/exhibition/">五金会展</a> | <a href="/market/">五金市场</a> | <a href="/bbs/">论坛</a></div>
<div class="link_right">
<a href="/reg/join.html">注册</a> | <a href="/user/login.html">登录</a>
</div>
</div>
<!--公司名称-->
<div id="company">
<div class="logo">
<%if usrlgo <>"" then response.Write("<img src="""&usrlgo&""" border=""0"" align=""absmiddle"" width=""70"" height=""70"" />")%>
</div>
<div class="company">
<div class="chinese"><%=usrgsn%></div>
<div class="english"></div>
</div>
</div>
<!--菜单-->
<div id="menu">
<div class="menu">
<ul>
<li class="<%=style1%>left"></li>
<li class="<%=style1%>center"><a href="/template/?id=<%=id%>">首页</a>
</li>
<li class="<%=style1%>right"></li>
</ul>
<ul>
<li class="<%=style2%>left"></li>
<li class="<%=style2%>center"><a href="/template/?id=<%=id%>&s=about">公司介绍</a>
</li>
<li class="<%=style2%>right"></li>
</ul>
<ul>
<li class="<%=style3%>left"></li>
<li class="<%=style3%>center"><a href="/template/?id=<%=id%>&s=news">公司动态</a>
</li>
<li class="<%=style3%>right"></li>
</ul>
<ul>
<li class="<%=style4%>left"></li>
<li class="<%=style4%>center"><a href="/template/?id=<%=id%>&s=sell">供应产品</a>
</li>
<li class="<%=style4%>right"></li>
</ul>
<ul>
<li class="<%=style5%>left"></li>
<li class="<%=style5%>center"><a href="/template/?id=<%=id%>&s=buy">求购产品</a>
</li>
<li class="<%=style5%>right"></li>
</ul>
<ul>
<li class="<%=style6%>left"></li>
<li class="<%=style6%>center"><a href="/template/?id=<%=id%>&s=products">产品展示</a>
</li>
<li class="<%=style6%>right"></li>
</ul>
<ul>
<li class="<%=style7%>left"></li>
<li class="<%=style7%>center"><a href="/template/?id=<%=id%>&s=price">最新报价</a>
</li>
<li class="<%=style7%>right"></li>
</ul>
<!--<ul>
<li class="<%=style8%>left"></li>
<li class="<%=style8%>center"><a href="/template/?id=<%=id%>&s=job">人才招聘</a>
</li>
<li class="<%=style8%>right"></li>
</ul>

<ul>
<li class="left"></li>
<li class="center"><a href="/template/?id=<%=id%>&s=contact">联系方式</a>
</li>
<li class="right"></li>
</ul>-->
</div>
</div>
<!-- 头部结束 -->