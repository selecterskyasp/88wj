﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
session.codepage = 65001  
Response.CharSet = "utf-8"
if request.Cookies("islogin")("id")="a" then
response.Write("<script language='javascript'>alert('您已经是会员，请不要重复注册！');window.close(this);</script>")
response.End()
end if

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="UTF-8" />
<meta name="robots" content="all" />
<meta http-equiv="imagetoolbar" content="false" />
<meta name="MSSmartTagsPreventParsing" content="true" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<meta name="Copyright" content="Copyright (c) 2007 www.58china.cn" />
<meta name="Keywords" content="五金，采购，机械，机床，出口，进口，百货，工具，建材，模具，小家电，金属，不锈钢，日用品" />
<meta name="Description" content="中国最大、最安全的网上交易社区，尽享中华二手设备网！" />
<link rel="stylesheet" href="/style/index.css" type="text/css" media="all" />
<link rel="stylesheet" href="/style/common.css" type="text/css" media="all" />
<link rel="stylesheet" href="/style/style.css" type="text/css" media="all" />
<script language="javascript" src="/js/validator.js"></script>
<script language="javascript" type="text/javascript" src="/jquery/jquery.js"></script>
<script language="javascript" type="text/javascript" src="/jquery/jQuery.FillOptions.js"></script>
<script language="javascript" type="text/javascript" src="/jquery/jQuery.CascadingSelect.js"></script>
<title>新用户注册_中华二手设备网</title>
<script language="javascript" type="text/javascript">
<!--
/**//**Ajax 开始 by chenjiaxin 2008-03-06*/

 var http = getHTTPObject();

//函数功能:提交成功后返回提示信息
//参数：elementID:用于显示信息的控件ID号
 function HttpResponse(){
　if(http.readyState == 4){ 
	 　if(http.status == 200){	 
	 　　	var xmlDocument = http.responseXML;
	 		var getInfo=new Array();
			var responseText=http.responseText;			
			getInfo=responseText.split("|");//得到用于显示信息的ID和要显示的信息
			try{
	 　　　　document.getElementById(getInfo[0]).style.display = "";
	 　　　//　document.getElementById(getInfo[0]).style.background= "#FF0000";
	 }catch(e){	 		
			alert('请先添加一个用于显示出错提示的控件!');			
	 }
	 		try{
	 　　　　document.getElementById(getInfo[0]).innerText =getInfo[1];
	 }catch(e){
	 		alert('请先添加一个用于显示出错提示的控件!');
	 }
	 　　}
	 　　else{
	 　　　alert("你所请求的页面发生异常，可能会影响你浏览该页的信息！");
	 　　　alert(http.status);
	 　　}
　	}
 }

//函数功能：提交要查询的信息
//参数：url--提交的网址   elmentId1--存储信息的控件ID elmentId2--显示提示信息的控件ID
 function chkInfo(url,elementId1,elementId2){
 　var getUrl = url;
 　var name = escape(document.getElementById(elementId1).value);    
 　getUrl += ("&st="+name+"&target="+elementId2);
 　http.open("GET",getUrl,true);
 　http.onreadystatechange = HttpResponse;
 　http.send(null);  
 }
 
 //该函数可以创建我们需要的XMLHttpRequest对象
 function getHTTPObject(){
 　var xmlhttp = false;
 　if(window.XMLHttpRequest){
 　　xmlhttp = new XMLHttpRequest();
 　　if(xmlhttp.overrideMimeType){
 　　　xmlhttp.overrideMimeType('text/xml');
 　　}
　}
 　else{
 　　try{
 　　　xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
　　}catch(e){
 　　　try{
 　　　　xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
 　　　}catch(E){
 　　　　xmlhttp = false;
　　　}
 　　}
 　}
 　return xmlhttp;
}
//--> 
</script>
</head>
<body>

<!-- 头部开始 -->
<div id="top">
<div class="top_left">
<div class="logo">
<h1><a title="www.58china.cn" href="/">www.58china.cn</a></h1>
</div>
</div>
<div class="top_right">
您好，欢迎来到中华二手设备网！<br />
<a href="/"><img src="/images/chinese.gif" border="0" /></a> 
<a href="/english/"><img src="/images/english.gif" border="0" /></a> 
</div>
<div class="top_center">
<div style="border-left:1px solid #999999; font-size:25px; font-family:黑体; letter-spacing:-1px; padding-left:5px; padding-top:15px; height:30px; text-align:left;">免费注册</div>
</div>
</div>

<!-- 头部结束 -->

<!-- 内容开始 -->
<div class="line"></div>
<div class="step">
<ul>
<li class="stepleft">&nbsp;</li>
<li class="step1">服务条款</li>
<li class="stepon">
<div class="stepon2">填写注册信息</div>
</li>
<li class="step3">注册成功</li>
<li class="stepright"></li>
</ul>
</div>
<div class="prompt">请认真、仔细地填写以下信息，严肃的商业信息有助于您获得别人的信任，结交潜在的商业伙伴，获取商业机会！</div>
<div class="regbt">
<h2>会员登入信息</h2>
</div>
<form action="Regsave.asp" method="post" name="form1" onSubmit="return Validator.Validate(this,2);">

<div class="regcontent">
  <table width="100%" border="0" cellspacing="0" cellpadding="2">
    <tr>
      <td width="28%" align="right" bgcolor="#f7f7f7" class="dashed">用户名：</td>
      <td width="72%" class="dashed"><input maxlength="20" size="30" name="username" onblur="chkInfo('/reg/chkInfo.asp?id=1','username','showStr')" dataType="Username" msg="请输入4到20个字符的登录名!"/>
        <span class="red">*</span>
   <span id="showStr" style="display:none; color:#0000FF; font-size:14px"></span>
  <br />
  <span class="gray">4—20位字符，请使用英文、数字的组合(注:<font color="#FF0000">注册后不能修改</font>)</span></td>
    </tr>
    <tr>
      <td align="right" bgcolor="#f7f7f7" class="dashed">密码：</td>
      <td class="dashed"><input maxlength="20" size="30" name="pwd1" dataType="LimitB" max="20" min="4" msg="请输入6到20个字符的密码!" type="password"/>
        <span class="red">*</span><br />
      <span class="gray">4-20位，请使用英文(a-z)、数字(0-9)注意区分大小写，密码不能与会员名相同。</span></td>
    </tr>
    <tr>
      <td align="right" bgcolor="#f7f7f7" class="dashed">再次输入密码：</td>
      <td class="dashed"><input maxlength="20" size="30" name="pwd2" dataType="Repeat" to="pwd1" msg="第二次输入密码不正确!" type="password"/>
        <span class="red">*</span><br />
      <span class="gray">请再输入一次密码。</span></td>
    </tr>
    <tr>
      <td align="right" bgcolor="#f7f7f7" class="dashed">密码问题：</td>
      <td class="dashed"><select id="f_que" name="f_que" dataType="Require" msg="请选择密码取回问题!">
        <option value="" selected="selected">请选择问题</option>
        <option value="您出生的城市">您出生的城市</option>
        <option value="您最爱的人的名字">您最爱的人的名字</option>
        <option value="您最爱的食物">您最爱的食物</option>
        <option value="您个人计算机的型号">您个人计算机的型号</option>
        <option value="您最难忘记的日子">您最难忘记的日子</option>
      </select>
       <span class="red">*</span><br />
      <span class="gray">请输入一个问题，可以在您丢失密码的时候用此问题的答案找回会员密码。</span></td>
    </tr>
    <tr>
      <td align="right" bgcolor="#f7f7f7" class="dashed">密码答案：</td>
      <td class="dashed"><input maxlength="20" size="30" name="f_anw" type="text" dataType="Require" msg="请输入密码取回答案!"/>
        <span class="red">*</span><br />
      <span class="gray">请认真填写上面密码提示问题的答案，用于找回会员密码。</span></td>
    </tr> 
	<tr>
      <td align="right" bgcolor="#f7f7f7" class="dashed">邮箱：</td>
      <td class="dashed"><input maxlength="60" size="30" name="f_email" type="text" dataType="Require" msg="请输入联系邮箱!" onblur="chkInfo('/reg/chkInfo.asp?id=2','f_email','showStr1')"/>
        <span class="red">*</span><span id="showStr1" style="display:none; color:#0000FF; font-size:14px"></span><br />
      <span class="gray">请认真填写上面密码提示问题的答案，用于找回会员密码。(注:<font color="#FF0000">注册后不能修改</font>)</span></td>
    </tr>
  </table>
</div>
<div class="regbt">
<h2>会员姓名与联系方式</h2>
</div>
<div class="regcontent">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="28%" align="right" bgcolor="#f7f7f7" class="dashed">真实姓名：</td>
      <td width="72%" class="dashed"><input name="f_lsr" type="text" id="f_lsr" size="30" maxlength="6" dataTyp="Chinese" Require="True" msg="请输入您的真实姓名!" />
      <span class="red">*</span></td>
    </tr>
    <tr>
      <td align="right" bgcolor="#f7f7f7" class="dashed">性别：</td>
      <td class="dashed"><select name="f_xb">
        <option value="1" selected="selected">男</option>
        <option value="0">女</option>
            </select>
      <span class="red">*</span></td>
    </tr>
    <tr>
      <td align="right" bgcolor="#f7f7f7" class="dashed">固定电话：</td>
      <td class="dashed"><input maxlength="13" size="30" name="f_tel" type="text" dataType="LimitB" min="6" max="20" msg="请输入有效电话号码!" />
      如:0579-85367402
      <span class="red">*</span></td>
    </tr>
    <tr>
      <td align="right" bgcolor="#f7f7f7" class="dashed">公司所在地：</td>
      <td class="dashed">
	<select name="addone_id" id="addone_id" style="width:100px" dataType="Require" msg="请选择您所在的省份!"></select>
		<select name="addtwo_id" id="addtwo_id" style="width:100px" dataType="Require" msg="请选择您所在的城市!" ></select>	
        <script language="javascript" type="text/javascript">
    
    $(function(){        
            $("#addone_id").FillOptions("/inc/getcity.asp",{datatype:"json",textfield:"name",valuefiled:"id"});
    });
    
	$("#addone_id").CascadingSelect(
							$("#addtwo_id"),
							"/inc/getcity.asp",
							{datatype:"json",textfield:"name",valuefiled:"id",parameter:"p"},
							function(){}
			);
    </script>
      <span class="red">*</span></td>
    </tr>
    <tr>
      <td align="right" bgcolor="#f7f7f7" class="dashed">详细地址：</td>
      <td class="dashed"><input maxlength="30" size="60" name="f_addr" dataType="LimitB" min="6" max="100" msg="请输入联系地址!" />
      <span class="red">*</span></td>
    </tr>
  </table>
</div>
<div class="regbt">
<h2>公司信息</h2>
</div>
<div class="regcontent">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="28%" align="right" bgcolor="#f7f7f7" class="dashed">公司名称：</td>
      <td width="72%" class="dashed"><input name="f_company" id="f_company" size="60" maxlength="30" onblur="chkInfo('/reg/chkInfo.asp?id=3','f_company','showcpStr')" dataType="Require" msg="请输入公司名称!"/>
        <span class="red">*</span><span id="showcpStr" style="display:none; color:#0000FF; font-size:14px"></span><br />
  <span class="gray">请填写您在工商局注册的公司(注:<font color="#FF0000">注册后不能修改</font>请认真填写！)</span></td>
    </tr>
    <tr>
      <td align="right" bgcolor="#f7f7f7" class="dashed">职位：</td>
      <td class="dashed"><input name="f_zhw1" id="f_zhw1" size="30" maxlength="8"  dataType="Require" msg="请输入职位!"/>
      <span class="red">*</span></td>
    </tr>
    <tr>
      <td align="right" bgcolor="#f7f7f7" class="dashed">所属行业：</td>
      <td class="dashed">
	  <select name="f_hy" id="f_hy"   dataType="Require" msg="请选择所属行业!">
        <option value="">请选择</option>
		<option value="1">生产型</option>
		<option value="2">贸易型</option>
		<option value="3">服务型</option>
		<option value="4">其它</option>
      </select>
      <span class="red">*</span></td>
    </tr>
    <tr>
      <td align="right" bgcolor="#f7f7f7" class="dashed">主营产品：</td>
      <td class="dashed">
        <div><span class="red">供应</span><span class="gray">的产品（提供的服务）：</span><br />
            <input name="f_sell" id="f_sell" size="40"  maxlength="10"  dataType="Require" msg="请选择您所在的城市!"/>
        <span class="red">*</span></div>
        <div><span class="red">求购</span><span class="gray">的产品（需要的服务）：</span><br />
            <input name="f_buy" id="f_buy" size="40"  maxlength="10"  dataType="Require" msg="请选择您所在的城市!"/>
        <span class="red">*</span></div>
		<span class="gray">请填产品名称，多个产品用逗号分隔。</span>		</td>
    </tr>
	<tr style="border-bottom:1px solid  #eeeeee;">
      <td align="right" >请输入右边图片显示的字符：</td>
      <td class="dashed">
      <%
randomize timer
isadd= int(rnd*8999)+1000 
session("isadd")=isadd
%>
      <input type="hidden" name="f_isadd" id="f_isadd" value="<%=isadd%>" />
      <input size="8" name="f_code12" dataType="Require"  msg="请输入正确的答案!" onBlur="chkCode(this)"/>      
      <script language="javascript" src="/inc/code.js"></script>&nbsp;<a href="javascript:;" onclick="document.getElementById('codeiframe').src='/inc/code.asp';">刷新验证码</a>&nbsp;<span class="red">为了防止恶意注册,请输入图片中的字符(不区分大小写)</span></td>
    </tr>   
  </table>
</div>
<div class="regcontent">
  <table width="100%" cellpadding="0" cellspacing="0">
    <tr>
      <td align="center" colspan="3">&nbsp;</td>
    </tr>
    <tr>
      <td align="center" colspan="3" height="26"><a href="servers.html" target="_blank">点此阅读中华二手设备网服务条款</a></td>
    </tr>
    <tr>
      <td align="center" colspan="3" height="26"><input id="chkReg" type="checkbox" checked="checked" name="chkReg" />
我已经阅读并接受 服务条款</td>
    </tr>
    <tr>
      <td align="center" colspan="3" height="38"><input name="submit" type="submit" value="同意服务条款,提交注册信息" /></td>
    </tr>
    <tr>
      <td align="center" colspan="3">&nbsp;</td>
    </tr>
  </table>  
</div>
</form>
<!-- 内容结束 -->

<!-- 尾部开始 -->
<script language="javascript" src="/js/footer.js"></script>
<!-- 尾部结束 -->

</body>
</html>