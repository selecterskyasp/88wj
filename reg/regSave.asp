﻿<!--#include virtual="/inc/conn.asp"-->
<!--#include virtual="/inc/safe_info.asp"-->
<!--#include virtual="/inc/md5.asp"-->

<%
result=request.Form("f_code12")
result=lcase(trim(result))
isadd=request.Form("f_isadd")
if chkrequest(isadd)=false or session("isadd")="" then
response.write("<script>alert('请不要在外面提交数据!');history.back();</script>")
response.end
end if
isadd=cint(isadd)
if cint(session("isadd"))<>isadd then
response.write("<script>alert('请不要在外面提交数据!');history.back();</script>")
response.end
end if

if result<>lcase(trim(session("GetCode"))) then
	call showErrmsg("\n·请输入正确的验证码!","")
end if
session("GetCode")=""


username=checkstr(request.Form("username"))
mail=checkstr(request.Form("f_email"))
pwd1=checkstr(request.Form("pwd1"))
pwd2=checkstr(request.Form("pwd2"))
f_que=checkstr(request.Form("f_que"))
f_anw=checkstr(request.Form("f_anw"))
f_lxr=checkstr(request.Form("f_lsr"))
f_xb=checkstr(request.Form("f_xb"))
f_tel=checkstr(request.Form("f_tel"))
f_city=checkstr(request.Form("addtwo_id"))
f_addr=checkstr(request.Form("f_addr"))
f_company=checkstr(request.Form("f_company"))
f_zhw=checkstr(request.Form("f_zhw1"))
f_hy=checkstr(request.Form("f_hy"))
f_sell=checkstr(request.Form("f_sell"))
f_buy=checkstr(request.Form("f_buy"))

errmsg=""
founderr=false
'---------------------数据判断-----------------------------

if not isValidName(username) then
founderr=true
errmsg=errmsg&"·用户名输入错误或不合法（4-20位以字母开头，数字下划线组成的字符）"
end if

if not IsValidEmail(mail) then
founderr=true
errmsg=errmsg&"\n\n·请输入正确的企业客户联系邮箱"
end if

if not chkRange(pwd1,4,20) or not chkRange(pwd2,4,20) then
founderr=true
errmsg=errmsg&"\n\n·密码输入不正确,长度必须在4-20之间"
end if

if pwd1<>pwd2 then
founderr=true
errmsg=errmsg&"\n\n·两次密码输入不一样，请确认"
end if

if chkNull(f_que,1) then
founderr=true
errmsg=errmsg&"\n\n·请选择取回密码的提示问题"
end if

if not chkRange(f_anw,2,30) then
founderr=true
errmsg=errmsg&"\n\n·取回密码答案输入不正确,长度必须为2-30个字符"
end if

if not chkRange(f_lxr,2,8) or not chkChinese(f_lxr) then
founderr=true
errmsg=errmsg&"\n\n·联系人输入不正确,长度必须为2-4个汉字"
end if

if f_xb<>"0" and f_xb<>"1" then
founderr=true
errmsg=errmsg&"\n\n·请选择联系人性别"
end if

if not chkRange(f_tel,6,20) then
founderr=true
errmsg=errmsg&"\n\n·联系电话不正确6-20位字符"
end if

if len(f_city)<>6 then
founderr=true
errmsg=errmsg&"\n\n·请选择您公司所在地"
end if

if not chkRange(f_addr,6,30)<6 then
founderr=true
errmsg=errmsg&"\n\n·联系地址不正确,长度必须为6-30个字符"
end if

if not chkRange(f_company,4,30) or not chkChinese(f_company) then
founderr=true
errmsg=errmsg&"\n\n·公司名不正确,长度必须为4-30个汉字"
end if

if not chkRange(f_zhw,2,6) or not chkChinese(f_zhw) then
founderr=true
errmsg=errmsg&"\n\n·职位输入不正确,长度必须2-6个汉字"
end if

if not chkrequest(f_hy) then
founderr=true
errmsg=errmsg&"\n\n·行业丢失,请选择主营行业"
else
f_hy=cint(f_hy)
if f_hy<1 or f_hy>4 then
founderr=true
errmsg=errmsg&"\n\n·行业丢失,请选择主营行业"
end if
end if

if not chkRange(f_sell,2,10) then
founderr=true
errmsg=errmsg&"\n\n·主要供应的产品输入不正确,长度必须2-10个字符"
end if

if not chkRange(f_buy,2,10) then
founderr=true
errmsg=errmsg&"\n\n·主要求购的产品输入不正确,长度必须2-10个字符"
end if

if founderr then
response.Write("<script>alert('对不起,注册用户失败,下面是可能引起失败的原因:"&errmsg&"');history.back();</script>")
response.End()
end if

set rs=server.CreateObject("adodb.recordset")
rs.open "select * from usr where usrcod='"&username&"' or usreml='"&mail&"' or usrgsn='"&f_company&"'",conn,1,1
if not (rs.bof and rs.eof) then
rs.close
set rs=nothing
closeconn
response.Write("<script>alert('对不起,注册用户失败,下面是可能引起失败的原因:\n\n用户或邮箱或公司名称已经存在,请重新选择!');history.back();</script>")
response.End()
end if
rs.close

usride =  now()
usride =  replace(usride," ","")
usride =  replace(usride,"-","")
usride =  replace(usride,":","")
'response.Write useride
'response.end

rs.open "select * from usr",conn,1,3
rs.addnew
rs("usride")=usride
rs("usrcod")=username
rs("usrpwd")=md5(pwd1)
rs("usrsdt")=now()
rs("usredt")=now()
rs("usrmtw")=f_que
rs("usrmpd")=md5(f_anw)
rs("usrlsr")=f_lxr
rs("usrsex")=f_xb
rs("usreml")=mail
rs("usrtel")=f_tel
rs("usrcty")=f_city
rs("usradr")=f_addr
rs("usrdat")=now()
rs("usrgsn")=f_company
rs("usrjob")=f_zhw
rs("usrtra")=f_hy
rs("usrsel")=f_sell
rs("usrbuy")=f_buy
rs("usracti")="N"
rs("usrlgn")=now()
rs("usrlgi")=Request.ServerVariables("REMOTE_ADDR")
rs("usrlgc")=1
rs("usrtyp")=1
rs.update

response.Cookies("islogin")("id")=usride
response.Cookies("islogin")("user")=username
response.Cookies("islogin")("lxr")=f_lxr
response.Cookies("islogin")("pass")=md5(pwd1)
response.Cookies("islogin")("mail")=mail
rs.close
set rs=nothing
call closeconn()
session("l1")=""
session("added")="yes"
response.Cookies("added")="yes"
Response.Cookies("added").Expires = DateAdd("n", 20, Now())

response.Redirect("regok.html")
%>