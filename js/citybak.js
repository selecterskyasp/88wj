addListGroup("city", "province");
addOption("province", "请选择省份", "");
addList("province", "湖南", "15", "15");
addOption("15", "请选择城市", "");
addOption("15", "常德市", "1");

addOption("15", "长沙市", "2");

addOption("15", "昭阳市", "5");

addOption("15", "永州市", "6");

addOption("15", "怀化市", "7");

addOption("15", "益阳市", "8");

addOption("15", "岳阳市", "9");

addOption("15", "张家界", "10");

addOption("15", "株洲市", "19");

addOption("15", "湘潭市", "20");

addOption("15", "衡阳市", "21");

addOption("15", "郴州市", "22");

addOption("15", "娄底地区", "35");

addOption("15", "湘西土家族苗族自治州", "36");

addOption("15", "其它地区", "37");

endOptGroup("15");
addList("province", "北京", "17", "17");
addOption("17", "请选择城市", "");
addOption("17", "顺义区", "409");

addOption("17", "延庆县", "410");

addOption("17", "昌平县", "411");

addOption("17", "怀柔县", "412");

addOption("17", "密云县", "489");

addOption("17", "东城区", "491");

addOption("17", "西城区", "492");

addOption("17", "崇文区　", "493");

addOption("17", "宣武区", "494");

addOption("17", "朝阳区", "495");

addOption("17", "丰台区", "496");

addOption("17", "石景山区", "497");

addOption("17", "海淀区", "498");

addOption("17", "门头沟区　", "499");

addOption("17", "平谷区", "501");

addOption("17", "大兴区", "502");

addOption("17", "房山区", "506");

endOptGroup("17");
addList("province", "天津", "18", "18");
addOption("18", "请选择城市", "");
addOption("18", "和平区", "11");

addOption("18", "河东区", "430");

addOption("18", "河西区", "431");

addOption("18", "南开区", "432");

addOption("18", "河北区", "433");

addOption("18", "红桥区", "434");

addOption("18", "塘沽区", "435");

addOption("18", "大港区", "436");

addOption("18", "东丽区", "437");

addOption("18", "西青区", "438");

addOption("18", "津南区", "439");

addOption("18", "北辰区", "440");

addOption("18", "蓟县", "441");

addOption("18", "宝坻县", "442");

addOption("18", "武清县", "443");

addOption("18", "宁河县", "444");

addOption("18", "静海县", "445");

addOption("18", "其它地区", "446");

addOption("18", "汉沽区", "507");

addOption("18", "和平区", "508");

endOptGroup("18");
addList("province", "河南", "19", "19");
addOption("19", "请选择城市", "");
addOption("19", "洛阳市", "3");

addOption("19", "焦作市", "4");

addOption("19", "三门峡", "17");

addOption("19", "郑州市", "18");

addOption("19", "新乡市", "447");

addOption("19", "鹤壁市", "448");

addOption("19", "安阳市", "449");

addOption("19", "濮阳市", "450");

addOption("19", "开封市", "451");

addOption("19", "商丘市", "452");

addOption("19", "许昌市", "453");

addOption("19", "漯河市", "454");

addOption("19", "平顶山", "455");

addOption("19", "南阳市", "456");

addOption("19", "信阳市", "457");

addOption("19", "济源市", "458");

addOption("19", "周口", "459");

addOption("19", "驻马店", "460");

addOption("19", "其它地区", "461");

endOptGroup("19");
addList("province", "河北", "20", "20");
addOption("20", "请选择城市", "");
addOption("20", "石家庄", "28");

addOption("20", "张家口", "478");

addOption("20", "秦皇岛", "479");

addOption("20", "承德市", "480");

addOption("20", "唐山市", "481");

addOption("20", "廊坊市", "482");

addOption("20", "保定市", "483");

addOption("20", "沧州市", "484");

addOption("20", "衡水市", "485");

addOption("20", "邢台市", "486");

addOption("20", "邯郸市", "487");

addOption("20", "其它地区", "488");

endOptGroup("20");
addList("province", "江苏", "21", "21");
addOption("21", "请选择城市", "");
addOption("21", "南京市", "27");

addOption("21", "徐州市", "417");

addOption("21", "连云港市", "418");

addOption("21", "宿迁市", "419");

addOption("21", "淮阴市", "420");

addOption("21", "盐城市", "421");

addOption("21", "扬州市", "422");

addOption("21", "泰州市", "423");

addOption("21", "南通市", "424");

addOption("21", "镇江市", "425");

addOption("21", "常州市", "426");

addOption("21", "无锡市", "427");

addOption("21", "苏州市", "428");

addOption("21", "其它地区", "429");

endOptGroup("21");
addList("province", "江西", "22", "22");
addOption("22", "请选择城市", "");
addOption("22", "南昌市", "24");

addOption("22", "九江市", "72");

addOption("22", "景德镇", "73");

addOption("22", "鹰潭市", "74");

addOption("22", "新余市", "75");

addOption("22", "萍乡市", "76");

addOption("22", "赣州市", "77");

addOption("22", "上饶市", "78");

addOption("22", "抚州市", "79");

addOption("22", "宜春市", "80");

addOption("22", "吉安市", "81");

addOption("22", "其它地区", "82");

endOptGroup("22");
addList("province", "云南", "23", "23");
addOption("23", "请选择城市", "");
addOption("23", "昆明市", "16");

addOption("23", "曲靖市", "462");

addOption("23", "玉溪市", "463");

addOption("23", "丽江地区", "464");

addOption("23", "昭通地区", "465");

addOption("23", "思茅地区", "466");

addOption("23", "临沧地区", "467");

addOption("23", "保山地区", "468");

addOption("23", "德宏傣族景颇族自治州", "469");

addOption("23", "怒江傈僳族自治州", "470");

addOption("23", "迪庆藏族自治州", "471");

addOption("23", "大理白族自治州", "472");

addOption("23", "楚雄彝族自治州", "473");

addOption("23", "红河哈尼族彝族自治州", "474");

addOption("23", "文山壮族苗族自治州", "475");

addOption("23", "西双版纳傣族自治州", "476");

addOption("23", "其它地区", "477");

endOptGroup("23");
addList("province", "上海", "25", "25");
addOption("25", "请选择城市", "");
addOption("25", "黄浦区", "25");

addOption("25", "南市区", "379");

addOption("25", "卢湾区", "380");

addOption("25", "徐汇区", "381");

addOption("25", "长宁区", "382");

addOption("25", "静安区", "383");

addOption("25", "普陀区", "384");

addOption("25", "闸北区", "385");

addOption("25", "虹口区", "386");

addOption("25", "杨浦区", "387");

addOption("25", "闵行区", "388");

addOption("25", "宝山区", "389");

addOption("25", "嘉定区", "390");

addOption("25", "浦东新区", "391");

addOption("25", "金山区", "392");

addOption("25", "松江区", "393");

addOption("25", "崇明县", "394");

addOption("25", "青浦县", "395");

addOption("25", "南汇县", "396");

addOption("25", "奉贤县", "397");

addOption("25", "其它地区", "398");

addOption("25", "黄浦区", "509");

endOptGroup("25");
addList("province", "湖北", "27", "27");
addOption("27", "请选择城市", "");
addOption("27", "武汉市", "23");

addOption("27", "十堰市", "55");

addOption("27", "襄樊市", "56");

addOption("27", "荆门市", "57");

addOption("27", "孝感市", "58");

addOption("27", "黄冈市", "59");

addOption("27", "鄂州市", "60");

addOption("27", "黄石市", "61");

addOption("27", "咸宁市", "62");

addOption("27", "荆州市", "63");

addOption("27", "宜昌市", "64");

addOption("27", "随州市", "65");

addOption("27", "仙桃市", "66");

addOption("27", "天门市", "67");

addOption("27", "潜江市", "68");

addOption("27", "神农架林区", "69");

addOption("27", "恩施土家族苗族自治州", "70");

addOption("27", "其它地区", "71");

endOptGroup("27");
addList("province", "广东", "28", "28");
addOption("28", "请选择城市", "");
addOption("28", "广州市", "30");

addOption("28", "深圳市", "31");

addOption("28", "东莞市", "32");

addOption("28", "汕头市", "33");

addOption("28", "珠海市", "34");

addOption("28", "清远市", "38");

addOption("28", "韶关市", "39");

addOption("28", "河源市", "40");

addOption("28", "梅州市", "41");

addOption("28", "潮州市", "42");

addOption("28", "揭阳市", "43");

addOption("28", "汕尾市", "44");

addOption("28", "惠州市", "45");

addOption("28", "中山市", "46");

addOption("28", "江门市", "47");

addOption("28", "佛山市", "48");

addOption("28", "肇庆市", "49");

addOption("28", "云浮市", "50");

addOption("28", "阳江市", "51");

addOption("28", "茂名市", "52");

addOption("28", "湛江市", "53");

addOption("28", "其它地区", "54");

endOptGroup("28");
addList("province", "山西", "29", "29");
addOption("29", "请选择城市", "");
addOption("29", "大同市", "367");

addOption("29", "太原市", "368");

addOption("29", "朔州市", "369");

addOption("29", "阳泉市", "370");

addOption("29", "长治市", "371");

addOption("29", "晋城市", "372");

addOption("29", "忻州地区", "373");

addOption("29", "吕梁地区", "374");

addOption("29", "晋中地区", "375");

addOption("29", "临汾地区", "376");

addOption("29", "运城地区", "377");

addOption("29", "其它地区", "378");

endOptGroup("29");
addList("province", "辽宁", "30", "30");
addOption("30", "请选择城市", "");
addOption("30", "沈阳市", "352");

addOption("30", "朝阳市", "353");

addOption("30", "阜新市", "354");

addOption("30", "铁岭市", "355");

addOption("30", "抚顺市", "356");

addOption("30", "本溪市", "357");

addOption("30", "辽阳市", "358");

addOption("30", "鞍山市", "359");

addOption("30", "丹东市", "360");

addOption("30", "大连市", "361");

addOption("30", "营口市", "362");

addOption("30", "盘锦市", "363");

addOption("30", "锦州市", "364");

addOption("30", "葫芦岛市", "365");

addOption("30", "其它地区", "366");

endOptGroup("30");
addList("province", "吉林", "31", "31");
addOption("31", "请选择城市", "");
addOption("31", "长春市", "342");

addOption("31", "白城市", "343");

addOption("31", "松原市", "344");

addOption("31", "吉林市", "345");

addOption("31", "四平市", "346");

addOption("31", "辽源市", "347");

addOption("31", "通化市", "348");

addOption("31", "白山市", "349");

addOption("31", "延边朝鲜族自治州", "350");

addOption("31", "其它地区", "351");

endOptGroup("31");
addList("province", "浙江", "32", "32");
addOption("32", "请选择城市", "");
addOption("32", "杭州市", "331");

addOption("32", "湖州市", "332");

addOption("32", "嘉兴市", "333");

addOption("32", "舟山市", "334");

addOption("32", "宁波市", "335");

addOption("32", "绍兴市", "336");

addOption("32", "金华市", "337");

addOption("32", "台州市", "338");

addOption("32", "温州市", "339");

addOption("32", "丽水地区", "340");

addOption("32", "其它地区", "341");

endOptGroup("32");
addList("province", "安徽", "33", "33");
addOption("33", "请选择城市", "");
addOption("33", "合肥市", "314");

addOption("33", "宿州市", "315");

addOption("33", "淮北市", "316");

addOption("33", "阜阳市", "317");

addOption("33", "蚌埠市", "318");

addOption("33", "淮南市", "319");

addOption("33", "滁州市", "320");

addOption("33", "马鞍山", "321");

addOption("33", "芜湖市", "322");

addOption("33", "铜陵市", "323");

addOption("33", "安庆市", "324");

addOption("33", "黄山市", "325");

addOption("33", "六安", "326");

addOption("33", "巢湖", "327");

addOption("33", "池州", "328");

addOption("33", "宣城", "329");

addOption("33", "其它地区", "330");

endOptGroup("33");
addList("province", "福建", "34", "34");
addOption("34", "请选择城市", "");
addOption("34", "福州市", "304");

addOption("34", "南平市", "305");

addOption("34", "三明市", "306");

addOption("34", "莆田市", "307");

addOption("34", "泉州市", "308");

addOption("34", "厦门市", "309");

addOption("34", "漳州市", "310");

addOption("34", "龙岩市", "311");

addOption("34", "宁德市", "312");

addOption("34", "其它地区", "313");

endOptGroup("34");
addList("province", "山东", "35", "35");
addOption("35", "请选择城市", "");
addOption("35", "济南市", "286");

addOption("35", "聊城市", "287");

addOption("35", "德州市", "288");

addOption("35", "东营市", "289");

addOption("35", "淄博市", "290");

addOption("35", "潍坊市", "291");

addOption("35", "烟台市", "292");

addOption("35", "威海市", "293");

addOption("35", "青岛市", "294");

addOption("35", "日照市", "295");

addOption("35", "临沂市", "296");

addOption("35", "枣庄市", "297");

addOption("35", "济宁市", "298");

addOption("35", "泰安市", "299");

addOption("35", "莱芜市", "300");

addOption("35", "滨州市", "301");

addOption("35", "菏泽市", "302");

addOption("35", "其它地区", "303");

endOptGroup("35");
addList("province", "内蒙古", "36", "36");
addOption("36", "请选择城市", "");
addOption("36", "呼和浩特", "273");

addOption("36", "包头市", "274");

addOption("36", "乌海市", "275");

addOption("36", "赤峰市", "276");

addOption("36", "呼伦贝尔", "277");

addOption("36", "兴安盟", "278");

addOption("36", "哲里木盟", "279");

addOption("36", "锡林郭勒", "280");

addOption("36", "乌兰察布", "281");

addOption("36", "伊克昭盟", "282");

addOption("36", "巴彦淖尔", "283");

addOption("36", "阿拉善盟", "284");

addOption("36", "其它地区", "285");

endOptGroup("36");
addList("province", "黑龙江", "37", "37");
addOption("37", "请选择城市", "");
addOption("37", "哈尔滨市", "259");

addOption("37", "齐齐哈尔", "260");

addOption("37", "黑河市", "261");

addOption("37", "大庆市", "262");

addOption("37", "伊春市", "263");

addOption("37", "鹤岗市", "264");

addOption("37", "佳木斯市", "265");

addOption("37", "双鸭山市", "266");

addOption("37", "七台河市", "267");

addOption("37", "鸡西市", "268");

addOption("37", "牡丹江市", "269");

addOption("37", "绥化地区", "270");

addOption("37", "大兴安岭", "271");

addOption("37", "其它地区", "272");

endOptGroup("37");
addList("province", "广西", "38", "38");
addOption("38", "请选择城市", "");
addOption("38", "南宁市", "244");

addOption("38", "桂林市", "245");

addOption("38", "柳州市", "246");

addOption("38", "梧州市", "247");

addOption("38", "贵港市", "248");

addOption("38", "玉林市", "249");

addOption("38", "钦州市", "250");

addOption("38", "北海市", "251");

addOption("38", "防城港市", "252");

addOption("38", "南宁地区", "253");

addOption("38", "百色地区", "254");

addOption("38", "河池地区", "255");

addOption("38", "柳州地区", "256");

addOption("38", "贺州地区", "257");

addOption("38", "其它地区", "258");

endOptGroup("38");
addList("province", "海南", "39", "39");
addOption("39", "请选择城市", "");
addOption("39", "海口市", "223");

addOption("39", "三亚市", "224");

addOption("39", "琼山市", "225");

addOption("39", "文昌市", "226");

addOption("39", "琼海市", "227");

addOption("39", "万宁市", "228");

addOption("39", "通什市", "229");

addOption("39", "东方市", "230");

addOption("39", "儋州市", "231");

addOption("39", "临高县", "232");

addOption("39", "澄迈县", "233");

addOption("39", "定安县", "234");

addOption("39", "屯昌县", "235");

addOption("39", "昌江黎族自治县", "236");

addOption("39", "白沙黎族自治县", "237");

addOption("39", "琼中黎族苗族自治县", "238");

addOption("39", "陵水里族自治县", "239");

addOption("39", "保亭黎族苗族自治县", "240");

addOption("39", "乐东黎族自治县", "241");

addOption("39", "西沙群岛南沙群岛中沙群岛办事", "242");

addOption("39", "其它地区", "243");

endOptGroup("39");
addList("province", "四川", "40", "40");
addOption("40", "请选择城市", "");
addOption("40", "成都市", "201");

addOption("40", "广元市", "202");

addOption("40", "绵阳市", "203");

addOption("40", "德阳市", "204");

addOption("40", "南充市", "205");

addOption("40", "广安市", "206");

addOption("40", "遂宁市", "207");

addOption("40", "内江市", "208");

addOption("40", "乐山市", "209");

addOption("40", "自贡市", "210");

addOption("40", "泸州市", "211");

addOption("40", "宜宾市", "212");

addOption("40", "攀枝花市", "213");

addOption("40", "巴中地区", "214");

addOption("40", "达川地区", "215");

addOption("40", "资阳地区", "216");

addOption("40", "眉山地区", "217");

addOption("40", "雅安地区", "218");

addOption("40", "阿坝藏族羌族自治州", "219");

addOption("40", "甘孜藏族自治州", "220");

addOption("40", "凉山彝族自治州", "221");

addOption("40", "其它地区", "222");

endOptGroup("40");
addList("province", "重庆", "41", "41");
addOption("41", "请选择城市", "");
addOption("41", "渝中区", "169");

addOption("41", "大渡口", "170");

addOption("41", "江北区", "171");

addOption("41", "沙坪坝", "172");

addOption("41", "九龙坡", "173");

addOption("41", "南岸区", "174");

addOption("41", "北碚区", "175");

addOption("41", "万盛区", "176");

addOption("41", "双桥区", "177");

addOption("41", "渝北区", "178");

addOption("41", "巴南区", "179");

addOption("41", "万州区", "180");

addOption("41", "涪陵区", "181");

addOption("41", "合川市", "182");

addOption("41", "永川市", "183");

addOption("41", "江津市", "184");

addOption("41", "南川市", "185");

addOption("41", "长寿县", "186");

addOption("41", "綦江县", "187");

addOption("41", "潼南县", "188");

addOption("41", "铜梁县", "189");

addOption("41", "大足县", "190");

addOption("41", "荣昌县", "191");

addOption("41", "璧山县", "192");

addOption("41", "垫江县", "193");

addOption("41", "武隆县", "194");

addOption("41", "丰都县", "195");

addOption("41", "城口县", "196");

addOption("41", "梁平县", "197");

addOption("41", "万州开发区", "198");

addOption("41", "黔江开发区", "199");

addOption("41", "其它地区", "200");

endOptGroup("41");
addList("province", "台湾", "42", "42");
addOption("42", "请选择城市", "");
addOption("42", "台北市", "164");

addOption("42", "高雄市", "165");

addOption("42", "台南市", "166");

addOption("42", "台中市", "167");

addOption("42", "其它地区", "168");

endOptGroup("42");
addList("province", "贵州", "43", "43");
addOption("43", "请选择城市", "");
addOption("43", "贵阳市", "154");

addOption("43", "六盘水市", "155");

addOption("43", "遵义市", "156");

addOption("43", "毕节地区", "157");

addOption("43", "铜仁地区", "158");

addOption("43", "安顺地区", "159");

addOption("43", "黔东南苗族侗族自治州", "160");

addOption("43", "黔南布依族苗族自治州", "161");

addOption("43", "黔西南布依族苗族自治州", "162");

addOption("43", "其它地区", "163");

endOptGroup("43");
addList("province", "西藏", "44", "44");
addOption("44", "请选择城市", "");
addOption("44", "拉萨市", "146");

addOption("44", "那曲地区", "147");

addOption("44", "昌都地区", "148");

addOption("44", "林芝地区", "149");

addOption("44", "山南地区", "150");

addOption("44", "日喀则", "151");

addOption("44", "阿里地区", "152");

addOption("44", "其它地区", "153");

endOptGroup("44");
addList("province", "陕西", "45", "45");
addOption("45", "请选择城市", "");
addOption("45", "西安市", "135");

addOption("45", "延安市", "136");

addOption("45", "铜川市", "137");

addOption("45", "咸阳市", "138");

addOption("45", "渭南市", "139");

addOption("45", "宝鸡市", "140");

addOption("45", "汉中市", "141");

addOption("45", "榆林地区", "142");

addOption("45", "商洛地区", "143");

addOption("45", "安康地区", "144");

addOption("45", "其它地区", "145");

endOptGroup("45");
addList("province", "甘肃", "46", "46");
addOption("46", "请选择城市", "");
addOption("46", "兰州市", "120");

addOption("46", "嘉峪关市", "121");

addOption("46", "金昌市", "122");

addOption("46", "白银市", "123");

addOption("46", "天水市", "124");

addOption("46", "酒泉地区", "125");

addOption("46", "张掖地区", "126");

addOption("46", "武威地区", "127");

addOption("46", "庆阳地区", "128");

addOption("46", "平凉地区", "129");

addOption("46", "定西地区", "130");

addOption("46", "陇南地区", "131");

addOption("46", "临夏回族自治州", "132");

addOption("46", "甘南藏族自治州", "133");

addOption("46", "其它地区", "134");

endOptGroup("46");
addList("province", "青海", "47", "47");
addOption("47", "请选择城市", "");
addOption("47", "西宁市", "111");

addOption("47", "海东地区", "112");

addOption("47", "海北藏族自治州", "113");

addOption("47", "海南藏族自治州", "114");

addOption("47", "黄南藏族自治州", "115");

addOption("47", "果洛藏族自治州", "116");

addOption("47", "玉树藏族自治州", "117");

addOption("47", "海西蒙古族藏族自治州", "118");

addOption("47", "其它地区", "119");

endOptGroup("47");
addList("province", "宁夏", "48", "48");
addOption("48", "请选择城市", "");
addOption("48", "银川市", "106");

addOption("48", "石嘴山市", "107");

addOption("48", "吴忠市", "108");

addOption("48", "固原地区", "109");

addOption("48", "其它地区", "110");

endOptGroup("48");
addList("province", "新疆", "49", "49");
addOption("49", "请选择城市", "");
addOption("49", "乌鲁木齐市", "89");

addOption("49", "克拉玛依市", "90");

addOption("49", "石河子市", "91");

addOption("49", "喀什地区", "92");

addOption("49", "阿克苏地区", "93");

addOption("49", "和田地区", "94");

addOption("49", "吐鲁番地区", "95");

addOption("49", "哈密地区", "96");

addOption("49", "克孜勒苏柯尔克孜自治州", "97");

addOption("49", "博尔塔拉蒙古自治州", "98");

addOption("49", "昌吉回族自治州", "99");

addOption("49", "巴音郭楞蒙古自治州", "100");

addOption("49", "伊犁哈萨克自治州", "101");

addOption("49", "伊犁地区", "102");

addOption("49", "塔城地区", "103");

addOption("49", "阿勒泰地区", "104");

addOption("49", "其它地区", "105");

endOptGroup("49");
addList("province", "香港", "50", "50");
addOption("50", "请选择城市", "");
addOption("50", "香港", "85");

addOption("50", "九龙", "86");

addOption("50", "新界", "87");

addOption("50", "其它地区", "88");

endOptGroup("50");
addList("province", "澳门", "51", "51");
addOption("51", "请选择城市", "");
addOption("51", "澳门", "83");

addOption("51", "其它地区", "84");

endOptGroup("51");
