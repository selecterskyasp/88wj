﻿addListGroup("sClass", "bClass");
addOption("bClass", "请选择大分类", "");
addList("bClass", "厨房餐具", "1", "1");
addOption("1", "请选择小分类", "");
addOption("1", "卫生洁具", "4");
addOption("1", "厨房用品", "5");
addOption("1", "水暖用品", "6");
addOption("1", "不锈钢餐具", "7");
addOption("1", "厨房挂件", "217");
addOption("1", "不锈钢餐具", "218");
addOption("1", "厨房设备", "219");
addOption("1", "蒸锅汤锅", "220");
addOption("1", "锅铲调羹", "221");
addOption("1", "食物器皿", "222");
addOption("1", "筷子", "223");
addOption("1", "碗盆盘碟", "224");
addOption("1", "烧烤", "225");
addOption("1", "厨柜", "226");
addOption("1", "保温容器", "227");
addOption("1", "炊具", "228");
addOption("1", "灶具", "229");
endOptGroup("1");
addList("bClass", "五金工具", "2", "2");
addOption("2", "请选择小分类", "");
addOption("2", "电动工具", "1");
addOption("2", "手动工具", "2");
addOption("2", "气动工具", "3");
addOption("2", "通用配件", "8");
addOption("2", "量具衡器", "9");
addOption("2", "研磨工具 ", "21");
addOption("2", "刀具、夹具", "22");
addOption("2", "钳工工具", "23");
addOption("2", "电工工具", "24");
addOption("2", "管工工具", "25");
addOption("2", "土木工具 ", "26");
addOption("2", "切削工具", "27");
addOption("2", "金刚石工具", "28");
addOption("2", "起重工具", "29");
addOption("2", "液压工具", "30");
addOption("2", "其他专用工具", "31");
addOption("2", "园林工具", "216");
endOptGroup("2");
addList("bClass", "义乌专区", "3", "3");
addOption("3", "请选择小分类", "");
addOption("3", "工具五金", "10");
addOption("3", "建筑五金类", "11");
addOption("3", "礼品五金", "12");
addOption("3", "匙扣拉链", "13");
addOption("3", "机械设备类", "14");
addOption("3", "日用五金类", "15");
addOption("3", "电子电器类", "16");
addOption("3", "电气电工类", "17");
addOption("3", "安全锁具类", "161");
addOption("3", "电工产品类", "162");
addOption("3", "水暖洁具类", "163");
addOption("3", "装潢装饰类", "164");
addOption("3", "车辆配件类", "165");
addOption("3", "体育旅游类", "166");
addOption("3", "厨房用品类", "167");
addOption("3", "不锈钢产品", "168");
addOption("3", "防盗门窗类", "169");
addOption("3", "保温杯类", "170");
endOptGroup("3");
addList("bClass", "玻璃橡胶", "6", "6");
addOption("6", "请选择小分类", "");
addOption("6", "玻璃制品", "92");
addOption("6", "橡胶制品", "93");
addOption("6", "合成制品", "184");
endOptGroup("6");
addList("bClass", "电工照明", "7", "7");
addOption("7", "请选择小分类", "");
addOption("7", "电子组件", "74");
addOption("7", "照明灯具", "75");
addOption("7", "磁性材料", "76");
addOption("7", "半导体材料", "77");
addOption("7", "绝缘材料", "78");
addOption("7", "电工陶瓷材料", "79");
addOption("7", "电子化学品", "80");
addOption("7", "插头，插座", "81");
addOption("7", "UPS与电源", "82");
addOption("7", "电动机、电机", "83");
addOption("7", "光电子、激光仪器 ", "84");
addOption("7", "电线电缆 ", "85");
addOption("7", "配电装置、开关柜、照明箱 ", "86");
addOption("7", "输电设备", "87");
addOption("7", "显示设备", "88");
addOption("7", "电热设备", "89");
addOption("7", "工业自动化装置 ", "90");
addOption("7", "电子材料", "91");
addOption("7", "仪器仪表", "269");
addOption("7", "低压电器", "270");
addOption("7", "配电装置", "271");
endOptGroup("7");
addList("bClass", "日用五金", "8", "8");
addOption("8", "请选择小分类", "");
addOption("8", "金属制品", "64");
addOption("8", "工艺制品", "65");
addOption("8", "保温容器", "66");
addOption("8", "钟表 ", "67");
addOption("8", "家具配件", "68");
addOption("8", "缝纫配件 ", "69");
addOption("8", "照明、信号", "70");
addOption("8", "家居用品", "71");
addOption("8", "工艺用五金工具 ", "72");
addOption("8", "打火机 烟具 ", "73");
addOption("8", "衣架衣柜", "292");
addOption("8", "雨具", "293");
addOption("8", "粘胶用品", "294");
addOption("8", "菜板", "295");
addOption("8", "拖把", "296");
addOption("8", "垫类", "297");
addOption("8", "刀剪刷", "298");
endOptGroup("8");
addList("bClass", "陶瓷玻璃", "9", "9");
addOption("9", "请选择小分类", "");
addOption("9", "陶瓷器皿", "94");
addOption("9", "陶瓷绝缘", "95");
addOption("9", "陶瓷工艺品", "185");
addOption("9", "玻璃制品", "299");
addOption("9", "工艺玻璃", "300");
addOption("9", "建筑玻璃", "301");
addOption("9", "建筑陶瓷", "302");
addOption("9", "原片玻璃", "305");
addOption("9", "日用玻璃", "306");
addOption("9", "玻璃工艺品", "307");
addOption("9", "卫浴陶瓷", "308");
addOption("9", "工业陶瓷", "309");
endOptGroup("9");
addList("bClass", "体育用品", "11", "11");
addOption("11", "请选择小分类", "");
addOption("11", "体育器械", "99");
addOption("11", "自行车、三轮车及配件 ", "100");
addOption("11", "滑板车类", "101");
addOption("11", "溜冰鞋类", "102");
addOption("11", "童车配件 ", "103");
addOption("11", "滑雪用具 ", "104");
addOption("11", "武术器材", "105");
addOption("11", "按摩器 ", "106");
addOption("11", "健身器材", "107");
addOption("11", "旅行包，拉杆箱，行李箱", "108");
addOption("11", "娱乐用品 ", "110");
addOption("11", "乐器 ", "111");
addOption("11", "渔具", "112");
addOption("11", "游艺设施 ", "113");
addOption("11", "登山用品、野营 ", "114");
addOption("11", "水族器材 ", "115");
addOption("11", "玩具 ", "116");
endOptGroup("11");
addList("bClass", "匙扣拉链", "12", "12");
addOption("12", "请选择小分类", "");
addOption("12", "钥匙扣", "117");
addOption("12", "拉链配件", "118");
addOption("12", "纽扣系列", "173");
addOption("12", "皮带扣", "333");
addOption("12", "尼龙拉链", "334");
addOption("12", "树脂拉链", "335");
addOption("12", "金属拉链", "336");
endOptGroup("12");
addList("bClass", "全新设备", "13", "13");
addOption("13", "请选择小分类", "");
addOption("13", "五金加工机械", "119");
addOption("13", "机床设备", "120");
addOption("13", "空气水泵", "121");
addOption("13", "矿产设备", "122");
addOption("13", "液压机械及部件 ", "123");
addOption("13", "五金设备", "125");
addOption("13", "雕刻打标设备 ", "126");
addOption("13", "动力机械 ", "127");
addOption("13", "仪器、仪表", "128");
addOption("13", "钻井设备 ", "129");
addOption("13", "渔业设备及用具", "130");
addOption("13", "粮油设备", "131");
addOption("13", "饲料加工机械 ", "132");
addOption("13", "肉类加工设备", "133");
addOption("13", "食品饮料加工设备", "134");
addOption("13", "皮革加工设备", "135");
addOption("13", "纺织设备", "136");
addOption("13", "制鞋及鞋修理设备", "137");
addOption("13", "矿业设备", "138");
addOption("13", "冶金设备 ", "139");
addOption("13", "制造设备", "140");
addOption("13", "石油设备", "141");
addOption("13", "印刷机械", "142");
addOption("13", "包装设备", "143");
addOption("13", "工程设备", "144");
addOption("13", "服装设备", "145");
addOption("13", "造纸设备及配件", "146");
addOption("13", "分离设备", "147");
addOption("13", "农用机械", "303");
addOption("13", "林业机械", "304");
addOption("13", "化工设备", "601");
endOptGroup("13");
addList("bClass", "模具磨料", "14", "14");
addOption("14", "请选择小分类", "");
addOption("14", "钢板模料", "148");
addOption("14", "模具系列", "149");
addOption("14", "模具加工", "186");
addOption("14", "塑料模", "323");
addOption("14", "橡胶模", "324");
addOption("14", "冲压模", "325");
addOption("14", "铸造模", "326");
addOption("14", "锻造模", "327");
addOption("14", "拉丝模", "328");
addOption("14", "成型模", "329");
addOption("14", "模具标准件", "330");
addOption("14", "模具设备", "331");
addOption("14", "模具配件", "332");
endOptGroup("14");
addList("bClass", "材料配件", "15", "15");
addOption("15", "请选择小分类", "");
addOption("15", "高新材料", "150");
addOption("15", "钢铁铝材", "151");
addOption("15", "稀土制品", "152");
addOption("15", "黑色金属", "153");
addOption("15", "粉末冶金 ", "154");
addOption("15", "磁性材料", "155");
addOption("15", "合金材料", "156");
addOption("15", "废金属", "157");
addOption("15", "石墨及碳素产品", "158");
addOption("15", "砧板", "159");
addOption("15", "炉料熔剂", "160");
endOptGroup("15");
addList("bClass", "其它", "16", "16");
addOption("16", "请选择小分类", "");
addOption("16", "商务礼品", "174");
addOption("16", "广告礼品", "175");
addOption("16", "节日礼品", "176");
addOption("16", "生日礼品", "177");
addOption("16", "工艺礼品", "178");
addOption("16", "情侣礼品", "179");
endOptGroup("16");
addList("bClass", "有色金属", "17", "17");
addOption("17", "请选择小分类", "");
addOption("17", "金银铜铁", "181");
addOption("17", "有色金属合金", "182");
addOption("17", "金属制品", "187");
addOption("17", "合金管材", "310");
addOption("17", "合金板材", "311");
addOption("17", "建筑钢材", "312");
addOption("17", "不锈钢材", "313");
addOption("17", "线材，卷材", "314");
addOption("17", "废金属", "315");
addOption("17", "磁性材料", "316");
addOption("17", "金属丝，网", "317");
addOption("17", "贵金属", "318");
addOption("17", "金属粉末", "319");
addOption("17", "钢结构", "320");
endOptGroup("17");
addList("bClass", "装璜装饰", "18", "18");
addOption("18", "请选择小分类", "");
addOption("18", "建筑装饰", "35");
addOption("18", "卫浴设施", "36");
addOption("18", "金属建材", "38");
addOption("18", "建筑金属构件", "39");
addOption("18", "防水、防潮材料", "40");
addOption("18", "耐火、防火材料 ", "41");
addOption("18", "隔热、吸声材料", "42");
addOption("18", "金属门窗 ", "44");
addOption("18", "管件管材 ", "45");
addOption("18", "钉、网 ", "46");
addOption("18", "胶粘剂 ", "47");
addOption("18", "地板瓷砖", "188");
addOption("18", "装饰配件", "254");
addOption("18", "装璜五金", "255");
addOption("18", "墙体材料", "256");
endOptGroup("18");
addList("bClass", "农艺五金", "19", "19");
addOption("19", "请选择小分类", "");
addOption("19", "渔业机械 ", "32");
addOption("19", "灌溉机械", "33");
addOption("19", "收获机械", "34");
endOptGroup("19");
addList("bClass", "汽摩配件", "20", "20");
addOption("20", "请选择小分类", "");
addOption("20", "汽车配件", "49");
addOption("20", "修理设备", "50");
addOption("20", "交通工具", "51");
addOption("20", "轮胎", "52");
addOption("20", "传动系统", "53");
addOption("20", "转向、制动系统", "54");
addOption("20", "照明、信号、车用电器", "55");
addOption("20", "车用仪表", "56");
addOption("20", "发动机及零部件", "57");
addOption("20", "汽车保养、美容", "58");
addOption("20", "汽摩产品制造设备", "59");
addOption("20", "汽车小电器", "60");
addOption("20", "冷却系统", "61");
addOption("20", "减震系统", "62");
addOption("20", "自行车配件", "172");
addOption("20", "车内装饰", "252");
addOption("20", "电动车配件", "253");
endOptGroup("20");
addList("bClass", "防盗门窗", "21", "21");
addOption("21", "请选择小分类", "");
addOption("21", "防盗门", "189");
addOption("21", "防盗窗", "190");
addOption("21", "其他防盗报警器材", "191");
addOption("21", "门磁、窗磁", "192");
addOption("21", "保险柜", "193");
endOptGroup("21");
addList("bClass", "通用零部件", "22", "22");
addOption("22", "请选择小分类", "");
addOption("22", "齿轮", "194");
addOption("22", "机床附件", "195");
addOption("22", "弹簧", "196");
addOption("22", "密封件", "197");
addOption("22", "分离设备", "198");
addOption("22", "焊接材料", "199");
addOption("22", "紧固件、连接件", "200");
addOption("22", "轴承 ", "201");
addOption("22", "传动链", "202");
addOption("22", "炉头", "203");
addOption("22", "链条锁", "204");
addOption("22", "链轮 、脚轮、万向轮、滑轮", "205");
addOption("22", "化工管道及配件", "206");
addOption("22", "工作台", "207");
addOption("22", "滚筒、管夹", "208");
addOption("22", "钢珠、滚珠、钢丝绳 ", "209");
addOption("22", "斗齿、吊滑车、 吊钩、抓钩", "210");
addOption("22", "直通、托辊、输送带", "211");
addOption("22", "喷嘴、喷头、接头、拉马", "212");
addOption("22", "其他五金配件", "213");
endOptGroup("22");
addList("bClass", "紧固配件", "24", "24");
addOption("24", "请选择小分类", "");
addOption("24", "螺栓", "230");
addOption("24", "螺母", "231");
addOption("24", "铆接", "232");
addOption("24", "膨胀", "233");
addOption("24", "螺钉", "234");
addOption("24", "板槽钉", "235");
addOption("24", "内六角", "236");
addOption("24", "销与钉", "237");
addOption("24", "垫圈", "238");
addOption("24", "紧固设备", "239");
endOptGroup("24");
addList("bClass", "锁具安防", "25", "25");
addOption("25", "请选择小分类", "");
addOption("25", "锁具", "240");
addOption("25", "防盗装置", "241");
addOption("25", "交通安全设备", "242");
addOption("25", "消防设备", "243");
addOption("25", "作业保护", "244");
addOption("25", "防暴器材", "245");
addOption("25", "救生器材", "246");
addOption("25", "监控器材", "247");
addOption("25", "防身用具", "248");
addOption("25", "楼宇对讲系统", "249");
addOption("25", "军需用品", "250");
addOption("25", "其它", "251");
endOptGroup("25");
addList("bClass", "小家电", "26", "26");
addOption("26", "请选择小分类", "");
addOption("26", "厨房家电", "257");
addOption("26", "剃须刀", "258");
addOption("26", "电吹风", "259");
addOption("26", "饮水机", "260");
addOption("26", "榨汁机", "261");
addOption("26", "豆浆机", "262");
addOption("26", "风扇", "263");
addOption("26", "电热取暖", "264");
addOption("26", "夏季专用", "265");
addOption("26", "吸尘器", "266");
addOption("26", "电烫斗", "267");
addOption("26", "其它", "268");
endOptGroup("26");
addList("bClass", "水暖卫浴", "27", "27");
addOption("27", "请选择小分类", "");
addOption("27", "卫生洁具", "272");
addOption("27", "水龙头", "273");
addOption("27", "水槽", "274");
addOption("27", "淋浴房", "275");
addOption("27", "水暖配件", "276");
addOption("27", "浴室配件", "277");
addOption("27", "阀门", "278");
addOption("27", "洗脸盆", "279");
addOption("27", "浇花用具", "280");
addOption("27", "其它", "281");
addOption("27", "地漏", "322");
endOptGroup("27");
addList("bClass", "货架门窗", "28", "28");
addOption("28", "请选择小分类", "");
addOption("28", "超市货架", "282");
addOption("28", "仓储货架", "283");
addOption("28", "防盗门窗", "284");
addOption("28", "塑钢门窗", "285");
addOption("28", "铝合金门窗", "286");
addOption("28", "卷帘门", "287");
addOption("28", "推拉门", "288");
addOption("28", "展示柜窗", "289");
addOption("28", "木质门窗", "290");
addOption("28", "防火门窗", "291");
endOptGroup("28");
addList("bClass", "二手机床", "29", "29");
addOption("29", "请选择小分类", "");
addOption("29", "车床", "337");
addOption("29", "锯床", "338");
addOption("29", "铣床", "339");
addOption("29", "刨床 ", "340");
addOption("29", " 冲床 ", "341");
addOption("29", "锻压 ", "342");
addOption("29", "弯管机", "343");
addOption("29", " 珩磨机 ", "344");
addOption("29", "磨床", "345");
addOption("29", "钻床", "346");
addOption("29", "压力机", "347");
addOption("29", "抛光机", "348");
addOption("29", "折边卷板", "349");
addOption("29", "组合专机", "350");
addOption("29", "数控设备", "351");
addOption("29", "拉床", "352");
addOption("29", "剪切", "353");
addOption("29", "线切割 ", "354");
endOptGroup("29");
addList("bClass", "化工设备", "30", "30");
addOption("30", "请选择小分类", "");
addOption("30", "反应设备 ", "355");
addOption("30", "储运设备", "356");
addOption("30", "实验设备", "357");
addOption("30", "混合设备", "358");
addOption("30", "蒸馏塔", "359");
addOption("30", "储罐", "360");
addOption("30", "化工泵", "361");
addOption("30", "离心机", "362");
addOption("30", "制氧机", "363");
addOption("30", "干燥机", "364");
addOption("30", "氧气罐", "365");
addOption("30", "粉碎设备", "366");
addOption("30", "反应釜", "367");
addOption("30", "震荡筛", "368");
addOption("30", "成型设备", "369");
endOptGroup("30");
addList("bClass", "纺织设备", "31", "31");
addOption("31", "请选择小分类", "");
addOption("31", "纺纱设备", "370");
addOption("31", "梭织机", "371");
addOption("31", "印染整套设备", "372");
addOption("31", "针织机", "373");
addOption("31", "络筒机", "374");
addOption("31", "织带机", "375");
addOption("31", "细纱机", "376");
addOption("31", "烫平机", "377");
addOption("31", "编织机", "378");
addOption("31", "化纤机", "379");
addOption("31", "织造机", "380");
addOption("31", "棉花加工设备", "381");
addOption("31", "经编机", "382");
addOption("31", "印花机", "383");
addOption("31", "提花机", "384");
addOption("31", "粗纱机", "385");
addOption("31", "绣花机", "386");
addOption("31", "植绒机", "387");
addOption("31", "复合机", "388");
addOption("31", "检针器", "389");
endOptGroup("31");
addList("bClass", "工程设备", "32", "32");
addOption("32", "请选择小分类", "");
addOption("32", "海港吊", "390");
addOption("32", "塔吊设备", "391");
addOption("32", "履带吊", "392");
addOption("32", "打桩机", "393");
addOption("32", "提升机", "394");
addOption("32", "混凝土机械", "395");
addOption("32", "焊接设备", "396");
addOption("32", "叉车", "397");
addOption("32", "挖掘机械", "398");
addOption("32", "汽车吊", "399");
addOption("32", "装载机械", "400");
addOption("32", "破路机", "401");
addOption("32", "压路机", "402");
addOption("32", "震动机", "403");
addOption("32", "推土机", "404");
addOption("32", "工程车", "405");
addOption("32", "起重设备", "406");
addOption("32", "搅拌机", "407");
addOption("32", "龙门吊", "408");
endOptGroup("32");
addList("bClass", "冶金设备", "33", "33");
addOption("33", "请选择小分类", "");
addOption("33", "钢管设备", "409");
addOption("33", "炼铁设备", "410");
addOption("33", "炼钢设备", "411");
addOption("33", "轧钢设备", "412");
addOption("33", "焦化设备", "413");
addOption("33", "钢丝设备", "414");
addOption("33", "卷管机", "415");
addOption("33", "无缝管设备", "416");
addOption("33", "热处理设备", "417");
addOption("33", "烧结机", "418");
addOption("33", "冶金成套设备", "419");
addOption("33", "轧机", "420");
addOption("33", "不锈钢设备", "421");
addOption("33", "拉伸机", "422");
addOption("33", "铸造设备", "423");
addOption("33", "液压设备", "424");
endOptGroup("33");
addList("bClass", "印刷设备", "34", "34");
addOption("34", "请选择小分类", "");
addOption("34", "制版", "425");
addOption("34", "复合机", "426");
addOption("34", "包本机", "427");
addOption("34", "丝印设备", "428");
addOption("34", "晒版机", "429");
addOption("34", "分切机", "430");
addOption("34", "复膜机", "431");
addOption("34", "印刷机", "432");
addOption("34", "轮转印机", "433");
addOption("34", "裁纸机", "434");
addOption("34", "压痕机", "435");
addOption("34", "移印机", "436");
addOption("34", "胶印机", "437");
addOption("34", "柔印机", "438");
addOption("34", "不干胶设备", "439");
addOption("34", "烫金机", "440");
addOption("34", "喷码机", "441");
addOption("34", "折页机", "442");
addOption("34", "上光机", "443");
addOption("34", "雕刻机", "444");
addOption("34", "凹印机", "445");
addOption("34", "印刷配件", "446");
endOptGroup("34");
addList("bClass", "包装设备", "35", "35");
addOption("35", "请选择小分类", "");
addOption("35", "纸品包装设备", "447");
addOption("35", "塑料包装设备", "448");
addOption("35", "金属包装设备", "449");
addOption("35", "竹木包装设备", "450");
addOption("35", "发泡机", "451");
addOption("35", "糊盒机", "452");
addOption("35", "纸袋机", "453");
addOption("35", "打样机", "454");
addOption("35", "高频机", "455");
addOption("35", "复合材料包装设备", "456");
addOption("35", "封切机", "457");
addOption("35", "整形机", "458");
addOption("35", "压盖机", "459");
addOption("35", "打包机", "460");
addOption("35", "充填机械", "461");
addOption("35", "打码机", "462");
addOption("35", "封口机", "463");
addOption("35", "灌装机械", "464");
addOption("35", "裹包机", "465");
addOption("35", "捆扎机械", "466");
addOption("35", " 喷码机", "467");
addOption("35", "绳索扎带", "468");
addOption("35", "缠绕机", "469");
addOption("35", "吹瓶机", "470");
endOptGroup("35");
addList("bClass", "分离设备", "36", "36");
addOption("36", "请选择小分类", "");
addOption("36", "过滤分离设备", "471");
addOption("36", "气体分离设备", "472");
addOption("36", "离心压缩机", "473");
addOption("36", "空压机", "474");
addOption("36", "制氧机", "475");
addOption("36", "压缩机", "476");
addOption("36", "空调设备", "477");
addOption("36", "氧气氢气设备", "478");
addOption("36", "氮气压缩设备", "479");
addOption("36", "储气罐设备", "480");
endOptGroup("36");
addList("bClass", "矿业设备", "37", "37");
addOption("37", "请选择小分类", "");
addOption("37", "采掘设备", "481");
addOption("37", "破碎设备", "482");
addOption("37", "选矿设备", "483");
addOption("37", "勘探设备", "484");
addOption("37", "球磨机", "485");
addOption("37", "雷蒙磨", "486");
addOption("37", "破碎机", "487");
addOption("37", "过滤机", "488");
addOption("37", "磁选设备", "489");
addOption("37", "钻机", "490");
addOption("37", "提升机", "491");
addOption("37", "分级设备", "492");
addOption("37", "筛分设备", "493");
addOption("37", "挖掘机", "494");
addOption("37", "浮选设备", "495");
endOptGroup("37");
addList("bClass", "服装设备", "38", "38");
addOption("38", "请选择小分类", "");
addOption("38", "打孔机", "496");
addOption("38", "封包机", "497");
addOption("38", "粘合机", "498");
addOption("38", "绷缝机", "499");
addOption("38", "缝纫机", "500");
addOption("38", "压烫机", "501");
addOption("38", "制鞋设备", "502");
addOption("38", "刺绣机", "503");
addOption("38", "套结机", "504");
addOption("38", "卷线机", "505");
addOption("38", "摺景机", "506");
addOption("38", "钉扣机", "507");
addOption("38", "花边机", "508");
addOption("38", "暗缝机", "509");
addOption("38", "熨烫设备", "510");
addOption("38", "开袋机", "511");
addOption("38", "曲腕机", "512");
addOption("38", "切带机", "513");
addOption("38", "水洗脱水", "514");
addOption("38", "包边机", "515");
addOption("38", "裁剪机", "517");
addOption("38", "验布机", "518");
addOption("38", "预缩机", "519");
addOption("38", "珠边机", "520");
addOption("38", "毛皮机 ", "521");
addOption("38", "包缝机", "522");
addOption("38", "干洗烘干机", "523");
addOption("38", "高头车", "524");
addOption("38", "切條机", "525");
addOption("38", "平缝机", "526");
addOption("38", "花样机", "527");
addOption("38", "家用机", "528");
addOption("38", "摊布机", "529");
addOption("38", "编织机", "530");
addOption("38", "裁布机", "531");
endOptGroup("38");
addList("bClass", "发电设备", "39", "39");
addOption("39", "请选择小分类", "");
addOption("39", "电动机", "532");
addOption("39", "风机", "533");
addOption("39", "中央空调", "534");
addOption("39", "风力发电机", "535");
addOption("39", "热力发电机", "536");
addOption("39", "柴油发电机", "537");
addOption("39", "汽油发电机", "538");
addOption("39", "汽轮发电机", "539");
addOption("39", "康明斯发电机", "540");
addOption("39", "劳斯莱斯发电机", "541");
endOptGroup("39");
addList("bClass", "电力电工", "40", "40");
addOption("40", "请选择小分类", "");
addOption("40", "电线电缆", "542");
addOption("40", "电源柜", "543");
addOption("40", "低压开关", "544");
addOption("40", "控制开关柜", "545");
addOption("40", "高压开关柜", "546");
addOption("40", "汽轮机", "547");
addOption("40", "变压设备", "548");
addOption("40", "电流互感器", "549");
addOption("40", "成套开关", "550");
addOption("40", "直控开关", "551");
addOption("40", "输电设备", "552");
addOption("40", "电力电容器", "553");
addOption("40", "励磁机", "554");
addOption("40", "电工电气产品", "555");
endOptGroup("40");
addList("bClass", "农林设备", "41", "41");
addOption("41", "请选择小分类", "");
addOption("41", "播种机", "556");
addOption("41", "插秧机", "557");
addOption("41", "饲料机械", "558");
addOption("41", "收割机", "559");
addOption("41", "土壤耕整机械", "560");
addOption("41", "拖拉机", "561");
addOption("41", "粮食加工设备", "562");
addOption("41", "肥料设备", "563");
addOption("41", "粮油设备", "564");
addOption("41", "修剪机", "565");
addOption("41", "除草机", "566");
addOption("41", "除虫设备", "567");
addOption("41", "浇灌设备", "568");
addOption("41", "砍伐机械", "569");
addOption("41", "打捆机", "570");
endOptGroup("41");
addList("bClass", "塑料设备", "42", "42");
addOption("42", "请选择小分类", "");
addOption("42", "塑料编织设备", "571");
addOption("42", "挤出片材机", "572");
addOption("42", "PVC管材设备", "573");
addOption("42", "塑料破碎机械", "574");
addOption("42", "型材生产线设备", "575");
addOption("42", "泡沫生产线设备", "576");
addOption("42", "挤压机", "577");
addOption("42", "吸塑机", "578");
addOption("42", "挤塑机", "579");
addOption("42", "注塑机", "580");
endOptGroup("42");
addList("bClass", "其它设备", "43", "43");
addOption("43", "请选择小分类", "");
addOption("43", "工业锅炉", "581");
addOption("43", "橡胶设备", "582");
addOption("43", "陶瓷设备", "583");
addOption("43", "设备配件", "584");
addOption("43", "电子数码", "585");
addOption("43", "二手车", "586");
addOption("43", "变压器", "587");
addOption("43", "变速箱", "588");
addOption("43", "交通船舶", "589");
addOption("43", "石油设备", "590");
addOption("43", "风机设备", "591");
addOption("43", "水泥设备", "592");
addOption("43", "食品设备", "593");
addOption("43", "酒店设备", "594");
addOption("43", "冷暖设备", "595");
addOption("43", "仪器仪表", "596");
addOption("43", "造纸设备", "597");
addOption("43", "木工机械", "598");
addOption("43", "体育游乐", "599");
addOption("43", "通讯设备", "600");
endOptGroup("43");


//最后修改时间:2010-1-21
