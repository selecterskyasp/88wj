
$=function(id){
	return document.getElementById(id);
}

Namespace = new Object();
Namespace.register = function(namespace){
	var nsArray = namespace.split('.');
	var sEval = "";
    var sNS = "";
	for (var i = 0; i < nsArray.length; i++){
        if (i != 0) sNS += ".";
        sNS += nsArray[i];
        sEval += "if (typeof(" + sNS + ") == 'undefined') " + sNS + " = new Object();"
    }
    if (sEval != "") eval(sEval);
}

Array.prototype.unite = function(arg){
	var temparg = new Array();
	var len1 = this.length;
	var len2 = arg.length;
	for(var i=0;i<(len1+len2);i++){
		if(i<len1) this[i] = this[i];
		else this[i] = arg[i-len1];
	}
	return this;
}

Array.prototype.add = function(obj){
	var add_flag = true;
	for(var i=0;i<this.length;i++){
		if(this[i]==obj){
			add_flag = false;
			break;
		}
	}
	if(add_flag==true){
		this[this.length] = obj;
	}
}

Namespace.register("Hc.widget.cookie");
Namespace.register("Hc.widget.movebox");
Namespace.register("Hc.util.setDiv");
Namespace.register("Hc.util.move");
Namespace.register("Hc.util.showDiv");
Namespace.register("Hc.util.hotClass");
Namespace.register("Hc.widget.hotword");


(function(){
	var movebox = null;
	var moveboxs = new Array();
	var oStart,oEnd,oStartY,oEndY,oStartAdH,oEndAdH;
	var speed = 36;
	var mflag = true;
	Hc.util.move = {

		init:function(id,mb_arr,cookiename){
			
			var movebox = new Hc.widget.movebox(id);
			for(var i=0;i<mb_arr.length;i++){
				movebox.list.push($(mb_arr[i]));
			}
			movebox.cookieid = cookiename;
			moveboxs.push(movebox);
		},

		moveUp:function(m_b,now,argAdTag,argAdCname){
			if(mflag){
			oStart=null;
			oEnd=null;
			oStartY=null;
			oEndY=null;
			var changepoint = new Array();
			var oStartAdObj,oEndAdObj;
			movebox = this.isHave(m_b);
			if(this.getUp(now)!=null){
				changepoint = this.getUp(now);
				oStart = changepoint[0];
				oEnd = changepoint[1];
				if(argAdTag!=null && argAdCname!=null){
					oStartAdObj = this.getAdCon(oStart,argAdTag,argAdCname);
					oEndAdObj = this.getAdCon(oEnd,argAdTag,argAdCname);
					oStartAdObj[0].innerHTML = oEndAdObj[1];
					oEndAdObj[0].innerHTML = oStartAdObj[1];
					oStartAdH = oStartAdObj[2];
					oEndAdH = oEndAdObj[2];
				}
				oStartY = parseInt(oEnd.style.top);
				oEndY = parseInt(oStart.style.top);
				oStart.style.zIndex = "0";
				oEnd.style.zIndex = "360";
				oStart.style.filter="alpha(opacity=10)";
				this.startmoving();
				this.endmoving();
				oStart.style.filter="alpha(opacity=100)";
				this.change(oEnd,oStart);
			}
			Hc.widget.cookie.newAndModif(movebox.cookieid,this.toString(),36);
			}
			
		},

		moveDown:function(m_b,now,argAdTag,argAdCname){
		if(mflag){
			oStart=null;
			oEnd=null;
			oStartY=null;
			oEndY=null;
			var changepoint = new Array();
			var oStartAdObj,oEndAdObj;
			adTag = argAdTag;
			adCname = argAdCname;
			movebox = this.isHave(m_b);
			if(this.getDown(now)!=null){
				changepoint = this.getDown(now);
				oStart = changepoint[0];
				oEnd = changepoint[1];
				if(argAdTag!=null && argAdCname!=null){
					oStartAdObj = this.getAdCon(oStart,argAdTag,argAdCname);
					oEndAdObj = this.getAdCon(oEnd,argAdTag,argAdCname);
					oStartAdObj[0].innerHTML = oEndAdObj[1];
					oEndAdObj[0].innerHTML = oStartAdObj[1];
					oStartAdH = oStartAdObj[2];
					oEndAdH = oEndAdObj[2];
				}
				oStartY = parseInt(oEnd.style.top);
				oEndY = parseInt(oStart.style.top);
				oStart.style.zIndex = "360";
				oEnd.style.zIndex = "0";
				oEnd.style.filter="alpha(opacity=10)";
				this.startmoving();
				this.endmoving();
				oEnd.style.filter="alpha(opacity=100)";
				this.change(oEnd,oStart);
				
			}
			Hc.widget.cookie.newAndModif(movebox.cookieid,this.toString(),36);
		}
			
		},
		getAdCon:function(el,argAdTag,argAdCname){
			var adTagArr = el.getElementsByTagName(argAdTag);
			var tempAdCon = new Array();
			for(var j=0;j<adTagArr.length;j++){
				if(adTagArr[j].className == argAdCname){
					tempAdCon[0] = adTagArr[j];
					tempAdCon[1] = adTagArr[j].innerHTML;
					tempAdCon[2] = adTagArr[j].scrollHeight;
					break;
				}
			}
			return tempAdCon;
		},

		startmoving:function(){
			var con = this;
			yStart = parseInt(oStart.style.top);
			yEnd = oStartY+(oEnd.scrollHeight-oStart.scrollHeight+(oEndAdH-oStartAdH));
			if(yStart<yEnd){
				this.moveY(oStart,1,speed,yEnd);
				setTimeout(function(){con.startmoving()},1);
				mflag = false;
			}
			if(yStart>yEnd){
				this.moveY(oStart,2,speed,yEnd);
				setTimeout(function(){con.startmoving()},1);
				mflag = false;
			}
			if(yStart==yEnd){
				mflag = true;
			}
		},

		endmoving:function(){
			yStart = parseInt(oEnd.style.top);
			yEnd = parseInt(oEndY);
			var con = this;
			if(yStart<yEnd){
				this.moveY(oEnd,1,speed,yEnd);
				setTimeout(function(){con.endmoving()},1);
				mflag = false;
			}
			if(yStart>yEnd){
				this.moveY(oEnd,2,speed,yEnd);
				setTimeout(function(){con.endmoving()},1);
				mflag = false;
			}
			if(yStart==yEnd){
				mflag = true;
			}
		},

		moveY:function(obj,ySpeenType,speed,yEnd){
			if(ySpeenType==1){
				if((parseInt(obj.style.top)+speed)>=yEnd){
					obj.style.top = yEnd+"px";
				}else{
					obj.style.top = parseInt(obj.style.top)+speed+"px";
				}
			}
			if(ySpeenType==2){
				if((parseInt(obj.style.top)-speed)<=yEnd){
					obj.style.top = yEnd+"px";
				}else{
					obj.style.top = parseInt(obj.style.top)-speed+"px";
				}
			}
		},

		getIndex:function(moveObj){
			for(var i=0;i<movebox.list.length;i++){
				if(movebox.list[i]==moveObj){
					return i;
				}
			}
			return -1;
		},

		isHave:function(m_b){
			for(var i=0;i<moveboxs.length;i++){
				if(moveboxs[i].name==$(m_b.id)){
					return moveboxs[i];
				}
			}
		},

		getUp:function(now){
			var arrm = new Array();
			for(var i=0;i<movebox.list.length;i++){
				if(movebox.list[i]==now){
					if(i>0){
						arrm[0] = movebox.list[i-1];
						arrm[1] = movebox.list[i];
						return arrm;
					}else{
						return null;
					}
				}
			}
		},

		getDown:function(now){
			var arrm = new Array();
			for(var i=0;i<movebox.list.length;i++){
				if(movebox.list[i]==now){
					if(i<movebox.list.length-1){
						arrm[0] = movebox.list[i];
						arrm[1] = movebox.list[i+1];
						return arrm;
					}else{
						return null;
					}
				}
			}
		},

		change:function(start,end){
			var s_index = this.getIndex(start);
			var e_index = this.getIndex(end);
			if(s_index!=-1&&e_index!=-1){
				movebox.list[s_index] = end;
				movebox.list[e_index] = start;
				movebox.version++;
			}
		},

		toString:function(){
			var tempStr = "";
			for(var i=0;i<movebox.list.length;i++){
				tempStr = tempStr + movebox.list[i].id + "&";
			}
			tempStr = tempStr.substring(0,tempStr.length-1);
			return tempStr;
		}
	};
})();


Hc.widget.movebox = function(name){
	this.name = name;
	this.list = new Array();
	this.size = 0;
	this.version = 0;
	this.obj = null;
	this.cookieid = null;
}

Hc.widget.hotword = function(name,url,cname){
	this.name = name;
	this.url = url;
	this.cname = cname;
}

function HCmarquee(id,mw,mh,mr,sr,ms,pause){
	
	var obj=document.getElementById(id);
	obj.ss=false; //stop tag
	obj.mr=mr; //marquee rows
	obj.sr=sr; //marquee display rows
	obj.mw=mw; //marquee width
	obj.mh=mh; //marquee height
	obj.ms=ms; //marquee speed
	obj.pause=pause; //pause time
	obj.pt=0; //pre top
	obj.st=0; //stop time
	obj.mul=1;
	obj.con="";
	with(obj){
		style.width=mw+"px";
		style.height=mh+"px";
		noWrap=false;
		onmouseover=stopm;
		onmouseout=startm;
		scrollTop=0+"px";
		scrollLeft=0+"px";
	}

	if(obj.mr!=1){
				obj.tt=mh*mr/sr;
				obj.ct=mh; //current top
				obj.innerHTML+=obj.innerHTML;
				setInterval(scrollUp,obj.ms);
	}
	function scrollUp(){
		if(obj.ss==true) return;
		obj.ct+=1;
		if(obj.ct==obj.mh+1){
			obj.st+=1; obj.ct-=1;
			if(obj.st==(obj.pause*obj.mul)){
				obj.ct=0; obj.st=0;
				if(obj.mul==1) obj.mul = 1;
				else obj.mul = 1;
			}
		}else {
			obj.pt=(++obj.scrollTop);
			if(obj.pt==obj.tt){obj.scrollTop=0;}
		}
	}

	function stopm(){obj.ss=true;}
	function startm(){obj.ss=false;}
	
}
